<?php
get_header();
while (have_posts()):

  the_post();

	// $post_cats = get_the_terms($post->ID, 'category');
	// $related_posts = [];
	// if ($post_cats) {
	// 	$post_cats_ids = [];
	// 	foreach ($post_cats as $cat) {
	// 		$post_cats_ids[] = $cat->term_id;
	// 	}
	// 	$tax_query = [
	// 		[
	// 			'taxonomy' => 'category',
	// 			'field' => 'term_id',
	// 			'terms' => $post_cats_ids,
	// 		],
	// 	];
	// 	$args = [
	// 		// post_type
	// 		'posts_per_page' => 3,
	// 		'tax_query' => $tax_query,
	// 		'exclude' => [$post->ID],
	// 		'suppress_filters' => 0,
	// 	];
	// 	$related_posts = get_posts($args);
	// }

	// $date = get_the_time('d M Y', $post->ID);
	// $all_link_url = spre_get_option('blog_page_id')
	// 	? get_permalink(spre_get_option('blog_page_id'))
	// 	: '';

	// // $header_url = spre_get_featured_image_url($post->ID, 'page_header');
  // $h1_title = $post->post_title;
  // var_dump($post, $post->post_excerpt)
  ?>
  <header class="relative z-0 pb-20 pageHeader">
    <div class="relative z-20 pageInner">
      <div class="relative flex justify-between w-full py-10">
        <?php include locate_template('includes/help_nav.php'); ?>
        <?php include locate_template('includes/social_share.php'); ?>
      </div>
      <div class="w-2/3 headerContent">
        <div class="flex mb-8">
          <div class="flex mr-4">
            <?php if (class_exists('MultiPostThumbnails')) :
              MultiPostThumbnails::the_post_thumbnail(
                get_post_type(),
                'secondary-image'
              );
            endif; ?>
          </div>
          <h1 class="text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($post->post_title) ?></h1>
        </div>
        <?php if ($post->post_excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $post->post_excerpt
          ) ?></div>
        <?php } ?>
      </div>
    </div>
    <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
      <div class="absolute bottom-0 right-0 z-0 overflow-hidden" style='left: 20px; top: 0px; background-image: url("<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg"); background-size: cover;'>
      </div>
    </div>
  </header>

  <div class="relative overflow-hidden row singleContentWrapper postContentWrapper">
    <div class="relative z-10 pageInner">
      <div class="singleContent postContent editableContent">
        <?php the_content(); ?>
        <div class="espace20"></div>
      </div>
    </div>
    <div class="absolute top-0 bottom-0 z-0 w-full" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/very_big_bg_circles.svg); background-position: top left; background-repeat: no-repeat; top:-330px; left: -1100px;">
    </div>
    <div class="absolute bottom-0 z-0 w-full top-72" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/big_circle_beige.svg); background-position: center right; background-repeat: no-repeat; right: -350px;">
    </div>
    <div class="absolute bottom-0 left-0 right-0 z-0 h-64 md:right-1/3" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/vibes.svg)">
    </div>
  </div>

  <?php
endwhile;
?>
<?php include(locate_template('includes/spre_help_footer.php')); ?>
<?php get_footer(); ?>
