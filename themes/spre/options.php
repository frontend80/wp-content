<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */

function optionsframework_option_name()
{
	// This gets the theme name from the stylesheet (lowercase and without spaces)
	$themename = get_option('stylesheet');
	$themename = preg_replace('/\W/', '_', strtolower($themename));

	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = $themename;
	update_option('optionsframework', $optionsframework_settings);

	// echo $themename;
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */

function optionsframework_options()
{
	// Pull all the pages into an array
	$options_pages = [];
	// $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages_obj = get_pages('sort_column=title');
	$options_pages[''] = 'Selectionner une page';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// Pull all the forms into an array
	$options_forms = [];
	if (class_exists('RGFormsModel')) {
		$options_forms_obj = RGFormsModel::get_forms();
	} else {
		$options_forms_obj = [];
	}
	$options_forms[''] = 'Selectionner un formulaire';
	foreach ($options_forms_obj as $page) {
		$options_forms[$page->id] = $page->title;
	}

	// Pull all the pages into an array
	$options_categories = [];
	// $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$terms = get_terms('category', ['hide_empty' => false]);
	$options_categories[''] = 'Selectionner une catégorie';
	foreach ($terms as $term) {
		$options_categories[$term->term_id] = $term->name;
	}

	$args = [
		'post_type' => 'block',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC',
		'suppress_filters' => 0,
	];
	$blocks = get_posts($args);
	$options_blocks[''] = 'Selectionner un bloc de contenu';
	foreach ($blocks as $block) {
		$options_blocks[$block->ID] = $block->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath = get_template_directory_uri() . '/images/';

	/**
	 * For $settings options see:
	 * http://codex.wordpress.org/Function_Reference/wp_editor
	 *
	 * 'media_buttons' are not supported as there is no post to attach items to
	 * 'textarea_name' is set by the 'id' you choose
	 */

	$wp_editor_settings = [
		'wpautop' => true, // Default
		'textarea_rows' => 5,
		'tinymce' => ['plugins' => 'wordpress,wplink'],
		'media_buttons' => true,
	];

	///////

	$options = [];

	$options[] = [
		'name' => __('Général', 'options_check'),
		'type' => 'heading',
	];

	// $options[] = [
	// 	'name' => __('Page Contact', 'options_check'),
	// 	'desc' => '',
	// 	'id' => 'contact_page_id',
	// 	'type' => 'select',
	// 	'options' => $options_pages,
	// ];

	// $options[] = [
	// 	'name' => __('Page Actualités', 'options_check'),
	// 	'desc' => '',
	// 	'id' => 'news_page_id',
	// 	'type' => 'select',
	// 	'options' => $options_pages,
	// ];

	// $options[] = [
	// 	'name' => __('Bloc page Actualités', 'options_check'),
	// 	'desc' => '',
	// 	'id' => 'blog_block_id',
	// 	'type' => 'select',
	// 	'options' => $options_blocks,
	// ];

	// $options[] = [
	// 	'name' => __('Bloc bas de page Actualité', 'options_check'),
	// 	'desc' => '',
	// 	'id' => 'blog_bottom_block_id',
	// 	'type' => 'select',
	// 	'options' => $options_blocks,
	// ];

	$options[] = [
		'name' => __('Contenu page 404', 'options_check'),
		'desc' => '',
		'id' => 'page_404',
		'type' => 'select',
		'options' => $options_blocks,
	];

	$options[] = [
		'name' => __('Pied de page: Informations de contact', 'options_check'),
		'id' => 'footer-5',
		'type' => 'textarea',
	];

	//

	$options[] = [
		'name' => __('Apparence', 'options_check'),
		'type' => 'heading',
	];

	$options[] = [
		'name' => __('Logo', 'options_check'),
		'id' => 'logo1x',
		'std' => '',
		'type' => 'upload',
	];

	$options[] = [
		'name' => __('Logo Retina', 'options_check'),
		'id' => 'logo2x',
		'std' => '',
		'type' => 'upload',
	];

	//

	$options[] = [
		'name' => __('Liens sociaux', 'options_check'),
		'type' => 'heading',
	];

	$options[] = [
		'name' => __('URL page LinkedIn', 'options_check'),
		'id' => 'linkedin_url',
		'std' => '',
		'type' => 'text',
	];

	$options[] = [
		'name' => __('URL page Facebook', 'options_check'),
		'id' => 'facebook_url',
		'std' => '',
		'type' => 'text',
	];

	$options[] = [
		'name' => __('URL page Twitter', 'options_check'),
		'id' => 'twitter_url',
		'std' => '',
		'type' => 'text',
	];

	//

	$options[] = [
		'name' => __('Codes js/css', 'options_check'),
		'type' => 'heading',
	];

	$options[] = [
		'name' => __('Code Google Analytics / Google Tag Manager', 'options_check'),
		'desc' => __("Code fourni pour l'intégration de GA ou GTM", 'options_check'),
		'id' => 'analytics_code',
		'std' => '',
		'type' => 'textarea',
	];

	$options[] = [
		'name' => __('Custom css code', 'options_check'),
		'desc' => '',
		'id' => 'custom_css_code',
		'std' => '',
		'type' => 'textarea',
	];

	$options[] = [
		'name' => __('Custom javascript code (avant /head)', 'options_check'),
		'desc' => '',
		'id' => 'custom_js_code_head',
		'std' => '',
		'type' => 'textarea',
	];

	$options[] = [
		'name' => __('Custom javascript code (avant /body)', 'options_check'),
		'desc' => '',
		'id' => 'custom_js_code',
		'std' => '',
		'type' => 'textarea',
	];

	return $options;
}

/*
 * Override default filter 'textarea' sanitization and $allowedposttags + embed and script.
 */
add_action('admin_init', 'optionscheck_change_santiziation', 100);

function optionscheck_change_santiziation()
{
	remove_filter('of_sanitize_textarea', 'of_sanitize_textarea');
	add_filter('of_sanitize_textarea', 'spre_sanitize_textarea');
}

function spre_sanitize_textarea($input)
{
	global $allowedposttags;
	$custom_allowedtags['embed'] = [
		'src' => [],
		'type' => [],
		'allowfullscreen' => [],
		'allowscriptaccess' => [],
		'height' => [],
		'width' => [],
	];
	$custom_allowedtags['script'] = [
		'src' => [],
		'type' => [],
		'async' => [],
		'allowfullscreen' => [],
		'allowscriptaccess' => [],
		'height' => [],
		'width' => [],
	];

	$custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
	$output = wp_kses($input, $custom_allowedtags);
	return $output;
}
