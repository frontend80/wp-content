<?php

/* Visual Composer Shortcodes mapper */

function taxonomy_choices($taxonomy){
   $terms = get_terms($taxonomy, array('hide_empty' => false));
   $options = array('- Choisir -' => '');
   foreach ($terms as $term) {
      $options[$term->name] = $term->term_id;
   }
   return $options;
}

function integer_choices($start, $ends){
   $choices = [];
   foreach (range($start, $ends) as $number) {
      $choices[$number] = $number;
  }
  return $choices;
}

function gravity_form_choices(){
   $choices = array('- Choisir -' => '');
   if (class_exists('RGFormsModel')) {
      $forms = RGFormsModel::get_forms();
      foreach ($forms as $form){
         $choices[$form->title] = $form->id;
      }
   }
   return $choices;
}

function post_type_choices($post_type){   
   $args = array( 'post_type' => $post_type, 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'suppress_filters' => 0 );
   $blocks = get_posts( $args );
   $result = array();
   foreach ($blocks as $block) {
      $result[$block->post_title] = $block->ID;
   }
   return $result;
}


add_action( 'vc_before_init', 'spre_integrateWithVC' );
function spre_integrateWithVC() {
   
   // $args = array( 'post_type' => 'block', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'suppress_filters' => 0 );
   // $blocks = get_posts( $args );
   // $options_blocks = array();
   // foreach ($blocks as $block) {
   //    $options_blocks[$block->post_title] = $block->ID;
   // }
   
   // vc_map( array(
   //    "name" => "Bloc de contenu",
   //    "base" => "block",
   //    "icon" => "block",
   //    "weight" => 30,
   //    "class" => "",
   //    "category" => "La SPRE",
   //    "params" => array(
   //       array(
   //          "type" => "dropdown",
   //          "class" => "",
   //          "heading" => "Bloc",
   //          "param_name" => "id",
   //          "value" => $options_blocks
   //       ),
   //    ),
   // ) );

   // $color_choices = array(
   //    "- Par défaut -" => "",
   //    "Orange" => "orange",
   // );
   
   // vc_map( array(
   //  "name" => "Bouton",
   //  "base" => "button",
   //  "icon" => "btn",
   //  "weight" => 20,
   //  "class" => "",
   //  "category" => "La SPRE",
   //  "params" => array(
   //    array(
   //       "type" => "textfield",
   //       "class" => "",
   //       "heading" => "URL",
   //       "param_name" => "url",
   //       "value" => ""
   //    ),
   //    array(
   //       "type" => "textfield",
   //       "class" => "",
   //       "heading" => " - ou - ID Vidéo YouTube",
   //       "param_name" => "video_id",
   //       "value" => ""
   //    ),
   //     array(
   //        "type" => "textfield",
   //        "class" => "",
   //        "heading" => "Texte",
   //        "param_name" => "content",
   //        "value" => ""
   //     ),
   //     /* array(
   //        "type" => "dropdown",
   //        "class" => "",
   //        "heading" => "Couleur",
   //        "param_name" => "color",
   //        "value" => $color_choices
   //     ), */
   //     array(
   //        "type" => "dropdown",
   //        "class" => "",
   //        "heading" => "Cible",
   //        "param_name" => "target",
   //        "value" => array(
   //            "Page courante" => "_self",
   //            "Nouvel onglet" => "_blank"
   //        )
   //     ),
   //     array(
   //        "type" => "textfield",
   //        "class" => "",
   //        "heading" => "Classe additionnelle",
   //        "param_name" => "style",
   //        "value" => "",
   //     ),
   //  ),
   // ));
    
   // vc_map( array(
   //    "name" => "Actualités",
   //    "base" => "news",
   //    "icon" => "news",
   //    "weight" => 20,
   //    "category" => "La SPRE",
   //    "show_settings_on_create" => false
   // ) );

   // vc_map( array(
   //    "name" => "Bloc Texte / image",
   //    "base" => "image_content",
   //    "icon" => "imageContent",
   //    "weight" => 20,
   //    "class" => "",
   //    "category" => "La SPRE",
   //    // 'as_parent' => array('except' => ''),
   //    // "content_element" => true,
   //    // "is_container" => true,
   //    "params" => array(
   //       array(
   //          "type" => "textarea_html",
   //          "class" => "",
   //          "heading" => "Contenu",
   //          "param_name" => "content",
   //          "value" => "",
   //       ),
   //       array(
   //           "type" => "attach_image",
   //           "class" => "",
   //           "heading" => "Photo",
   //           "param_name" => "image",
   //           "value" => "",
   //       ),
   //       array(
   //          "type" => "textarea",
   //          "class" => "",
   //          "heading" => "Légende de l'image",
   //          "param_name" => "image_legend",
   //          "value" => "",
   //       ),
   //       array(
   //          "type" => "dropdown",
   //          "class" => "",
   //          "heading" => "Type",
   //          "param_name" => "type",
   //          "value" => array(
   //             "Image de fond - Texte gauche 1/2" => "imageBg_txtLeft12",
   //             "Image de fond - Texte gauche 1/3" => "imageBg_txtLeft13",
   //             "Image de fond - Texte bas" => "imageBg_txtBottom",
   //             "Image gauche - Texte droite 2/3" => "imageLeft_textRight23",
   //             "Texte gauche - Image droite 2/3" => "textLeft_imageRight23",
   //          )
   //       ),
   //       array(
   //          "type" => "textfield",
   //          "class" => "",
   //          "heading" => "Classe additionnelle",
   //          "param_name" => "style",
   //          "value" => "",
   //       ),
   //    ),
   //    // "custom_markup" => ''
   // ) );

   // vc_map( array(
   //    "name" => "Vidéo Viméo",
   //    "base" => "spre_video",
   //    "icon" => "video",
   //    "weight" => 20,
   //    "class" => "",
   //    "category" => "La SPRE",
   //    "params" => array(
   //       array(
   //          "type" => "textfield",
   //          "class" => "",
   //          "heading" => "ID Vidéo Vimeo",
   //          "param_name" => "video_id",
   //          "value" => '',
   //          "description" => "ex: 33918228"
   //       ),
   //       array(
   //          "type" => "textfield",
   //          "class" => "",
   //          "heading" => "Largeur",
   //          "param_name" => "width",
   //          "value" => '100%',
   //          "description" => ""
   //       ),
   //       array(
   //          "type" => "textfield",
   //          "class" => "",
   //          "heading" => "Hauteur",
   //          "param_name" => "height",
   //          "value" => 'auto',
   //          "description" => ""
   //       ),
   //       // array(
   //          // "type" => "checkbox",
   //          // "class" => "",
   //          // "heading" => "Lecture automatique",
   //          // "param_name" => "autoplay",
   //          // "value" => "",
   //       // ),
   //       array(
   //          "type" => "attach_image",
   //          "class" => "",
   //          "heading" => "Image de couverture",
   //          "param_name" => "cover",
   //          "value" => "",
   //       ),
   //    ),
   // ) );

   vc_map( array(
      "name" => "Citation",
      "base" => "spre_quote",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_quote",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Citation",
            "param_name" => "spre_text",
            "value" => "",
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Mission",
      "base" => "spre_mission",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_mission",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Chapeau",
            "param_name" => "spre_hat",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
             "type" => "attach_image",
             "class" => "",
             "heading" => "Icon",
             "param_name" => "spre_icon",
             "value" => "",
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Chiffres Clés",
      "base" => "spre_keyNumbers",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_keyNumbers",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Chapeau",
            "param_name" => "spre_hat",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Chiffre 1",
            "param_name" => "spre_number1",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu 1",
            "param_name" => "spre_content1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Chiffre 2",
            "param_name" => "spre_number2",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu 2",
            "param_name" => "spre_content2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Chiffre 3",
            "param_name" => "spre_number3",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu 3",
            "param_name" => "spre_content3",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Chiffre 4",
            "param_name" => "spre_number4",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu 4",
            "param_name" => "spre_content4",
            "value" => "",
         ),
         array(
             "type" => "attach_image",
             "class" => "",
             "heading" => "Icon",
             "param_name" => "spre_icon",
             "value" => "",
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Bandeau Petit",
      "base" => "spre_alertSmall",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_alertSmall",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon",
            "param_name" => "spre_icon",
            "value" => "",
        ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton",
            "param_name" => "spre_button",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton",
            "param_name" => "spre_button_link",
            "value" => "",
         ),
         array(
            "type" => "dropdown",
            "class" => "",
            "heading" => "Style",
            "param_name" => "spre_button_style",
            'value'       => array(
               'Rouge' => 'red',
               'Violet' => 'purple',
            ),
            'std'         => 'Rouge',
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Bandeau",
      "base" => "spre_alertBig",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_alertBig",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton",
            "param_name" => "spre_button",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton",
            "param_name" => "spre_button_link",
            "value" => "",
         ),
         array(
            "type" => "dropdown",
            "class" => "",
            "heading" => "Style",
            "param_name" => "spre_button_style",
            'value'       => array(
               'Rouge' => 'red',
               'Violet' => 'purple',
            ),
            'std'         => 'Rouge',
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Bloc Aide",
      "base" => "spre_helpBlock",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_helpBlock",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre 1",
            "param_name" => "spre_title1",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon 1",
            "param_name" => "spre_icon1",
            "value" => "",
        ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu 1",
            "param_name" => "spre_content1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton 1",
            "param_name" => "spre_button1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton 1",
            "param_name" => "spre_button_link1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre 2",
            "param_name" => "spre_title2",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon 2",
            "param_name" => "spre_icon2",
            "value" => "",
        ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu 2",
            "param_name" => "spre_content2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton 2",
            "param_name" => "spre_button2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton 2",
            "param_name" => "spre_button_link2",
            "value" => "",
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Block Logos",
      "base" => "spre_logoBlock",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_logoBlock",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Logo 1",
            "param_name" => "spre_logo1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Logo 1",
            "param_name" => "spre_logo_link1",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Logo 2",
            "param_name" => "spre_logo2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Logo 2",
            "param_name" => "spre_logo_link2",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Logo 3",
            "param_name" => "spre_logo3",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Logo 3",
            "param_name" => "spre_logo_link3",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Logo 4",
            "param_name" => "spre_logo4",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Logo 4",
            "param_name" => "spre_logo_link4",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Logo 5",
            "param_name" => "spre_logo5",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Logo 5",
            "param_name" => "spre_logo_link5",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Logo 6",
            "param_name" => "spre_logo6",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Logo 6",
            "param_name" => "spre_logo_link6",
            "value" => "",
         ),
      ),
      // "custom_markup" => ''
   ) );


   vc_map( array(
      "name" => "Appel Action",
      "base" => "spre_cta",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_cta",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon",
            "param_name" => "spre_icon",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton",
            "param_name" => "spre_button",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton",
            "param_name" => "spre_button_link",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton 2",
            "param_name" => "spre_button2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton 2",
            "param_name" => "spre_button_link2",
            "value" => "",
         ),
      ),
      // "custom_markup" => ''
   ) );

   vc_map( array(
      "name" => "Liste Contenu",
      "base" => "spre_content_list",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "spre_content_list",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "loop",
            "class" => "",
            "heading" => "Liste",
            "param_name" => "spre_list",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon",
            "param_name" => "spre_icon",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre Bouton",
            "param_name" => "spre_button",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Lien Bouton",
            "param_name" => "spre_button_link",
            "value" => "",
         )
      ),
      // "custom_markup" => ''
   ) );
   
   vc_map( array(
      "name" => "Utilisateurs",
      "base" => "spre_user_block",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 30,
      "class" => "",
      "category" => "La SPRE",
   ) );

   vc_map( array(
      "name" => "Centre Aide",
      "base" => "spre_help_block",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 30,
      "class" => "",
      "category" => "La SPRE",
   ) );

   vc_map( array(
      "name" => "Téléchargements",
      "base" => "spre_file_block",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 30,
      "class" => "",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon",
            "param_name" => "spre_icon",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Filtre Catégorie ID",
            "param_name" => "spre_cat_ID",
            "value" => "",
         ),
      ),
   ) );

   vc_map( array(
      "name" => "Informations",
      "base" => "spre_news_block",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 30,
      "class" => "",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "attach_image",
            "class" => "",
            "heading" => "Icon",
            "param_name" => "spre_icon",
            "value" => "",
         ),
         array(
            "type" => "textarea",
            "class" => "",
            "heading" => "Contenu",
            "param_name" => "spre_content",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Filtre Catégorie ID",
            "param_name" => "spre_cat_ID",
            "value" => "",
         ),
      ),
   ) );

   vc_map( array(
      "name" => "Menu Ancre",
      "base" => "spre_anchor_menu",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Titre",
            "param_name" => "spre_title",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Menu 1",
            "param_name" => "spre_menu1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "ID Menu 1",
            "param_name" => "spre_anchor1",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Menu 2",
            "param_name" => "spre_menu2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "ID Menu 2",
            "param_name" => "spre_anchor2",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Menu 3",
            "param_name" => "spre_menu3",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "ID Menu 3",
            "param_name" => "spre_anchor3",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Menu 4",
            "param_name" => "spre_menu4",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "ID Menu 4",
            "param_name" => "spre_anchor4",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Menu 5",
            "param_name" => "spre_menu5",
            "value" => "",
         ),
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "ID Menu 5",
            "param_name" => "spre_anchor5",
            "value" => "",
         ),
      ),
   ) );
   
   
   vc_map( array(
      "name" => "Barème",
      "base" => "spre_scale",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "class" => "",
      "category" => "La SPRE",
      "params" => array(
         array(
            "type" => "dropdown",
            "class" => "",
            "heading" => "Barème",
            "param_name" => "id",
            "value" => post_type_choices('scale')
         ),
      ),
      // 'custom_markup' => '<div class="vc_custom-element-container">Bloc de contenu #% params.id %</div>'
   ) );

   vc_map( array(
      "name" => "Barème - Bloc",
      "base" => "spre_scale_block",
      "as_parent" => array('except' => 'spre_scaleBlock'),
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "category" => "La SPRE",
      "content_element" => true,
      "is_container" => false,
      "js_view" => 'VcColumnView',
      "show_settings_on_create" => false,
   ) );
   vc_map( array(
      "name" => "Barème - Bloc de contenu",
      "base" => "spre_scale_content",
      "as_parent" => array('except' => ''),
      "as_child" => array('only' => 'spre_scaleBlock'),
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "category" => "La SPRE",
      "content_element" => true,
      "is_container" => false,
      "js_view" => 'VcColumnView',
      "show_settings_on_create" => false,
   ) );
   vc_map( array(
      "name" => "Barème - Séparateur",
      "base" => "spre_scale_separator",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "category" => "La SPRE",
      // "content_element" => true,
      "params" => array(
         array(
            "type" => "textfield",
            "class" => "",
            "heading" => "Texte",
            "param_name" => "content",
            "value" => ""
         ),
      )
   ) );
   vc_map( array(
      "name" => "Barème - Rémunération",
      "base" => "spre_scale_remuneration",
      "icon" => get_template_directory_uri().'/assets/images/svg/icons/spre.svg',
      "weight" => 20,
      "category" => "La SPRE",
      // "content_element" => true,
      "params" => array(
         array(
            "type" => "textarea_html",
            "class" => "",
            "heading" => "Texte",
            "param_name" => "content",
            "value" => "",
            "description" => "Une ligne par élément de l'opération",
         ),
      )
   ) );

   if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
      class WPBakeryShortCode_Spre_scale_block extends WPBakeryShortCodesContainer {}
   }
   if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
      class WPBakeryShortCode_Spre_scale_content extends WPBakeryShortCodesContainer {}
   }
   //  if ( class_exists( 'WPBakeryShortCode' ) ) {
   //    class WPBakeryShortCode_Spoiler extends WPBakeryShortCode {
   //    }
   //  }
}