<?php

/* Custom Post types pour la gestion des Blocs de contenus  */

add_action('init', 'spre_create_block');
function spre_create_block() {
    $block_args = array(
        'labels' => array(
            'name' => __( 'Blocs de contenu'),
            'all_items' => __( 'Tous les Blocs'),
            'singular_name' => __( 'Bloc' ),
            'add_new' => __( 'Ajouter un Bloc' ),
            'add_new_item' => __( 'Ajouter un Bloc' ),
            'edit_item' => __( "Editer le Bloc" ),
            'new_item' => __( 'Ajouter un Bloc' ),
            'view_item' => __( "Voir le Bloc" ),
            'search_items' => __( "Rechercher parmi les Blocs" ),
            'not_found' => __( 'Aucun Bloc trouvé' ),
            'not_found_in_trash' => __( 'Aucun Bloc trouvé dans la corbeille' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-text',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'has_archive' => false,
        'has_archive' => false,
        'rewrite' => false,
        'supports' => array('title', 'editor' ),
        'taxonomies' => array(),
    );
    register_post_type('block', $block_args);
}