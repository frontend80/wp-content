<?php

/* Custom Post types pour la gestion des téléchargements  */

add_action('init', 'spre_create_downloads');
function spre_create_downloads() {
    $downloads_args = array(
        'labels' => array(
            'name' => __( 'Téléchargements'),
            'all_items' => __( 'Tous les téléchargements'),
            'singular_name' => __( 'téléchargement' ),
            'add_new' => __( 'Ajouter un téléchargement' ),
            'add_new_item' => __( 'Ajouter un téléchargement' ),
            'edit_item' => __( "Editer l'téléchargement" ),
            'new_item' => __( 'Ajouter un téléchargement' ),
            'view_item' => __( "Voir l'téléchargement" ),
            'search_items' => __( "Rechercher parmi les téléchargements" ),
            'not_found' => __( 'Aucun téléchargement trouvé' ),
            'not_found_in_trash' => __( 'Aucun téléchargement trouvé dans la corbeille' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-download',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'has_archive' => false,
        'rewrite' => false,
        'supports' => array('title', 'editor', 'excerpt', 'page-attributes',), // 'custom-fields' ),
        'taxonomies' => array(),
    );
    register_post_type('downloads', $downloads_args);
}