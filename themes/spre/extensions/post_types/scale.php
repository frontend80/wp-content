<?php

/* Custom Post types pour la gestion des barèmes  */

add_action('init', 'spre_create_scale');
function spre_create_scale() {
    $scale_args = array(
        'labels' => array(
            'name' => __( 'Barèmes'),
            'all_items' => __( 'Tous les barèmes'),
            'singular_name' => __( 'barème' ),
            'add_new' => __( 'Ajouter un barème' ),
            'add_new_item' => __( 'Ajouter un barème' ),
            'edit_item' => __( "Editer l'barème" ),
            'new_item' => __( 'Ajouter un barème' ),
            'view_item' => __( "Voir l'barème" ),
            'search_items' => __( "Rechercher parmi les barèmes" ),
            'not_found' => __( 'Aucun barème trouvé' ),
            'not_found_in_trash' => __( 'Aucun barème trouvé dans la corbeille' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-calculator',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'has_archive' => false,
        'rewrite' => false,
        'supports' => array('title', 'editor'),
        'taxonomies' => array(),
    );
    register_post_type('scale', $scale_args);
}