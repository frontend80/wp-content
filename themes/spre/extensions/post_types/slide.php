<?php

/* Custom Post types pour la gestion des slides  */

add_action('init', 'spre_create_slide');
function spre_create_slide() {
    $slide_args = array(
        'labels' => array(
            'name' => __( 'Slider'),
            'all_items' => __( 'Tous les slides'),
            'singular_name' => __( 'Slide' ),
            'add_new' => __( 'Ajouter un slide' ),
            'add_new_item' => __( 'Ajouter un slide' ),
            'edit_item' => __( "Editer le slide" ),
            'new_item' => __( 'Ajouter un slide' ),
            'view_item' => __( "Voir le slide" ),
            'search_items' => __( "Rechercher parmi les slides" ),
            'not_found' => __( 'Aucun slide trouvé' ),
            'not_found_in_trash' => __( 'Aucun slide trouvé dans la corbeille' )
        ),
        'public' => false,
        'menu_icon' => 'dashicons-slides',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'has_archive' => false,
        'has_archive' => false,
        'rewrite' => false,
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes' ),
        'taxonomies' => array(),
    );
    register_post_type('slide', $slide_args);
}

// Add the posts and pages columns filter. They can both use the same function.
add_filter('manage_slide_posts_columns', 'spre_slide_edit_columns', 1);
// Add the column
function spre_slide_edit_columns($cols){
    $custom_cols = array(
        'id' => __('ID'),
        'post_thumb' => __('Image'),
        'title' => __('Nom')
    );
    $cols = array_merge( $cols, $custom_cols );
    return $cols;
}
// Hook into the posts an pages column managing. Sharing function spre_callback again.
add_action('manage_slide_posts_custom_column', 'spre_display_slide_column', 1, 2);
// Grab featured-thumbnail size post thumbnail and display it.
function spre_display_slide_column($col, $id){
  switch($col){
    case 'id':
      echo $id;
      break;
    case 'post_thumb':
      if( function_exists('the_post_thumbnail') )
        echo the_post_thumbnail('thumbnail');
      else
        echo 'Not supported in theme';
      break;
    case 'title':
      the_title();
      break;
  }
}

//Add Meta Boxes
add_action( 'add_meta_boxes', 'spre_slide_meta_box_add' );
function spre_slide_meta_box_add()
{
    add_meta_box( 'meta-box-link', 'Informations supplémentaires', 'spre_slide_meta_box', 'slide', 'normal', 'high' );
}

function spre_slide_meta_box( $post )
{
    $button_text = get_post_meta($post->ID, 'button_text', true);
    $button_url = get_post_meta($post->ID, 'button_url', true);
    $text_color = get_post_meta($post->ID, 'text_color', true);
    wp_nonce_field( 'spre_meta_box_nonce', 'meta_box_nonce' );
    
    $args = array( 'post_type' => 'customer', 'post_status' => 'any', 'posts_per_page' => -1, 'suppress_filters' => 0 );
    ?>
    
    <div class="customFields">
        <p>
            <label for="button_text" style="width: 120px; display: inline-block;">Texte du bouton</label>&nbsp;
            <input type="text" name="button_text" id="button_text" value="<?= $button_text; ?>" style="width:350px" />
        </p>
        <p>
            <label for="button_url" style="width: 120px; display: inline-block;">URL du bouton</label>&nbsp;
            <input type="text" name="button_url" id="button_url" value="<?= $button_url; ?>" style="width:350px" />
        </p>
        <p>
            <label for="text_color" style="width: 120px; display: inline-block;">Couleur du texte</label>&nbsp;
            <select name="text_color">
                <option value="light" <?= !$text_color || $text_color == 'light' ? 'selected="selected"' : '' ; ?>>Clair</option>
                <option value="dark" <?= $text_color == 'dark' ? 'selected="selected"' : '' ; ?>>Foncé</option>
            </select>
        </p>
    </div>

    <?php	
}

add_action( 'save_post', 'spre_slide_meta_box_save' );
function spre_slide_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'spre_meta_box_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_posts' ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'strong' => array(),
        'em'     => array(),
        'b'      => array(),
        'i'      => array(),
        'div'      => array('style' => array()),
        'span'      => array('style' => array()),
        'a' => array( // on allow a tagsun ref
            'href' => array() // and those anchors can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set
    update_post_meta( $post_id, 'button_text', $_POST['button_text'] );
    update_post_meta( $post_id, 'button_url', $_POST['button_url'] );
    update_post_meta( $post_id, 'text_color', $_POST['text_color'] );
}