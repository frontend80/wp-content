<?php

/* Custom Post types pour la gestion du centre d'aide  */

add_action('init', 'fdr_create_help_center');
function fdr_create_help_center() {
    $help_center_args = array(
        'labels' => array(
            'name' => __( "Centre d'aide"),
            'all_items' => __( 'Tous les éléments'),
            'singular_name' => __( 'élément' ),
            'add_new' => __( 'Ajouter un élément' ),
            'add_new_item' => __( 'Ajouter un élément' ),
            'edit_item' => __( "Editer l'élément" ),
            'new_item' => __( 'Ajouter un élément' ),
            'view_item' => __( "Voir l'élément" ),
            'search_items' => __( "Rechercher parmi les éléments" ),
            'not_found' => __( 'Aucun élément trouvé' ),
            'not_found_in_trash' => __( 'Aucun élément trouvé dans la corbeille' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-info',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'has_archive' => false,
        'rewrite' => array('slug' => 'éléments'),
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes',), // 'custom-fields' ),
        'taxonomies' => array(),
    );
    register_post_type('help_center', $help_center_args);
}