<?php

/* Custom Post types pour la gestion des utilisateurs  */

add_action('init', 'spre_create_spre_user');
function spre_create_spre_user() {
    $spre_user_args = array(
        'labels' => array(
            'name' => __( 'Utilisateurs'),
            'all_items' => __( 'Tous les utilisateurs'),
            'singular_name' => __( 'utilisateur' ),
            'add_new' => __( 'Ajouter un utilisateur' ),
            'add_new_item' => __( 'Ajouter un utilisateur' ),
            'edit_item' => __( "Editer l'utilisateur" ),
            'new_item' => __( 'Ajouter un utilisateur' ),
            'view_item' => __( "Voir l'utilisateur" ),
            'search_items' => __( "Rechercher parmi les utilisateurs" ),
            'not_found' => __( 'Aucun utilisateur trouvé' ),
            'not_found_in_trash' => __( 'Aucun utilisateur trouvé dans la corbeille' )
        ),
        'public' => true,
        'menu_icon' => 'dashicons-groups',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'capability_type' => 'page',
        'hierarchical' => false,
        'has_archive' => false,
        'rewrite' => array('slug' => 'utilisateurs'),
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes',), // 'custom-fields' ),
        'taxonomies' => array(),
    );
    register_post_type('spre_user', $spre_user_args);
}