<?php

/**************** UTILS ******************/


function spre_shortcode_empty_paragraph_fix( $content ) {
    // define your shortcodes to filter, '' filters all shortcodes
    $shortcodes = array( '' );
    foreach ( $shortcodes as $shortcode ) {
        $array = array (
            '<p>[' . $shortcode => '[' .$shortcode,
            '<p>[/' . $shortcode => '[/' .$shortcode,
            $shortcode . ']</p>' => $shortcode . ']',
            $shortcode . ']<br />' => $shortcode . ']'
        );
        $content = strtr( $content, $array );
    }
    return $content;
}
add_filter( 'the_content', 'spre_shortcode_empty_paragraph_fix' );


/**************** SHORT CODES ******************/

function space( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'height' => '',
        'tablet' => '',
        'mobile' => '',
        'style' => '',
    ), $atts));
    $height = str_replace('px', '', $height);
    $property = intval($height) < 0 ? 'margin-top' : 'padding-top';
    $output = '<div class="clear espace '.$style.'" style=":'.$property.' '.$height.'px!important;" data-desktop-height="'.$height.'" data-tablet-height="'.$tablet.'" data-mobile-height="'.$mobile.'" ></div>';
    $output = '<div class="clear espace '.$style.'" style="'.$property.': '.$height.'px!important;" data-desktop-height="'.$height.'" data-tablet-height="'.$tablet.'" data-mobile-height="'.$mobile.'" ></div>';
    return $output;
}
add_shortcode('espace', 'space');
add_shortcode('space', 'space');

function button( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'url' => '',
		'href' => '',
		'video_id' => '',
		'style' => '',
		'color' => '',
		'target' => '',
		'width' => '',
		'class' => '',
		'title' => '', // Sebac compatibility
		'link' => '', // Sebac compatibility
	), $atts ) );
    $url = $url ? $url : $href;
    $url = $url ? $url : $link;
    $content = $content ? $content : $title;
    $width = $width ? 'width: '.$width.'px' : '';
    // return '<a href="'.$url.'" target="'.$target.'" class="button pRight '.$color.' '.$style.'" style="'.$width.'"><span class="content">' . $content . '</span><i class="pictoMore"></i></a>';
    if  ($video_id){
        $href = 'href="javascript:;" data-video-id="'.$video_id.'"';
        $style .= ' popupVideoLink';
    }else{
        $href = 'href="'.$url.'"';
    }
    $colorClass = '';
    if ($color === 'gris') {
        $colorClass = "text-spre-white bg-spre-brown_dark border border-transparent hover:bg-spre-brown";
    }
    if ($color === 'blanc') {
        $colorClass = "text-spre-brown_dark bg-spre-white border border-spre-brown_dark hover:bg-spre-brown_ultralight";
    }
	return '<a '.$href.' target="'.$target.'" class="inline-flex items-center justify-center py-2 mr-4 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out rounded-md px-9 '.$colorClass.' '.$style.' '.$class.'" style="'.$width.'">' . $content . '</a>';
}
add_shortcode('button', 'button');

function icon( $atts, $content = null ) {// Sebac compatibility
    return '';
}
add_shortcode('icon', 'icon');

function link_( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'url' => '',
		'href' => '',
		'style' => '',
		'color' => '',
		'target' => '',
		'width' => '',
		'class' => '',
	), $atts ) );
    $url = $url ? $url : $href;
	return '<a href="'.$url.'" target="'.$target.'" class="link '.$color.' '.$style.' '.$class.'">' . $content . spre_svg('arrow_right') . '</a>';
}
add_shortcode('link', 'link_');

function download( $atts, $content = null ) {
	extract( shortcode_atts( array(
        // 'file' => '',
        'image' => '',
		'style' => '',
		// 'color' => '',
		// 'title' => '',
    ), $atts ) );
    if ( $image ){        
        $image_url = wp_get_attachment_image_src($image, 'full');
        $image_url = $image_url[0];
        $download_url = spre_download_url($image_url);
        ob_start();
        include(locate_template('includes/download.php'));
        $res = ob_get_contents(); ob_end_clean();
        return $res;
    }
    return '- Aucune image -';
}
add_shortcode('download', 'download');

function block( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'slug' => '',
		'id' => '',
	), $atts ) );
    ob_start();
    
    if ($slug){ $block = get_page_by_path($slug, OBJECT, 'block'); $id = $block->ID; }
    // else if ($id){ $block = get_post($id); }
    
    if ($id){
        
        if (function_exists('icl_object_id')){
             $id = icl_object_id($id, 'block', true);
        }
        $block = get_post($id);
        
        if ($block){
            echo apply_filters('the_content', $block->post_content);
            // echo do_shortcode($block->post_content);
        }
    }
    
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}
add_shortcode('block', 'block');
add_shortcode('block2', 'block');

function slider( $atts, $content = null ) {
    ob_start();
    include(locate_template('includes/slider.php'));
    $res = ob_get_contents(); ob_end_clean();
    return $res;
}
add_shortcode('slider', 'slider');

function featured_news($atts, $content = null) {
    ob_start();
    
    $current_tag = isset($_GET['tag']) ? $_GET['tag'] : '';
    $current_cat = isset($_GET['cat']) ? $_GET['cat'] : '';
    $current_year = isset($_GET['y']) ? $_GET['y'] : '';
    $count = isset($_GET['count']) ? $_GET['count'] : get_option('posts_per_page');
    
    $posts = spre_get_posts($count, 0, $current_tag, $current_cat, $current_year);

    $featured = true;
    include(locate_template('includes/news.php'));
    echo '<div class="clear"></div>';
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}
add_shortcode('featured_news', 'featured_news');

function news($atts, $content = null) {
    ob_start();
    
    $current_tag = isset($_GET['tag']) ? $_GET['tag'] : '';
    $current_cat = isset($_GET['cat']) ? $_GET['cat'] : '';
    $current_year = isset($_GET['y']) ? $_GET['y'] : '';
    $count = isset($_GET['count']) ? $_GET['count'] : get_option('posts_per_page');
    
    $posts = spre_get_posts($count, 0, $current_tag, $current_cat, $current_year);
    
    include(locate_template('includes/news.php'));
    echo '<div class="clear"></div>';
    $res = ob_get_contents();
    ob_end_clean();
    return $res;
}
add_shortcode('news', 'news');

function tabs( $atts, $content = null ) {
    return '<div class="oldTabs">'.do_shortcode($content).'</div>';
}
add_shortcode('tabs', 'tabs');

function tab($atts, $content = null) {
	extract( shortcode_atts( array(
		'title' => '',
	), $atts ) );
    return '<h2>'.$title.'</h2>'.do_shortcode($content);
}
add_shortcode('tab', 'tab');

function image_content( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'image' => '',
		'type' => 'imageBg_txtLeft12',
		'style' => '',
		'image_legend' => '',
	), $atts ) );
    $image_url = $image ? wp_get_attachment_image_src($image, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/image_content.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('image_content', 'image_content');

function spre_video($atts, $content = null) {
	extract( shortcode_atts( array(
		'video_id' => '',
		'width' => '100%',
		'height' => 'auto',
		'autoplay' => false,
        'cover' => '',
        'cover_url' => '',
    ), $atts ) );
    
    $res = '';
    if ($video_id){
        if (!$cover_url){
            $cover_url = $cover ? wp_get_attachment_image_src($cover, 'full')[0] : '';
        }
        
        ob_start();
        include(locate_template('includes/video_iframe.php'));
        echo '<div class="clear"></div>';
        $res = ob_get_contents();
        ob_end_clean();
    }
    return $res;
}
add_shortcode('spre_video', 'spre_video');

///////////////////////////////////////////////////////

function spre_quote( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_text' => '',
    ), $atts ) );
    ob_start();
    include(locate_template('includes/spre_quote.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_quote', 'spre_quote');

function spre_mission( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_hat' => '',
        'spre_content' => '',
        'spre_icon' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_mission.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_mission', 'spre_mission');

function spre_keyNumbers( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_hat' => '',
        'spre_number1' => '',
        'spre_content1' => '',
        'spre_number2' => '',
        'spre_content2' => '',
        'spre_number3' => '',
        'spre_content3' => '',
        'spre_number4' => '',
        'spre_content4' => '',
        'spre_icon' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_keyNumbers.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_keyNumbers', 'spre_keyNumbers');

function spre_alertBig( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_content' => '',
        'spre_button' => '',
        'spre_button_link' => '',
        'spre_button_style' => '',
    ), $atts ) );
    ob_start();
    include(locate_template('includes/spre_alertBig.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_alertBig', 'spre_alertBig');

function spre_alertSmall( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_content' => '',
        'spre_button' => '',
        'spre_button_link' => '',
        'spre_button_style' => '',
        'spre_icon' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_alertSmall.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_alertSmall', 'spre_alertSmall');

function spre_helpBlock( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_icon1' => '',
        'spre_title1' => '',
        'spre_content1' => '',
        'spre_button1' => '',
        'spre_button_link1' => '',
        'spre_icon2' => '',
        'spre_title2' => '',
        'spre_content2' => '',
        'spre_button2' => '',
        'spre_button_link2' => '',
    ), $atts ) );
    $spre_icon_url1 = $spre_icon1 ? wp_get_attachment_image_src($spre_icon1, 'full')[0] : '';
    $spre_icon_url2 = $spre_icon2 ? wp_get_attachment_image_src($spre_icon2, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_helpBlock.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_helpBlock', 'spre_helpBlock');

function spre_logoBlock( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_logo1' => '',
        'spre_logou_link1' => '',
        'spre_logo2' => '',
        'spre_logo_link2' => '',
        'spre_logo3' => '',
        'spre_logo_link3' => '',
        'spre_logo4' => '',
        'spre_logo_link4' => '',
        'spre_logo5' => '',
        'spre_logo_link5' => '',
        'spre_logo6' => '',
        'spre_logo_link6' => '',
    ), $atts ) );
    $spre_logo_url1 = $spre_logo1 ? wp_get_attachment_image_src($spre_logo1, 'full')[0] : '';
    $spre_logo_url2 = $spre_logo2 ? wp_get_attachment_image_src($spre_logo2, 'full')[0] : '';
    $spre_logo_url3 = $spre_logo3 ? wp_get_attachment_image_src($spre_logo3, 'full')[0] : '';
    $spre_logo_url4 = $spre_logo4 ? wp_get_attachment_image_src($spre_logo4, 'full')[0] : '';
    $spre_logo_url5 = $spre_logo5 ? wp_get_attachment_image_src($spre_logo5, 'full')[0] : '';
    $spre_logo_url6 = $spre_logo6 ? wp_get_attachment_image_src($spre_logo6, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_logoBlock.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_logoBlock', 'spre_logoBlock');

function spre_cta( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_content' => '',
        'spre_icon' => '',
        'spre_button' => '',
        'spre_button_link' => '',
        'spre_button2' => '',
        'spre_button_link2' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_cta.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_cta', 'spre_cta');

function spre_content_list( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_content' => '',
        'spre_icon' => '',
        'spre_button' => '',
        'spre_button_link' => '',
        'spre_list' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_content_list.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_content_list', 'spre_content_list');

function spre_user_block( $atts, $content = null ) {
    ob_start();
    include(locate_template('includes/spre_user_block.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_user_block', 'spre_user_block');

function spre_help_block( $atts, $content = null ) {
    ob_start();
    include(locate_template('includes/spre_help_block.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_help_block', 'spre_help_block');

function spre_file_block( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_content' => '',
        'spre_icon' => '',
        'spre_cat_ID' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_file_block.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_file_block', 'spre_file_block');

function spre_news_block( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_content' => '',
        'spre_icon' => '',
        'spre_cat_ID' => '',
    ), $atts ) );
    $spre_icon_url = $spre_icon ? wp_get_attachment_image_src($spre_icon, 'full')[0] : '';
    ob_start();
    include(locate_template('includes/spre_news_block.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_news_block', 'spre_news_block');

function spre_anchor_menu( $atts, $content = null ) {
	extract( shortcode_atts( array(
        'spre_title' => '',
        'spre_menu1' => '',
        'spre_anchor1' => '',
        'spre_menu2' => '',
        'spre_anchor2' => '',
        'spre_menu3' => '',
        'spre_anchor3' => '',
        'spre_menu4' => '',
        'spre_anchor4' => '',
        'spre_menu5' => '',
        'spre_anchor5' => '',
    ), $atts ) );
    ob_start();
    include(locate_template('includes/spre_anchor_menu.php'));
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('spre_anchor_menu', 'spre_anchor_menu');


function spre_scale_separator( $atts, $content = null ) {
    return '<div class="scaleSeparator">'.$content.'</div>';
}
add_shortcode('spre_scale_separator', 'spre_scale_separator');

function spre_scale_remuneration( $atts, $content = null ) {
    ob_start();
    include(locate_template('includes/spre_scale_remuneration.php'));
    $output = ob_get_contents();
    ob_end_clean();    
    return $output;
}
add_shortcode('spre_scale_remuneration', 'spre_scale_remuneration');

function spre_scale( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'id' => '',
	), $atts ) );
    if ($id){        
        if (function_exists('icl_object_id')){
            $id = icl_object_id($id, 'scale', true);
        }
        $scale = get_post($id);        
        if ($scale){
            return apply_filters('the_content', $scale->post_content);
        }
    }
    return '';
}
add_shortcode('spre_scale', 'spre_scale');