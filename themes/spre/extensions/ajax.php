<?php

add_action('wp_ajax_nopriv_get-posts', 'spre_get_posts');
add_action('wp_ajax_get-posts', 'spre_ajax_get_posts');
function spre_ajax_get_posts(){
    $offset = isset($_GET['offset']) ? $_GET['offset'] : '';
    $current_tag = isset($_GET['tag']) ? $_GET['tag'] : '';
    $current_cat = isset($_GET['cat']) ? $_GET['cat'] : '';
    $current_use = isset($_GET['user']) ? $_GET['user'] : '';
    $current_year = isset($_GET['y']) ? $_GET['y'] : '';
    $count = isset($_GET['count']) ? $_GET['count'] : get_option('posts_per_page');
    
    $posts = spre_get_posts($count, $offset, $current_tag, $current_cat, $current_use, $current_year);
    // print_r($posts);
    
    $featured = false;
    include(locate_template('includes/posts_loop_content.php'));
	
    die;
}