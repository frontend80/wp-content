<?php

// Add Meta Boxes
add_action( 'add_meta_boxes', 'spre_post_meta_box_add' );
function spre_post_meta_box_add()
{
    // add_meta_box( 'meta-box-post', 'Informations événement / formation', 'spre_post_meta_box', 'post', 'normal', 'high' );
    add_meta_box( 'meta-box-page', 'Informations supplémentaires', 'spre_page_meta_box', 'page', 'normal', 'high' );
}

/* POST */

function spre_post_meta_box( $post )
{
    $date_description = get_post_meta($post->ID, 'date_description', true);
    $date_begin = get_post_meta($post->ID, 'date_begin', true);
    $date_end = get_post_meta($post->ID, 'date_end', true);
    $can_subscribe = get_post_meta($post->ID, 'can_subscribe', true);

    wp_nonce_field( 'spre_post_meta_box_nonce', 'meta_box_nonce' );
?>

    <div class="customFields">
        <p>
            <label for="can_subscribe" style="width: 250px; display: inline-block;">
            <input type="checkbox" name="can_subscribe" id="can_subscribe" <?= $can_subscribe ? 'checked="checked"' : ''; ?> /> Inscription possible en ligne</label>&nbsp;
        </p>
        <p>
            <label style="width: 120px; display: inline-block;">Date de début de l'événement</label>&nbsp;
            <?php 
                if ($date_begin){
                    if ( gettype($date_begin) == "string" ){ $date_begin = DateTime::createFromFormat('Y/m/j', $date_begin); }
                    $date_begin_y = $date_begin->format('Y');
                    $date_begin_m = $date_begin->format('m');
                    $date_begin_d = $date_begin->format('j');
                }else{
                    $date_begin_y = get_the_date( 'Y', $post->ID );
                    $date_begin_m = get_the_date( 'm', $post->ID );
                    $date_begin_d = get_the_date( 'j', $post->ID );
                }
            ?>
            <input type="text" name="date_begin_d" value="<?= $date_begin_d; ?>" placeholder="jj" style="display: inline-block; width: 35px; vertical-align: middle;" />
            <select name="date_begin_m" style="display: inline-block; vertical-align: middle;">
                <option value="1" <?php if ($date_begin_m == '1'){ ?>selected="selected"<?php } ?>>janvier</option>
                <option value="2" <?php if ($date_begin_m == '2'){ ?>selected="selected"<?php } ?>>février</option>
                <option value="3" <?php if ($date_begin_m == '3'){ ?>selected="selected"<?php } ?>>mars</option>
                <option value="4" <?php if ($date_begin_m == '4'){ ?>selected="selected"<?php } ?>>avril</option>
                <option value="5" <?php if ($date_begin_m == '5'){ ?>selected="selected"<?php } ?>>mai</option>
                <option value="6" <?php if ($date_begin_m == '6'){ ?>selected="selected"<?php } ?>>juin</option>
                <option value="7" <?php if ($date_begin_m == '7'){ ?>selected="selected"<?php } ?>>juillet</option>
                <option value="8" <?php if ($date_begin_m == '8'){ ?>selected="selected"<?php } ?>>août</option>
                <option value="9" <?php if ($date_begin_m == '9'){ ?>selected="selected"<?php } ?>>septembre</option>
                <option value="10" <?php if ($date_begin_m == '10'){ ?>selected="selected"<?php } ?>>octobre</option>
                <option value="11" <?php if ($date_begin_m == '11'){ ?>selected="selected"<?php } ?>>novembre</option>
                <option value="12" <?php if ($date_begin_m == '12'){ ?>selected="selected"<?php } ?>>décembre</option>
            </select>
            <input type="text" name="date_begin_y" value="<?= $date_begin_y; ?>" placeholder="aaaa" style="display: inline-block; width: 50px; vertical-align: middle;" />
        </p>
        <p>
            <label style="width: 120px; display: inline-block;">Date de fin</label>&nbsp;
            <?php 
                if ($date_end){
                    if ( gettype($date_end) == "string" ){ $date_end = DateTime::createFromFormat('Y/m/j', $date_end); }
                    $date_end_y = $date_end->format('Y');
                    $date_end_m = $date_end->format('m');
                    $date_end_d = $date_end->format('j');
                }else{
                    $date_end_y = get_the_date( 'Y', $post->ID );
                    $date_end_m = get_the_date( 'm', $post->ID );
                    $date_end_d = get_the_date( 'j', $post->ID );
                }
            ?>
            <input type="text" name="date_end_d" value="<?= $date_end_d; ?>" placeholder="jj" style="display: inline-block; width: 35px; vertical-align: middle;" />
            <select name="date_end_m" style="display: inline-block; vertical-align: middle;">
                <option value="1" <?php if ($date_end_m == '1'){ ?>selected="selected"<?php } ?>>janvier</option>
                <option value="2" <?php if ($date_end_m == '2'){ ?>selected="selected"<?php } ?>>février</option>
                <option value="3" <?php if ($date_end_m == '3'){ ?>selected="selected"<?php } ?>>mars</option>
                <option value="4" <?php if ($date_end_m == '4'){ ?>selected="selected"<?php } ?>>avril</option>
                <option value="5" <?php if ($date_end_m == '5'){ ?>selected="selected"<?php } ?>>mai</option>
                <option value="6" <?php if ($date_end_m == '6'){ ?>selected="selected"<?php } ?>>juin</option>
                <option value="7" <?php if ($date_end_m == '7'){ ?>selected="selected"<?php } ?>>juillet</option>
                <option value="8" <?php if ($date_end_m == '8'){ ?>selected="selected"<?php } ?>>août</option>
                <option value="9" <?php if ($date_end_m == '9'){ ?>selected="selected"<?php } ?>>septembre</option>
                <option value="10" <?php if ($date_end_m == '10'){ ?>selected="selected"<?php } ?>>octobre</option>
                <option value="11" <?php if ($date_end_m == '11'){ ?>selected="selected"<?php } ?>>novembre</option>
                <option value="12" <?php if ($date_end_m == '12'){ ?>selected="selected"<?php } ?>>décembre</option>
            </select>
            <input type="text" name="date_end_y" value="<?= $date_end_y; ?>" placeholder="aaaa" style="display: inline-block; width: 50px; vertical-align: middle;" />
        </p>
        <p>
            <label for="date_description" style="width: 120px; display: inline-block;">Date description</label>&nbsp;
            <input type="text" name="date_description" id="date_description" value="<?= $date_description; ?>" style="width:350px" /><br />
            <em><small>(si non renseigné, la date ci-dessus est prise en compte)</small></em>
        </p>
    </div>

    <?php
}

/* PAGE */

function spre_page_meta_box( $post )
{
    $h1_title = get_post_meta($post->ID, 'h1_title', true);
    $body_class = get_post_meta($post->ID, 'body_class', true);
    // $menu_id = get_post_meta($post->ID, 'menu_id', true);
    // $form_id = get_post_meta($post->ID, 'form_id', true);
    $prefooter_block_id = get_post_meta($post->ID, 'prefooter_block_id', true);
    $mobile_header_bg_color = get_post_meta($post->ID, 'mobile_header_bg_color', true);
    $header_bg_type = get_post_meta($post->ID, 'header_bg_type', true);

    $menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );

    // if (class_exists('RGFormsModel')) {
    //     $forms = RGFormsModel::get_forms();
    // }else{ $forms = array(); }

    $args = array( 'post_type' => 'block', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'suppress_filters' => 0 );
    $blocks = get_posts( $args );

    wp_nonce_field( 'spre_page_meta_box_nonce', 'meta_box_nonce' );
?>

    <div class="customFields">
        <!-- <p>
            <label for="h1_title" style="width: 350px; display: inline-block;">Titre h1</label>&nbsp;<br />
            <textarea name="h1_title" id="h1_title" style="width:600px; height: 100px;"><?= $h1_title; ?></textarea>
        </p>
        <p>
            <label for="body_class" style="width: 350px; display: inline-block;">Classe additionnelle du body</label>&nbsp;<br />
            <input type="text" name="body_class" id="body_class" value="<?= $body_class; ?>" style="width:600px" />
        </p> -->
        <!-- <p>
            <label for="header_bg_type" style="width: 250px; display: inline-block;">Image du header</label>&nbsp;<br />
            <select id="header_bg_type" name="header_bg_type">
                <option value="">Etirée</option>
                <option value="contain" <?php if ($header_bg_type == 'contain'){ echo "selected='selected'"; } ?>>Contenue</option>
            </select>
        </p> -->
        <!-- <p>
            <label for="form_id" style="width: 250px; display: inline-block;">Formulaire intégré</label>&nbsp;<br />
            <select id="form_id" name="form_id">
                <option value="">-- Choisir --</option>
                <?php if ($forms){ foreach ($forms as $form) { ?>
                <option value="<?= $form->id; ?>" <?php if ($form->id == $form_id){ echo "selected='selected'"; } ?>><?= $form->title; ?></option>
                <?php } } ?>
            </select>
        </p> -->
        <!-- <p>
            <label for="menu_id" style="width: 250px; display: inline-block;">Menu d'en-tête</label>&nbsp;<br />
            <select id="menu_id" name="menu_id">
                <option value="">-- Choisir --</option>
                <?php foreach ($menus as $menu) { ?>
                <option value="<?= $menu->term_id; ?>" <?php if ($menu->term_id == $menu_id){ echo "selected='selected'"; } ?>><?= $menu->name; ?></option>
                <?php } ?>
            </select>
        </p> -->
        <p>
            <label for="prefooter_block_id" style="width: 250px; display: inline-block;">Bloc bas de page</label>&nbsp;<br />
            <select id="prefooter_block_id" name="prefooter_block_id">
                <option value="">-- Choisir --</option>
                <?php if ($blocks){ foreach ($blocks as $block) { ?>
                <option value="<?= $block->ID; ?>" <?php if ($block->ID == $prefooter_block_id){ echo "selected='selected'"; } ?>><?= $block->post_title; ?></option>
                <?php } } ?>
            </select>
        </p>
    </div>

    <?php
}

/* BOTH */

add_action( 'save_post', 'spre_post_meta_box_save' );
function spre_post_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    if ( get_post_type($post_id) == 'page' ){
        if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'spre_page_meta_box_nonce' ) ) return;
        if( !current_user_can( 'edit_page' ) ) return;
    
        update_post_meta( $post_id, 'h1_title', $_POST['h1_title'] );
        update_post_meta( $post_id, 'body_class', $_POST['body_class'] );
        // update_post_meta( $post_id, 'form_id', $_POST['form_id'] );
        // update_post_meta( $post_id, 'menu_id', $_POST['menu_id'] );
        update_post_meta( $post_id, 'prefooter_block_id', $_POST['prefooter_block_id'] );
        update_post_meta( $post_id, 'mobile_header_bg_color', $_POST['mobile_header_bg_color'] );
        update_post_meta( $post_id, 'header_bg_type', $_POST['header_bg_type'] );
    }
    if ( get_post_type($post_id) == 'post' ){
        if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'spre_post_meta_box_nonce' ) ) return;
        if( !current_user_can( 'edit_post' ) ) return;
    
        if( isset( $_POST['date_begin_y'] ) && isset( $_POST['date_begin_m'] ) && isset( $_POST['date_begin_d'] ) ){
            $date_begin_m = strlen($_POST['date_begin_m']) == 1 ? '0' . $_POST['date_begin_m'] : $_POST['date_begin_m'];
            $date_begin_d = strlen($_POST['date_begin_d']) == 1 ? '0' . $_POST['date_begin_d'] : $_POST['date_begin_d'];
            $date_str = $_POST['date_begin_y'].'/'.$date_begin_m.'/'.$date_begin_d;
            update_post_meta( $post_id, 'date_begin', $date_str );
        }    
        if( isset( $_POST['date_end_y'] ) && isset( $_POST['date_end_m'] ) && isset( $_POST['date_end_d'] ) ){
            $date_end_m = strlen($_POST['date_end_m']) == 1 ? '0' . $_POST['date_end_m'] : $_POST['date_end_m'];
            $date_end_d = strlen($_POST['date_end_d']) == 1 ? '0' . $_POST['date_end_d'] : $_POST['date_end_d'];
            $date_str = $_POST['date_end_y'].'/'.$date_end_m.'/'.$date_end_d;
            update_post_meta( $post_id, 'date_end', $date_str );
        }
        update_post_meta( $post_id, 'date_description', $_POST['date_description'] );
        update_post_meta( $post_id, 'can_subscribe', $_POST['can_subscribe'] );
    }
}