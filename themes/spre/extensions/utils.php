<?php

function spre_body_classes( $classes ) {
    global $post;
	$classes[] = 'loading';

	// if ($post->ID == spre_get_option('news_page_id')){ $classes[] = 'newsIndex'; }

    if ( is_page() ){
        $body_class = get_post_meta($post->ID, 'body_class', true);
		$classes[] = $body_class;
    }

	return $classes;
}
add_filter( 'body_class', 'spre_body_classes' );

function spre_get_option($option_slug, $default=""){
  if (function_exists('of_get_option')){
      $option = of_get_option($option_slug);
  }else{
      $option = get_option($option_slug);
  }
return $option ? $option : $default;
}

function spre_get_featured_image_url($post_ID=null, $version='full'){
    if (!$post_ID){
        global $post;
        $post_ID = $post->ID;
    }
    $image_id = get_post_thumbnail_id($post_ID);
    return $image_id ? wp_get_attachment_image_src($image_id, $version)[0] : '';
}

function spre_get_term_featured_image_url($term_ID, $version='full'){
    $image_id = get_term_thumbnail_id($term_ID);
    return $image_id ? wp_get_attachment_image_src($image_id, $version)[0] : '';
}

function spre_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'spre_mime_types');

function spre_svg($icon_name){
    return file_get_contents(__DIR__ . '/../assets/images/svg/'.$icon_name.'.svg');
}

function spre_download_url($url){
    $content_url = content_url('/');
    $download_url = $content_url . 'get-file.php?file=' . str_replace($content_url, '', $url);
    $download_short_url = $download_url;
    return $download_short_url;
}

function spre_make_bitly_url($url, $format = 'json', $version = '2.0.1'){
    $login = 'etilax';
    $appkey = spre_get_option('bitly_api_key');
	//create the URL
	$bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;
	
	//get the url
	//could also use cURL here
	$response = file_get_contents($bitly);
	
	//parse depending on desired format
	if(strtolower($format) == 'json')
	{
		$json = @json_decode($response,true);
		return $json['results'][$url]['shortUrl'];
	}
	else //xml
	{
		$xml = simplexml_load_string($response);
		return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
	}
}


function spre_insert_attachment_from_url($url, $parent_post_id = null) {

	if( !class_exists( 'WP_Http' ) )
		include_once( ABSPATH . WPINC . '/class-http.php' );

	$http = new WP_Http();
	$response = $http->request( $url );
	if( is_wp_error($response) || $response['response']['code'] != 200 ) {
		return false;
	}

	$upload = wp_upload_bits( basename($url), null, $response['body'] );
	if( !empty( $upload['error'] ) ) {
		return false;
	}

	$file_path = $upload['file'];
	$file_name = basename( $file_path );
	$file_type = wp_check_filetype( $file_name, null );
	$attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
	$wp_upload_dir = wp_upload_dir();

	$post_info = array(
		'guid'           => $wp_upload_dir['url'] . '/' . $file_name,
		'post_mime_type' => $file_type['type'],
		'post_title'     => $attachment_title,
		'post_content'   => '',
		'post_status'    => 'inherit',
	);

	// Create the attachment
	$attach_id = wp_insert_attachment( $post_info, $file_path, $parent_post_id );

	// Include image.php
	require_once( ABSPATH . 'wp-admin/includes/image.php' );

	// Define attachment metadata
	$attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );

	// Assign metadata to attachment
	wp_update_attachment_metadata( $attach_id,  $attach_data );

	return $attach_id;

}

function spre_get_all_posts($post_type){
    $args = array( 
      'post_type' => $post_type, 
      'posts_per_page' => -1, 
      'orderby' => 'title',
      'order' => 'ASC',
    );
    return get_posts( $args );
}



//***Fil d'ariane
//Récupérer les catégories parentes
function spre_get_category_parents($id, $link = false,$separator = '/',$nicename = false,$visited = array()) {
    $chain = '';
    $parent = &get_category($id);
    if (is_wp_error($parent))return $parent;
    
    // cats principales : 50, 51, 57
    // if (in_array($parent->term_id, array(46, 14, 16, 13, 58, 59))) $link = true;
    
    if ($nicename)$name = $parent->name;
    else $name = $parent->cat_name;

    //21, 22, 23

    if ($parent->parent && ($parent->parent != $parent->term_id ) && !in_array($parent->parent, $visited)) {
        $visited[] = $parent->parent;
        $chain .= spre_get_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
    }
        
    if ($link){
        $chain .= '<span typeof="v:Breadcrumb"><a href="' . get_category_link( $parent->term_id ) . '" title="Voir tous les articles de '.$parent->cat_name.'" rel="v:url" property="v:title">'.$name.'</a></span>' . $separator;
    }else{
        $chain .= $name.$separator;
    }
    return $chain;
}

function is_technology_category($category){
    return cat_is_ancestor_of(13, $category) or cat_is_ancestor_of(14, $category) or cat_is_ancestor_of(59, $category);
}

//Le rendu
function spre_bread() {
  if ( function_exists('yoast_breadcrumb') ) {
    yoast_breadcrumb( '<p class="breadcrumb">','</p>' );
    return false;
  }
  // variables gloables
  global $wp_query;$ped=get_query_var('paged');$rendu = '<div class="pageInner"><div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">';  
  $debutlien = '<span typeof="v:Breadcrumb"><a title="Accueil" id="breadh" href="'.home_url().'" rel="v:url" property="v:title">Accueil </a></span>';
  $debut = '<span typeof="v:Breadcrumb">Accueil de '. get_bloginfo('name') .'</span>';

  // si l'utilisateur a défini une page comme page d'accueil
  $origin = isset($_GET['origin']) ? $_GET['origin'] : '';

  if ( is_front_page() ) {
      $rendu .= $debut;
  }
  else {

    // on teste si une page a été définie comme devant afficher une liste d'article 
    if( get_option('show_on_front') == 'page') {
      $url = urldecode(substr($_SERVER['REQUEST_URI'], 1));
      $uri = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
      $posts_page_id = get_option( 'page_for_posts');
      $posts_page_url = get_page_uri($posts_page_id);  
      $pos = strpos($uri,$posts_page_url);
      // if($pos !== false) {
        // $rendu .= $debutlien.' > <span typeof="v:Breadcrumb">Les articles</span>';
      // }
      // else {$rendu .= $debutlien;} 
      $rendu .= $debutlien;
    }

    //Si c'est l'accueil
    elseif ( is_home()) {$rendu .= $debut;}

    //pour tout le reste
    else {$rendu .= $debutlien;}

    // les catégories
    if ( is_category() ) {
      $cat_obj = $wp_query->get_queried_object();$thisCat = $cat_obj->term_id;$thisCat = get_category($thisCat);$parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) $rendu .= " » ".spre_get_category_parents($parentCat, true, " » ", true);
      if ($thisCat->parent == 0) {$rendu .= " » ";}
      if ( $ped <= 1 ) {$rendu .= '<span>'.single_cat_title("", false).'</span>';}
      elseif ( $ped > 1 ) {
        $rendu .= '<span typeof="v:Breadcrumb"><a href="' . get_category_link( $thisCat ) . '" title="Voir tous les articles de '.single_cat_title("", false).'" rel="v:url" property="v:title">'.single_cat_title("", false).'</a></span>';}}

    // les auteurs
    elseif ( is_author()){
      global $author;$user_info = get_userdata($author);$rendu .= " > Articles de l'auteur ".$user_info->display_name."</span>";}  

    // les mots clés
    elseif ( is_tag()){
      $tag=single_tag_title("",FALSE);$rendu .= " > Articles sur le th&egrave;me <span>".$tag."</span>";}
      elseif ( is_date() ) {  
          if ( is_day() ) {
              global $wp_locale;
              $rendu .= '<span typeof="v:Breadcrumb"><a href="'.get_month_link( get_query_var('year'), get_query_var('monthnum') ).'" rel="v:url" property="v:title">'.$wp_locale->get_month( get_query_var('monthnum') ).' '.get_query_var('year').'</a></span> ';
              $rendu .= " > Archives pour ".get_the_date();}
      else if ( is_month() ) {
              $rendu .= " > Archives pour ".single_month_title(' ',false);}
      else if ( is_year() ) {
              $rendu .= " > Archives pour ".get_query_var('year');}
    }

    //les archives hors catégories
    elseif ( is_archive() && !is_category()){
      // $posttype = get_post_type();
      // $tata = get_post_type_object( $posttype );
      // $var = '';
      // $the_tax = get_taxonomy( get_query_var( 'taxonomy' ) );
      // $titrearchive = $tata->labels->menu_name;
      // if (!empty($the_tax)){$var = $the_tax->labels->name.' ';}
          // if (empty($the_tax)){$var = $titrearchive;}
      // $rendu .= ' > '.$var;
      
      $tax = get_taxonomy( get_query_var( 'taxonomy' ) );
      $tax_name = $tax->labels->name;
      
      $term = $wp_query->get_queried_object();
      $title = $term->name;
      if ($tax_name == 'Divisions'){
        $title = 'Division '.$title;    
      }
      $url = get_term_link($term);
      $rendu .= " > <span>".$title."</span>";
    }

    // La recherche
    elseif ( is_search()) {
      $rendu .= " > R&eacute;sultats de votre recherche <span>> ".get_search_query()."</span>";}

    // la page 404
    elseif ( is_404()){
      $rendu .= " > 404 Page non trouv&eacute;e";}

    //Une page
    elseif ( is_page()) {
      $post = $wp_query->get_queried_object();
      if ( $post->post_parent == 0 ){$rendu .= " > <span>".the_title('','',FALSE)."</span>";}
      elseif ( $post->post_parent != 0 ) {
        $title = the_title('','',FALSE);
        $ancestors = array_reverse(get_post_ancestors($post->ID));array_push($ancestors, $post->ID);
        foreach ( $ancestors as $ancestor ){
          if( $ancestor != end($ancestors) ){$rendu .= '> <span typeof="v:Breadcrumb"><a href="'. get_permalink($ancestor) .'" rel="v:url" property="v:title">'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</a></span>';}
          else {$rendu .= ' > <span>'.strip_tags(apply_filters('single_post_title',get_the_title($ancestor))).'</span>';}
        }
      }
    }

    //Un article
    elseif ( is_single()){
      $categories = get_the_category();
      $post_type = get_post_type();
      $post = $wp_query->get_queried_object();
      
      if ($post_type == 'produit'){
        
        if ($origin == 'divisions' || $origin == 'division'){
            $division = elax_get_product_division();
            $title = 'Division '.$division->name;
            $url = get_term_link($division);
            $rendu .= " > <a class='breadl' href='".$url."' title='".$title."' rel='v:url' property='v:title'>".$title."</a>";
        }
        else{
            $solution_id = wpcf_pr_post_get_belongs($post->ID, 'solution');
            
            $expertises = get_the_terms($solution_id, 'expertise');
            // print_r($expertises);
            if ($expertises){
                foreach ($expertises as $expertise){ break; }
                // $expertise = $expertises[0];
                $title = $expertise->name;
                $url = get_term_link($expertise);
                $rendu .= " > <a class='breadl' href='".$url."' title='".$title."' rel='v:url' property='v:title'>".$title."</a>";
            }
            
            $solution = get_post($solution_id);
            $title = $solution->post_title;
            $url = get_permalink($solution_id);
            $rendu .= " > <a class='breadl' href='".$url."' title='".$title."' rel='v:url' property='v:title'>".$title."</a>";
        }
        $rendu .= " > <span>".the_title('','',FALSE)."</span>";
      }else if ($post_type == 'solution'){
        
        if ($origin == 'produits' || $origin == 'expertise'){
            $expertises = get_the_terms($post->ID, 'expertise');
            if ($expertises){
                foreach ($expertises as $expertise){ break; }
                $title = $expertise->name;
                $url = get_term_link($expertise);
                $rendu .= " > <a class='breadl' href='".$url."' title='".$title."' rel='v:url' property='v:title'>".$title."</a>";
            }
        }else{
            $divisions = get_the_terms($post->ID, 'division');
            if ($divisions){
                foreach ($divisions as $division){ break; }
                $title = 'Division '.$division->name;
                $url = get_term_link($division);
                $rendu .= " > <a class='breadl' href='".$url."' title='".$title."' rel='v:url' property='v:title'>".$title."</a>";
            }
        }
        $rendu .= " > <span>".the_title('','',FALSE)."</span>";
      }else{
        $category = $categories[0];
        $category_id = get_cat_ID( $category->cat_name );
        if ($category_id != 0) {
          $rendu .= " » ".spre_get_category_parents($category_id,TRUE,' > ')."<span>".the_title('','',FALSE)."</span>";}
        elseif ($category_id == 0) {
          $tata = get_post_type_object( $post_type );
          $titrearchive = $tata->labels->menu_name;
          $urlarchive = get_post_type_archive_link( $post_type );
          if ( $titrearchive != 'Marques' ){
          $rendu .= ' > <span typeof="v:Breadcrumb"><a class="breadl" href="'.$urlarchive.'" title="'.$titrearchive.'" rel="v:url" property="v:title">'.$titrearchive.'</a></span> > <span>'.the_title('','',FALSE).'</span>';
          }else{
          $rendu .= ' > <span typeof="v:Breadcrumb">'.$titrearchive.'</span> > <span>'.the_title('','',FALSE).'</span>';
          }
        }
      }
    }
    
    if ( $ped >= 1 ) {$rendu .= ' (Page '.$ped.')';}
  }
  $rendu .= '</div></div>';
  echo $rendu;
}
/**
 * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 *
 * @return string Trimmed string.
 */
function truncateHtml($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
	if ($considerHtml) {
		// if the plain text is shorter than the maximum length, return the whole text
		if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
			return $text;
		}
		// splits all html-tags to scanable lines
		preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
		$total_length = strlen($ending);
		$open_tags = array();
		$truncate = '';
		foreach ($lines as $line_matchings) {
			// if there is any html-tag in this line, handle it and add it (uncounted) to the output
			if (!empty($line_matchings[1])) {
				// if it's an "empty element" with or without xhtml-conform closing slash
				if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
					// do nothing
				// if tag is a closing tag
				} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
					// delete tag from $open_tags list
					$pos = array_search($tag_matchings[1], $open_tags);
					if ($pos !== false) {
					unset($open_tags[$pos]);
					}
				// if tag is an opening tag
				} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
					// add tag to the beginning of $open_tags list
					array_unshift($open_tags, strtolower($tag_matchings[1]));
				}
				// add html-tag to $truncate'd text
				$truncate .= $line_matchings[1];
			}
			// calculate the length of the plain text part of the line; handle entities as one character
			$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
			if ($total_length+$content_length> $length) {
				// the number of characters which are left
				$left = $length - $total_length;
				$entities_length = 0;
				// search for html entities
				if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
					// calculate the real length of all entities in the legal range
					foreach ($entities[0] as $entity) {
						if ($entity[1]+1-$entities_length <= $left) {
							$left--;
							$entities_length += strlen($entity[0]);
						} else {
							// no more characters left
							break;
						}
					}
				}
				$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
				// maximum lenght is reached, so get off the loop
				break;
			} else {
				$truncate .= $line_matchings[2];
				$total_length += $content_length;
			}
			// if the maximum length is reached, get off the loop
			if($total_length>= $length) {
				break;
			}
		}
	} else {
		if (strlen($text) <= $length) {
			return $text;
		} else {
			$truncate = substr($text, 0, $length - strlen($ending));
		}
	}
	// if the words shouldn't be cut in the middle...
	if (!$exact) {
		// ...search the last occurance of a space...
		$spacepos = strrpos($truncate, ' ');
		if (isset($spacepos)) {
			// ...and cut the text in this position
			$truncate = substr($truncate, 0, $spacepos);
		}
	}
	// add the defined ending to the text
	$truncate .= $ending;
	if($considerHtml) {
		// close all unclosed html-tags
		foreach ($open_tags as $tag) {
			$truncate .= '</' . $tag . '>';
		}
	}
	return $truncate;
}

function spre_the_excerpt_max_charlength($postID, $charlength, $strip=false, $echo=true) {      
  $post = get_post($postID);
  $excerpt = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
  $excerpt = do_shortcode($excerpt);
  $charlength++;
    
    $striped_excerpt = wp_strip_all_tags($excerpt);
    if ($strip){ $excerpt = $striped_excerpt; }

  if ( mb_strlen( $striped_excerpt ) > $charlength ) {
        $result = truncateHtml($excerpt, $charlength, '...', false, true);
  } else {
    $result = $excerpt;
  }
    
    if ($echo){ echo $result; }
    else{ return $result; }
}

function spre_get_all_terms($taxonomy, $post_type){
    // Query pages.
    // $args = array(
                // 'orderby'       =>  'term_order',
                // 'depth'         =>  0,
                // 'child_of'      => 0,
                // 'hide_empty'    =>  0
    // );
    // $taxonomy_terms = get_terms($taxonomy, $args);
    // return $taxonomy_terms;
                
    $args = array( 'post_type' => $post_type, 'posts_per_page' => -1 );
    $posts = get_posts( $args );
    $terms = array();
    $terms_ids = array();
    foreach ( $posts as $post ){
        $the_terms = get_the_terms($post->ID, $taxonomy);
        if ($the_terms){
            foreach ( $the_terms as $term ){
                // echo $term->name;
                if (!in_array($term->term_id, $terms_ids)){
                    $terms[] = $term;
                    $terms_ids[] = $term->term_id;
                }
            }
        }
    }
    return $terms;
}

function spre_get_products($cat_ids=array(), $search='', $interlocutor_ids=array(), $brand_ids=array()){
  $tax_query = array();
  if (!empty($cat_ids)){
    $tax_query[] = array(
      'taxonomy' => 'product_cat',
      'field' => 'term_id',
      'terms' => $cat_ids,
    );
  }
  // print_r($tax_query);

  $meta_query = array();
  if ($interlocutor_ids){
    $interlocutors_meta_query = array('relation' => 'OR',);
    foreach ($interlocutor_ids as $interlocutor_id){
      $interlocutors_meta_query[] = array(
        'key' => 'interlocutors_ids',
        'value' => $interlocutor_id,
        'compare' => 'LIKE',
      );
    }
    $meta_query[] = $interlocutors_meta_query;
  }
  if ($brand_ids){
    $brand_meta_query = array(
      'relation' => 'OR'
    );
    foreach ($brand_ids as $brand_id){
      $brand_meta_query[] = array(
        'key' => 'brand_id',
        'value' => $brand_id,
        'compare' => 'LIKE',
      );
    }
    $meta_query[] = $brand_meta_query;
  }
  // print_r($meta_query);

  $args = array(
      'post_type' => 'product',
      'posts_per_page' => -1,
      'tax_query' => $tax_query,
      'meta_query' => $meta_query,
      's' => $search
  );
  $products = get_posts( $args );
  return $products;
}

function spre_get_brand_products($brand_id){
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'brand_id',
                'value' => $brand_id,
                'compare' => 'LIKE',
            )
        )
    );
    $products = get_posts( $args );
    return $products;
}

function spre_slugify($text){
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
 
    // trim
    $text = trim($text, '-');
 
    // transliterate
    if (function_exists('iconv'))
    {
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    }
 
    // lowercase
    $text = strtolower($text);
 
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
 
    if (empty($text))
    {
        return 'n-a';
    }
 
    return $text;
}

function spre_get_products_cats(){
  $current_division = isset($_GET['division']) ? $_GET['division'] : '';
  $current_search = isset($_GET['recherche']) ? $_GET['recherche'] : '';
  $current_interlocutor = isset($_GET['interlocuteur']) ? $_GET['interlocuteur'] : '';
  $current_brand = isset($_GET['marque']) ? $_GET['marque'] : '';

  $current_division_ids = $current_division ? explode(',', $current_division) : '';
  /* $current_division_ids = array();
  if ($current_division){
    foreach ($current_division_array as $division_slug){
      $current_division_ids[] = get_term_by( 'slug', $division_slug, 'product_cat' )->term_id;
    }
  } */

  $current_interlocutor_ids = $current_interlocutor ? explode(',', $current_interlocutor) : '';
  // $current_interlocutor_ids = '';
  $current_brand_ids = $current_brand ? explode(',', $current_brand) : '';

  if ( $current_search || $current_brand || $current_interlocutor ){
    $products = spre_get_products($current_division_ids, $current_search, $current_interlocutor_ids, $current_brand_ids);
    if ( count($products) > 1 ){
      echo '<h2>'.count($products).' produits trouvés</h2>';
    }else if ( count($products) > 0 ){
      echo '<h2>'.count($products).' produit trouvé</h2>';
    }
    include(locate_template('includes/products_list.php'));

  }else{
    if ($current_division_ids){
      $cats = get_terms('product_cat', array(
        'parent' => 0,
        'hide_empty' => false,
        'include' => $current_division_ids
      ));
    }else{
      $cats = get_terms('product_cat', array(
        'parent' => 0,
        'hide_empty' => false,
        'exclude' => array(203, 274) // Non classé, A trier
      ));
    }
    include(locate_template('includes/products_cats_content.php'));
  }
}

add_action('wp_ajax_nopriv_get-products-cats', 'spre_ajax_get_products_cats');
add_action('wp_ajax_get-products-cats', 'spre_ajax_get_products_cats');
function spre_ajax_get_products_cats(){
  spre_get_products_cats();
  die;
}

function spre_get_products_filters(){
  $current_division = isset($_GET['division']) ? $_GET['division'] : '';
  $current_search = isset($_GET['recherche']) ? $_GET['recherche'] : '';
  $current_interlocutor = isset($_GET['interlocuteur']) ? $_GET['interlocuteur'] : '';
  $current_brand = isset($_GET['marque']) ? $_GET['marque'] : '';

  $current_division_ids = $current_division ? explode(',', $current_division) : '';

  $current_interlocutor_ids = $current_interlocutor ? explode(',', $current_interlocutor) : '';
  $current_brand_ids = $current_brand ? explode(',', $current_brand) : '';

  if ( $current_search || $current_brand || $current_interlocutor || $current_division_ids ){
    $products = spre_get_products($current_division_ids, $current_search, $current_interlocutor_ids, $current_brand_ids);  
  }else{
    $products = '';
    $cats = get_terms('product_cat', array(
      'parent' => 0,
      'hide_empty' => false,
      'exclude' => array(203, 274) // Non classé, A trier
    ));
  }
  include(locate_template('includes/products_filters_inner.php'));
}

add_action('wp_ajax_nopriv_get-products-filters', 'spre_ajax_get_products_filters');
add_action('wp_ajax_get-products-filters', 'spre_ajax_get_products_filters');
function spre_ajax_get_products_filters(){
  spre_get_products_filters();
  die;
}

function spre_get_term_top_most_parent( $term, $taxonomy ) {
  // Start from the current term
  $parent  = get_term( $term, $taxonomy );
  // Climb up the hierarchy until we reach a term with parent = '0'
  while ( $parent->parent != '0' ) {
      $term_id = $parent->parent;
      $parent  = get_term( $term_id, $taxonomy);
  }
  return $parent;
}

function spre_get_posts($count=0, $offset=0, $tag='', $cat='', $use='', $year=''){
    if ($count == 0){
        $count = get_option('posts_per_page');
    }

    $tax_query = array();
    $meta_query = array();
    $date_query = array();

    $cat_tax = 'category';
    $tag_tax = 'post_tag';
    $user_tax = 'spre_user_category';

    if ($tag){
      $tax_query[] = array(
        'taxonomy' => $tag_tax,
        'field' => 'slug',
        'terms' => $tag,
        'operator' => 'IN',
      );
    }
    if ($cat){
      $tax_query[] = array(
        'taxonomy' => $cat_tax,
        'field' => 'slug',
        'terms' => $cat,
        'operator' => 'IN',
      );
    }
    if ($use){
      $tax_query[] = array(
        'taxonomy' => $user_tax,
        'field' => 'slug',
        'terms' => $use,
        'operator' => 'IN',
      );
    }

    $month = '';
    if ( !empty($year) ){
        if ( !empty($month) ){
            $date_query[] = array(
                'year'  => $year,
                'month'  => $month,
            );
        }else{
            $date_query[] = array(
                'year'  => $year
            );
        }
    }

    $args = array( 
        'post_type' => 'post',
        'posts_per_page' => $count,
        'offset' => $offset,
        'tax_query' => $tax_query,
        'date_query' => $date_query,
        'meta_query' => $meta_query,
        'suppress_filters' => 1
    );
    return get_posts( $args );
}

function spre_get_jobs($count=0, $offset=0){
  if ($count == 0){
      $count = get_option('posts_per_page');
  }
  
  $args = array(
    'post_type' => 'job', 
    'posts_per_page' => $count,
    'offset' => $offset,
    'orderby' => 'menu_order', 
    'order' => 'ASC', 
    'suppress_filters' => 0
  );
  return get_posts( $args );
}

function get_post_date_str($post_id, $main_category){    
    if ($main_category->term_id=spre_get_option('events_category_id')){
      $date_description = get_post_meta($post_id, 'date_description', true);
      if ($date_description){
        return $date_description;
      }else{
        $date_begin = get_post_meta($post_id, 'date_begin', true);
        return $date_begin ? date_i18n('j F Y', strtotime($date_begin)) : get_the_time('j F Y', $post_id);
      }
  }else{
    return get_the_time('j F Y', $post_id);
  }
}

function spre_get_time_reading($post_id){
	return max(round(str_word_count( strip_tags( get_post_field( 'post_content', $post_id ))) / 300), 1) . ' min';
}

function spre_is_private_area($post_=false){
  global $post;
  if (!$post_){ $post_ = $post; }
  $private_home_page_id = spre_get_option( 'private_home_page_id' );
  if ( $post_ && $post_->post_parent ){
      $ancestors = get_post_ancestors($post_->ID);
      $root = count($ancestors)-1;
      $top_parent_id = $ancestors[$root];
      return ($top_parent_id == $private_home_page_id);
  }
  return ( ($post_ && $post_->ID == $private_home_page_id) || is_singular('private_resource') );
}

function spre_check_user_login(){
    if ( !is_user_logged_in() ) {
        $url = // URL de la page login
        wp_redirect( $url.'?next='.$_SERVER["REQUEST_URI"], 302 );
        exit;
    }
}