<?php

//*************************************************************
//on enregistres les boutons dans tinymce
// toute les fonctions et icones pour tinymce sont dans le répertoire "buttons"
////***********************************************************

// ENREGISTREMENT Des BOUTONS
 
add_action('init', 'spre_setup_buttons');
 
function spre_setup_buttons() {
    // on ajoute notre plugin    
    add_filter("mce_external_plugins", "spre_add_external_plugin");
    // on ajoute les boutons    
    // add_filter( 'mce_buttons', 'spre_register_buttons' );
    add_filter( 'mce_buttons_2', 'spre_register_buttons_2' );
    // add_filter( 'mce_buttons_3', 'spre_register_buttons_3' );
} 
 
function spre_add_external_plugin($plugin_array) {
    // on indique où se trouve le fichier de configuration des boutons
    $plugin_array['spre_buttons'] = get_template_directory_uri() . '/assets/js/tinymce_buttons/buttons.js';
    return $plugin_array;
}
 
function spre_register_buttons($buttons) {
	if ( !in_array( 'alignjustify', $buttons ) && in_array( 'alignright', $buttons ) ){
		$key = array_search( 'alignright', $buttons );
		$inserted = array( 'alignjustify' );
		array_splice( $buttons, $key + 1, 0, $inserted );
	}
    return $buttons;
}
 
function spre_register_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
 
function spre_register_buttons_3($buttons) {
    // on ajoute les boutons renseignés dans le fichier buttons.js
    $buttons[] = 'space';
    $buttons[] = 'button';
    $buttons[] = 'news';
    $buttons[] = 'featured_news';
    $buttons[] = 'events';
    $buttons[] = 'featured_events';
    $buttons[] = 'tabs';
    $buttons[] = 'accordion';
    $buttons[] = 'exergue';
    $buttons[] = 'quote';
    $buttons[] = 'team';
    $buttons[] = 'desktop_br';
    return $buttons;
}

function spre_tinymce_colors_options( $init ) {
    $custom_colours = '
        "CC1F30", "Rouge",
        "3C287E", "Bleu",
        "BCC0FA", "Bleu clair",
        "4A4A49", "Gris",
        "000000", "Noir",
        "FFFFFF", "Blanc",
	';

    $init['textcolor_map'] = '['.$custom_colours.']';
    $init['textcolor_rows'] = 6; // expand colour grid to 6 rows
    return $init;
}
add_filter('tiny_mce_before_init', 'spre_tinymce_colors_options');

// Callback function to filter the MCE settings
function spre_tinymce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
        array(
			'title' => 'H2 Rouge',
			'block' => 'h2',
			'classes' => 'spre_section_title_red',
			'wrapper' => true,
        ),
        array(
			'title' => 'H3 Rouge',
			'block' => 'h3',
			'classes' => 'spre_h3_title_red',
			'wrapper' => true,
		),
        array(
			'title' => 'Sous titre Gris',
			'block' => 'div',
			'classes' => 'spre_chapeau',
			'wrapper' => true,
        ),
        array(
			'title' => 'Paragraphe',
			'block' => 'p',
			'classes' => 'spre_paragraph',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}

// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'spre_tinymce_before_init_insert_formats' );

function mce_formats( $init ) {

	$formats = array(
        'spre_section_title_red'    => __( 'H2 Rouge', 'text-domain' ),
        'spre_h3_title_red'         => __( 'H3 Rouge', 'text-domain' ),
		'spre_chapeau'              => __( 'Sous titre Gris', 'text-domain' ),
        'spre_chapeau_purple'       => __( 'Sous titre Violet', 'text-domain' ),
        'spre_paragraph'            => __( 'Paragraphe', 'text-domain' ),
	);
    
    // concat array elements to string
	array_walk( $formats, function ( $key, $val ) use ( &$block_formats ) {
		$block_formats .= esc_attr( $key ) . '=' . esc_attr( $val ) . ';';
	}, $block_formats = '' );

	$init['block_formats'] = $block_formats;

	return $init;
}
add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\\mce_formats' );

// Enqueue the script to customize the formats
add_action( 'after_wp_tiny_mce', 'custom_after_wp_tiny_mce' );
function custom_after_wp_tiny_mce() {
printf( "<script type='text/javascript' src='%s/assets/js/tinymce_buttons/tinymce-format-init.js'></script>", get_stylesheet_directory_uri() );
}
