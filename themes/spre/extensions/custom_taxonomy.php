<?php

function spre_taxonomies_init() {
	register_taxonomy(
		'spre_user_category',
		array('spre_user', 'downloads', 'post'),
		array(
			'label' => "Catégories utilisateurs",
			'rewrite' => false,
            'hierarchical' => true,
            'show_in_rest' => false,
		)
	);
	register_taxonomy(
		'help_center_category',
		array('spre_user', 'help_center', 'downloads'),
		array(
			'label' => "Catégories centre d'aide",
			'rewrite' => false,
            'hierarchical' => true,
            'show_in_rest' => false,
		)
	);
	register_taxonomy(
		'download_category',
		array('downloads'),
		array(
			'label' => "Catégories téléchargements",
			'rewrite' => false,
            'hierarchical' => true,
            'show_in_rest' => false,
		)
	);
}
add_action( 'init', 'spre_taxonomies_init', 1 );