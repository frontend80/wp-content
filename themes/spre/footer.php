      </div>
    </div>
    <div class="clear"></div>

    
    <?php
    global $post;
    $prefooter_block_id = '';
    if (is_singular('post')) {
    	$prefooter_block_id = spre_get_option('blog_bottom_block_id');
    } else {
      if($post) {
        $prefooter_block_id = get_post_meta($post->ID, 'prefooter_block_id', true);
      }
    }
    $padding_top = 'pt-20';
    $margin_top_footer = 'mt-0'; // mt-24
    $margin_top_div = 'mt-0'; // mt-10
    if ($prefooter_block_id) {
      $margin_top_footer = 'mt-0';
      $margin_top_div = 'mt-0';
      if ($prefooter_block_id != 285) {
        $padding_top = 'pt-40';
      }
    ?>
    <!-- <section id="preFooter">
        <div class="pageInner"> -->
            <?= do_shortcode('[block id="' . $prefooter_block_id . '"]') ?>
        <!-- </div>
    </section> -->
    <?php }
    ?>

  <footer class="relative z-0 <?= $margin_top_footer ?> lg:mt-0 bg-spre-footer rounded-t-4xl">
    <div class="inset-0 hidden footer-pseudo lg:absolute lg:grid lg:grid-cols-4">
      <div class="absolute inset-0" style='background-image: url("<?= get_template_directory_uri() ?>/assets/images/svg/patterns/Roue_Coin-bas-droit.png"); background-position: bottom right; background-repeat: no-repeat;'></div>
      <!-- <div class="absolute inset-0" style='background-image: url("<?= get_template_directory_uri() ?>/assets/images/svg/patterns/footer_bottom_left_bg.svg"); background-position: bottom right; background-repeat: no-repeat;'></div> -->
    </div>
    <div class="relative px-0 pt-20 mx-auto <?= $margin_top_divs ?> lg:mt-0 max-w-1100 lg:py-0 lg:px-0">
      <div class="relative lg:grid lg:grid-cols-4 lg:gap-8">
        <div class="lg:col-span-3">
          <div class="lg:grid lg:grid-cols-3 lg:<?= $padding_top ?>">
            <div class="px-4 lg:col-span-2 lg:px-0">
              <div class="pb-6 text-lg font-semibold leading-loose text-spre-brown_dark">Vous êtes utilisateur ?</div>
              <div id="menu-footer" class="grid grid-cols-2 gap-20 menu-footer">
                <div>
                  <ul class="text-base font-light leading-1_4 text-spre-brown_dark"><?php wp_nav_menu([
                    'theme_location' => 'footer-1',
                    'items_wrap' => '%3$s',
                    'container' => false,
                  ]); ?></ul>
                </div>
                <div>
                  <ul class="text-base font-light leading-1_4 text-spre-brown_dark"><?php wp_nav_menu([
                    'theme_location' => 'footer-2',
                    'items_wrap' => '%3$s',
                    'container' => false,
                  ]); ?></ul>
                </div>
              </div>
            </div>
            <div class="lg:col-span-1">
              <div class="px-4 py-6 border-l divide-y lg:py-0 lg:pl-5 border-spre-gray_light divide-spre-gray_light">
                <ul id="menu-footer-right1" class="pb-6 text-lg font-semibold leading-loose menu-footer-right1 text-spre-brown_dark"><?php wp_nav_menu([
                  'theme_location' => 'footer-3',
                  'items_wrap' => '%3$s',
                  'container' => false,
                ]); ?></ul>
                <ul id="menu-footer-right2" class="pt-6 text-lg font-semibold leading-loose menu-footer-right2 text-spre-brown_dark"><?php wp_nav_menu([
                  'theme_location' => 'footer-4',
                  'items_wrap' => '%3$s',
                  'container' => false,
                ]); ?></ul>
              </div>
            </div>
          </div>
          <ul id="menu-footer-bottom" class="hidden pt-10 pb-6 space-x-10 font-light menu-footer-bottom lg:flex text-xxs text-spre-brown_dark">
            <li class="copyright">©SPRE <?= date('Y') ?></li>
            <?php wp_nav_menu([
              'theme_location' => 'footer-bottom',
              'items_wrap' => '%3$s',
              'container' => false,
            ]); ?>
          </ul>
        </div>
        <div class="px-4 pb-8 mt-8 lg:col-span-1 lg:mt-0 lg:bg-transparent bg-spre-red lg:pb-16 lg:<?= $padding_top ?> lg:px-8">
          <div class="relative z-20">
            <div class="text-lg font-normal leading-relaxed text-white whitespace-pre-line lg:-mt-6">
              <?php if ( spre_get_option( 'footer-5' ) ) { echo spre_get_option( 'footer-5' ); } ?>
            </div>
            <div class="inline-flex pt-6 rounded-md shadow-sm">
              <a href="./contact/" class="inline-flex items-center justify-center px-6 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md py-1_5 text-spre-brown_dark bg-spre-white hover:shadow-spre">
                Nous contacter
              </a>
            </div>
        </div>
      </div>
      <div class="block px-4 pt-6 pb-6 lg:hidden">
        <ul id="menu-footer-bottom" class="flex space-x-10 font-light menu-footer-bottom text-xxs text-spre-brown_dark">
          <li class="copyright">©SPRE <?= date('Y') ?></li>
          <?php wp_nav_menu([
            'theme_location' => 'footer-bottom',
            'items_wrap' => '%3$s',
            'container' => false,
          ]); ?>
        </ul>
      </div>
    </div>
  </footer>
  <div class="clear"></div>

  <!-- <?php include locate_template('includes/resp_menu.php'); ?>     -->
  
  <?php if (spre_get_option('custom_css_code')) {
    echo '<style>' . spre_get_option('custom_css_code') . '</style>';
  } ?>
  <?php if (spre_get_option('custom_code_body')) {
    echo spre_get_option('custom_code_body');
  } ?>
  <?php if (spre_get_option('analytics_code')) {
    echo spre_get_option('analytics_code');
  } ?>
  
  <?php
//include(locate_template('includes/quantcast_header.php'));
?>
    <?php wp_footer(); ?>
</body>
</html>