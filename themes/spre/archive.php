<?php
	$cat = get_queried_object();
    
    $sub_cats = get_terms('product_cat', array(
        'parent' => $cat->term_id,
        'hide_empty' => false
    ));
    if (!$sub_cats){
        $products = spre_get_products(array($cat->term_id));
        if ( count($products) == 1){
            wp_redirect( get_permalink($products[0]->ID), 302 );
        }
    }
    
    get_header();

    $description = $cat->description;
    
    $division = spre_get_term_top_most_parent( $cat->term_id, $cat->taxonomy );
    $header_url = spre_get_option($division->slug.'_bg');
?>
    <header class="pageHeader" <?php if ($header_url){ ?>style="background-image: url(<?= $header_url; ?>);"<?php } ?>>
        <div class="pageInner">
            <?php if (function_exists('spre_bread')) spre_bread(); ?>
            <div class="headerContent">
                <h1><?= single_cat_title( '', false ); ?></h1>
                <?php if ($description){ ?>
                    <div class="excerpt contentIntro"><?= apply_filters('the_excerpt', $description); ?></div>
                <?php } ?>
            </div>
            <?php include(locate_template('includes/social_share.php')); ?>
        </div>
    </header>
    <div class="subHeader"></div>
    <div class="pageContent editableContent">
        <div class="pageInner">
            <div class="pagePart">
                <?php if ($sub_cats){ 
                    $cats = $sub_cats; include(locate_template('includes/product_cats_list.php'));
                }else{ 
                    include(locate_template('includes/products_list.php'));
                ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php while ( have_posts() ) : the_post(); ?>
    <?php endwhile; ?>
<?php get_footer(); ?>