<?php
get_header();
while (have_posts()):

  the_post();
  $post_cats = get_the_terms($post->ID, 'category');
  $date = get_the_time('d M Y', $post->ID);
  
?>
  <div class="relative z-0">
    <header class="relative z-10 pageHeader">
      <div class="relative z-20 pb-4 pageInner">
        <div class="border-b border-gray-200">
          <div class="relative flex justify-between w-full py-10">
            <?php include locate_template('includes/post_nav.php'); ?>
            <?php include locate_template('includes/social_share.php'); ?>
          </div>
          <div class="mb-1 font-medium text-spre-purple">
            <?php if (
              $post_cats &&
              $post_cats[0]->term_id != 1
            ) { ?><span class=""><?= $post_cats[0]->name ?></span> - <?php } ?>
            <span class=""><?= $date ?></span>
          </div>
          <div class="w-full headerContent">
            <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($post->post_title) ?></h1>
            <!-- <?php if ($post->post_excerpt) { ?>
              <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
                'the_excerpt',
                $post->post_excerpt
              ) ?></div>
            <?php } ?> -->
          </div>
        </div>
      </div>
      <!-- <?php if (get_the_post_thumbnail_url($post->ID)) { ?>
        <div class="absolute top-0 bottom-0 right-0 z-10 w-1/3">
          <div class="absolute z-10 overflow-hidden" style="left: 0px; top: 20%;">
            <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_the_post_thumbnail_url($post->ID); ?>" />
          </div>
        </div>
      <?php } ?> -->
      <!-- <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
        <div class="absolute bottom-0 right-0 z-0 overflow-hidden" style='left: 20px; top: 0px; background-image: url("<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg"); background-size: cover;'>
        </div>
      </div> -->
      <!-- <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
        <div class="absolute z-0 overflow-hidden" style="left: 20px; top: 50px;">
          <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg" />
        </div>
      </div> -->
    </header>

    <div class="relative row singleContentWrapper postContentWrapper">
      <div class="relative z-10 pageInner">
        <div class="py-16 border-b border-gray-200 singleContent postContent editableContent">
          <?php the_content(); ?>
          <div class="espace20"></div>
        </div>
      </div>
    </div>

    <div class="absolute inset-0 z-0 overflow-hidden">
      <div 
        class="absolute z-0"
        style="
          background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/very_big_bg_circles.svg);
          background-position: center right;
          background-repeat: no-repeat;
          width: 1220px;
          height: 1220px;
          top: 250px;
          right: -800px;
          background-size: contain;"
        >
      </div>
    </div>

  </div>

  <?php
endwhile;
?>
<?php include(locate_template('includes/spre_news_block_white.php')); ?>
<?= do_shortcode('[block id="285"]') ?>
<?php get_footer(); ?>
