<?php
  $post = get_post();
  // var_dump($post->post_type);
  $type = 'spre_user_category';
  if($post->post_type === 'help_center') {
    $type = 'help_center_category';
  }
  $user_cat = get_the_terms($post->ID, $type);
  // var_dump($user_cat);
  if($user_cat && $user_cat[0]){
    $user_cat_ID = $user_cat[0]->term_id;
    $args = array(
      'post_type' => 'downloads',
      'posts_per_page' => -1,
      'orderby' => 'post_date',
      'order' => 'DESC',
      'suppress_filters' => 0 ,
      'tax_query' => array(
        array(
          'taxonomy' => $type,
          'field' => 'term_id',
          'terms' => $user_cat_ID,
        )
      )
    );
    $blocks = get_posts( $args );
?>

<div class="relative z-0">
  <div class="box-content px-6 pt-10 pb-8 mx-auto bg-white rounded-md sm:px-12 shadow-spre">
    <div class="flex items-baseline mb-4">
      <div class="text-2xl font-bold text-spre-purple"><?= $spre_title; ?></div>
      <img class="ml-8" src="<?= $spre_icon_url; ?>" />
    </div>
    <div class="mt-8 mb-10 text-base italic font-light leading-1_4 text-spre-brown_dark"><?= $spre_content; ?></div>
    <?php
      foreach ($blocks as $block) {
        $file_cat = get_the_terms($block->ID, 'download_category');
    ?>
      <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="flex flex-col w-full p-5 mb-5 transition duration-150 ease-in-out rounded-md cursor-pointer bg-spre-graybg hover:bg-spre-gray_ultralight">
        <div class="text-sm text-spre-purple"><?= $file_cat[0]->name ?></div>
        <div class="my-2 text-lg font-bold text-spre-red"><?= $block->post_title ?></div>
        <div class="spre_paragraph"><?= $block->post_content ?></div>
        <div class="flex mt-2 font-semibold">
          <img class="w-auto h-auto mr-2" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/file_download.svg" />
          <div>Télécharger le document</div>
        </div>
      </a>
    <?php } ?>
  </div>
</div>
<?php } ?>