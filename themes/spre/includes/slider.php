<?php
    $args = array(
        'post_type'      => 'slide',
        'orderby'        => 'menu_order',
        'order'          => 'ASC',
        'posts_per_page' => -1,
        'suppress_filters' => 0
    );
    $slides = get_posts( $args );
    // $slides = array_merge($slides, $slides);
?>   

<?php if ($slides){ enqueue_swiper();
?>
<div class="homeSliderWrapper">
    <div class="swiper-container homeSlider">
        <div class="swiper-wrapper">
            <?php 
            $index = 0;
            foreach ( $slides as $slide ) {
                setup_postdata( $post );
                $button_text = get_post_meta($slide->ID, 'button_text', true);
                $button_url = get_post_meta($slide->ID, 'button_url', true);
                $text_color = get_post_meta($slide->ID, 'text_color', true);
                $div = ($index == 0) ? 'h1' : 'h2';
            ?>
            <div class="swiper-slide" style="background-image: url(<?= spre_get_featured_image_url($slide->ID, 'slider'); ?>);">
                <div class="contentWrapper <?= $text_color; ?>">
                    <<?= $div; ?> class="title"><?= $slide->post_title; ?></<?= $div; ?>>
                    <div class="content">
                        <?= apply_filters('the_content', $slide->post_content); ?>
                    </div>
                    <?php if ($button_url && $button_text){ ?>
                    <a href="<?= $button_url; ?>" class="button moreButton"><?= $button_text; ?></a>
                    <?php } ?>
                </div>
            </div>
            <?php $index++; } ?>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</div>
<?php } ?>