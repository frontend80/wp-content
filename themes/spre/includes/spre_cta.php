<div class="relative z-20">
  <!-- <div class="absolute top-0 bottom-0 left-0 right-0 md:right-1/3" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/vibes.svg)">
  </div> -->
  <div class="relative z-10 max-w-4xl px-10 py-12 mx-auto -mb-20 bg-white rounded-md shadow-spre">
    <div class="flex items-center">
      <div class="flex-shrink-0"><img class="w-20 md:w-auto" src="<?= $spre_icon_url; ?>" /></div>
      <div class="pl-8">
        <div class="mb-5 text-2xl font-semibold text-spre-red"><?= $spre_title; ?></div>
        <div class="mb-5 spre_chapeau"><?= $spre_content; ?></div>
        <div class="inline-flex rounded-md shadow-sm">
          <a href="<?= $spre_button_link; ?>" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-white bg-spre-brown_dark hover:bg-spre-brown hover:shadow-spre">
          <?= $spre_button; ?>
          </a>
        </div>
        <?php if($spre_button2) { ?>
          <div class="inline-flex rounded-md shadow-sm">
            <a href="<?= $spre_button_link2; ?>" class="inline-flex items-center justify-center py-2 ml-3 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-white bg-spre-red hover:bg-spre-red_hover hover:shadow-spre">
            <?= $spre_button2; ?>
            </a>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>