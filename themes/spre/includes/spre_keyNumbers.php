<div class="relative rounded-t-4xl bg-spre-brown_ultralight">
  <div class="relative z-10 pt-32 pb-64 mx-auto max-w-1100">
    <div class="relative xl:grid xl:grid-cols-2 xl:gap-16">
      <div class="flex flex-col items-end">
        <div class="flex">
          <div><img class="image" src="<?= $spre_icon_url; ?>" /></div>
          <div class="ml-8 spre_section_title_red"><?= $spre_title; ?></div>
        </div>
        <div  class="w-1/2 mt-6 mb-4 text-right spre_chapeau"><?= $spre_hat; ?></div>
      </div>
      <div class="relative xl:divide-x xl:grid xl:grid-cols-2 xl:divide-spre-gray_light">
        <div class="xl:divide-y xl:divide-spre-gray_light">
          <div class="pb-8 pr-8">
            <div class="mb-2 text-5xl font-semibold leading-none text-spre-purple"><?= $spre_number1; ?></div>
            <div class="spre_paragraph"><?= $spre_content1; ?></div>
          </div>
          <div class="pt-8 pr-8">
            <div class="mb-2 text-5xl font-semibold leading-none text-spre-purple"><?= $spre_number3; ?></div>
            <div class="spre_paragraph"><?= $spre_content3; ?></div>
          </div>
        </div>
        <div class="xl:divide-y xl:divide-spre-gray_light">
          <div class="pb-8 pl-8">
            <div class="mb-2 text-5xl font-semibold leading-none text-spre-purple"><?= $spre_number2; ?></div>
            <div class="spre_paragraph"><?= $spre_content2; ?></div>
          </div>
          <div class="pt-8 pl-8">
            <div class="mb-2 text-5xl font-semibold leading-none text-spre-purple"><?= $spre_number4; ?></div>
            <div class="spre_paragraph"><?= $spre_content4; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="absolute z-0 overflow-hidden" style="bottom: -465px; left: -20px;">
    <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/round_numbers.svg" />
  </div>
</div>