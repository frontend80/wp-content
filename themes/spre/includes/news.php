<?php 
    if ($posts){
  $featured = isset($featured) ? $featured : false;
?>


<?php if (!$featured){ 
    $categories = get_terms(
        'category'
    );
?>
<form class="filtersForm" method="GET">
    <select name="cat" class="styled">
        <option value="">Catégorie</option>
        <?php foreach ($categories as $category){ ?><option value="<?= $category->slug; ?>" <?php if ($current_cat == $category->slug){ ?>selected="selected"<?php } ?>><?= $category->name; ?></option><?php } ?>
    </select>
</form>
<script type="text/javascript">
(function($){
    $(document).ready(function(){
        $('.filtersForm select').change(function(){
            $('.filtersForm').submit();
        })
        Custom.init();
    });
})(jQuery);
</script>
<?php } ?>

<div class="postsListWrapper">
    <?php if ( $featured && $all_link ){ ?>
    <a href="<?= $all_link; ?>" class="button allLink noMobile">Voir toutes les actualités</a>
    <div class="clear"></div>
    <?php } ?>
    <div class="row postsList newsList<?= $featured ? 'featured' : ''; ?>">
        <?php include(locate_template('includes/posts_loop_content.php')); ?>
        <div class="clear"></div>
    </div>
    <?php if ( $featured && $all_link ){ ?>
    <a href="<?= $all_link; ?>" class="button allLink mOnly">Voir toutes les actualités</a>
    <div class="clear"></div>
    <?php } ?>
</div>

<?php if ( !$featured ){ ?>
<div class="textCenter"><a href="javascript:;" class="button morePosts moreNews"><?= __("Afficher + d'articles", "spre"); ?></a></div>
<script type="text/javascript">
    <?php
        $all_posts = spre_get_posts(-1, 0, $current_tag, $current_cat, $current_year);
    ?>
    var posts_count = <?= count( $all_posts ); ?>,
        current_tag = '<?= $current_tag; ?>',
        current_cat = '<?= $current_cat; ?>',
        current_year = '<?= $current_year; ?>',
        posts_per_page = '<?= $count; ?>';
</script>
<?php }
} ?>