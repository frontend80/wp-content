<div class="imageContentWrapper">
    <div class="imageContent <?= $type; ?> <?= $style; ?>">
        <?php if ($type=='textLeft_imageRight23'){ ?>
        <img class="image" src="<?= $image_url; ?>" />
        <?php }else{ ?>
        <div class="image" <?php if ( $image_url ){ ?>style="background-image: url(<?= $image_url; ?>);"<?php } ?>>
            <?php if ( $image_legend ){ ?><span class="legend"><?= $image_legend; ?></span><?php } ?>
        </div>
        <?php } ?>
        <div class="content">
            <?= apply_filters('the_content', $content); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>