<div
    x-data="{ isOpen: false }"  
    @keydown.escape="isOpen2 = false"
    class="relative">
    <div
        class="cursor-pointer"
        @click="isOpen = !isOpen"
        title="<?= __('Partager cette page', 'spre'); ?>"><?= spre_svg('share'); ?>
    </div>
    <div
        x-show="isOpen"
        @click.away="isOpen = false"
        class="absolute top-0 right-0 z-10 block w-6 mt-8"
    >
        <?php include(locate_template('includes/social_share_inner.php')); ?>
    </div>
    <div class="clear"></div>
</div>