<?php
    $picto_version= isset($picto_version) ? '_'.$picto_version : '';
?>
<div class="socials socialLinks">
    <?php if (spre_get_option( 'linkedin_url' )){ ?>
    <a class="linkedin" href="<?= spre_get_option( 'linkedin_url' ); ?>" title="spre sur LinkedIn" target="_blank"><span class="icon"><?= spre_svg('socials/linkedin'.$picto_version); ?></span></a>
    <?php } ?>
    <?php if (spre_get_option( 'twitter_url' )){ ?>
    <a class="twitter" href="<?= spre_get_option( 'twitter_url' ); ?>" title="spre sur Twitter" target="_blank"><span class="icon"><?= spre_svg('socials/twitter'.$picto_version); ?></span></a>
    <?php } ?>
    <?php if (spre_get_option( 'facebook_url' )){ ?>
    <a class="twitter" href="<?= spre_get_option( 'facebook_url' ); ?>" title="spre sur Facebook" target="_blank"><span class="icon"><?= spre_svg('socials/facebook'.$picto_version); ?></span></a>
    <?php } ?>
    <?php if (spre_get_option( 'youtube_url' )){ ?>
    <a class="youtube" href="<?= spre_get_option( 'youtube_url' ); ?>" title="spre sur YouTube" target="_blank"><span class="icon"><?= spre_svg('socials/youtube'.$picto_version); ?></span></a>
    <?php } ?>
    <div class="clear"></div>
</div>