<?php if ( $posts ){ ?>

<?php
$position = $featured ? 1 : 0;
$is_ajax = isset($is_ajax) ? $is_ajax : false;
foreach ( $posts as $post ) :
    if (in_array($position, array(0, 4))){
        echo '<div class="col-12">';
    }else if (in_array($position, array(1, 2, 5, 6))){
        echo '<div class="col-6 col-m-12">';
    }
?>
    <?php include(locate_template('includes/news_item.php')); ?>
<?php
    if (in_array($position, array(0, 1, 3, 4, 5, 7))){
        echo '</div>';
    }
?>
    
<?php
    if (!$featured && $position == 3 && spre_get_option('blog_block_id') && !$is_ajax){
        echo '<div class="col-12"><div class="espace120"></div></div>';
        echo '<div class="col-12">'.do_shortcode('[block id="'.spre_get_option('blog_block_id').'"]').'</div>';
        echo '<div class="col-12"><div class="espace120"></div></div>';
    }
    $position++;
    if ($position > 7){ $position = 0; }
endforeach; ?>

<?php }else{ ?>
<div class="col-12">
    <h2 class="textCenter">Aucun article trouvé</h2>
    <div class="esapce20"></div>
</div>
<?php } ?>