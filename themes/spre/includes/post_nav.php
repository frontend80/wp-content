<?php
    ob_start();
    previous_post_link( '%link', spre_svg('arrow_left'), false );
    $prev_link = ob_get_contents();
    ob_end_clean();

    $all_link_url = get_site_url() . "/infos";
    
    ob_start();
    next_post_link( '%link', spre_svg('arrow_right'), false );
    $next_link = ob_get_contents();
    ob_end_clean();
?>
<nav class="postsNav">
    <ul class="flex">
        <?php if ($prev_link){ ?><li class="mr-4 prev"><?php echo $prev_link; ?></li><?php } ?>
        <?php if ($all_link_url){ ?><li class="mr-4 text-base font-semibold leading-none uppercase all text-spre-brown_dark"><a href="<?= $all_link_url; ?>"><?= __("Retour à la liste", "spre"); ?></a></li><?php } ?>
        <?php if ($next_link){ ?><li class="next"><?php echo $next_link; ?></li><?php } ?>
    </ul>
</nav>