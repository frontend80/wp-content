<?php
  $post = get_post();
  $user_cat = get_the_terms($post->ID, 'spre_user_category');
  $user_cat_ID = $user_cat[0]->term_id;
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'suppress_filters' => 0,
    'tax_query' => array(
      array(
        'taxonomy' => 'spre_user_category',
        'field' => 'term_id',
        'terms' => $user_cat_ID,
      )
    )
  );
  $blocks = get_posts( $args );

  $template_uri = get_template_directory_uri();
  $title = 'Les infos sur le même thème';
  $chapeau = 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.';
  $icon_url = get_template_directory_uri() . '/assets/images/svg/icons/news.svg';

  $bg_full = '';
  $bg_container = 'bg-white rounded-md box-content';
  $bg_box = 'bg-spre-brown_ultralight';
  if(true) {
    $bg_full = 'bg-spre-brown_ultralight rounded-t-4xl';
    $bg_container = '';
    $bg_box = 'bg-white';
  }
?>

<div class="relative z-0 -mb-14 <?= $bg_full; ?>">
  <div class="pt-20 pb-36 mx-auto <?= $bg_container; ?> max-w-1100">
    <div class="flex items-baseline mb-8">
      <div class="spre_section_title_red"><?= $title; ?></div>
      <img class="ml-8" src="<?= $icon_url; ?>" />
    </div>
    <div class="mb-4 spre_chapeau"><?= $chapeau; ?></div>
    <div class="flex flex-wrap justify-center pt-12">
      <?php
        foreach ($blocks as $block) {
          $cats = get_the_category( $block->ID );
          $date = get_the_time('d M Y', $block->ID);
      ?>
        <div class="flex w-full p-2 lg:w-1/3">
          <a href="<?php echo site_url() . '/' . $block->post_name ?>" class="w-full p-6 rounded-md group hover:shadow-spre <?= $bg_box; ?>">
            <div class="mb-4 text-sm font-medium text-spre-red"><?= $cats[0]->name ?> - <?= $date ?></div>
            <div class="mb-4 text-lg font-semibold leading-snug text-spre-purple group-hover:text-spre-red"><?= $block->post_title ?></div>
            <div class="spre_paragraph"><?= $block->post_excerpt ?></div>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
