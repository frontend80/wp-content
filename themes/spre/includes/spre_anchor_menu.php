<div class="relative z-0">
  <div class="relative z-0 py-5 mx-auto bg-white rounded-md shadow-spre">
    <div class="relative z-10 px-6 pt-2 pb-4">
      <div class="pr-12 text-2xl font-semibold text-spre-red"><?= $spre_title; ?></div>
      <?php if ($spre_menu1 && $spre_anchor1) { ?>
        <div class="flex mt-5 mb-4 text-base font-semibold leading-normal text-left text-spre-brown_dark hover:text-spre-red">
          <img class="w-auto h-auto mr-2" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/arrow_right.svg" />
          <a class="border-b-2 border-transparent hover:border-spre-red" href="<?= $spre_anchor1; ?>"><?= $spre_menu1; ?></a>
        </div>
      <?php } ?>
      <?php if ($spre_menu2 && $spre_anchor2) { ?>
        <div class="flex mt-5 mb-4 text-base font-semibold leading-normal text-left text-spre-brown_dark hover:text-spre-red">
          <img class="w-auto h-auto mr-2" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/arrow_right.svg" />
          <a class="border-b-2 border-transparent hover:border-spre-red" href="<?= $spre_anchor2; ?>"><?= $spre_menu2; ?></a>
        </div>
      <?php } ?>
      <?php if ($spre_menu3 && $spre_anchor3) { ?>
        <div class="flex mt-5 mb-4 text-base font-semibold leading-normal text-left text-spre-brown_dark hover:text-spre-red">
          <img class="w-auto h-auto mr-2" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/arrow_right.svg" />
          <a class="border-b-2 border-transparent hover:border-spre-red" href="<?= $spre_anchor3; ?>"><?= $spre_menu3; ?></a>
        </div>
      <?php } ?>
      <?php if ($spre_menu4 && $spre_anchor4) { ?>
        <div class="flex mt-5 mb-4 text-base font-semibold leading-normal text-left text-spre-brown_dark hover:text-spre-red">
          <img class="w-auto h-auto mr-2" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/arrow_right.svg" />
          <a class="border-b-2 border-transparent hover:border-spre-red" href="<?= $spre_anchor4; ?>"><?= $spre_menu4; ?></a>
        </div>
      <?php } ?>
      <?php if ($spre_menu5 && $spre_anchor5) { ?>
        <div class="flex mt-5 mb-4 text-base font-semibold leading-normal text-left text-spre-brown_dark hover:text-spre-red">
          <img class="w-auto h-auto mr-2" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/arrow_right.svg" />
          <a class="border-b-2 border-transparent hover:border-spre-red" href="<?= $spre_anchor5; ?>"><?= $spre_menu5; ?></a>
        </div>
      <?php } ?>
    </div>
  </div>
</div>