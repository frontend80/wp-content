
<div class="relative z-0" style="margin-bottom: -62px !important;">
  <div class="relative mt-0 mb-0 rounded-t-4xl bg-spre-purple">
    <div class="relative z-10 pt-32 pb-64 mx-auto max-w-1100">
      <div class="relative lg:divide-x-2 lg:divide-white lg:grid lg:grid-cols-2">
        <div class="flex flex-col items-start pl-10 pr-0 mb-10 lg:pl-0 lg:pr-10 lg:items-end lg:mb-0">
          <div class="flex">
            <div class="hidden lg:block"><img class="image" src="<?= $spre_icon_url1; ?>" /></div>
            <div class="mt-2 ml-0 mr-8 text-2xl font-semibold lg:ml-8 lg:mr-0 text-spre-purple_light"><?= $spre_title1; ?></div>
            <div class="block lg:hidden"><img class="image" src="<?= $spre_icon_url1; ?>" /></div>
          </div>
          <div  class="w-3/4 mt-5 mb-4 text-lg leading-normal text-left lg:text-right font-regular text-spre-white"><?= $spre_content1; ?></div>
          <div class="inline-flex mt-5 rounded-md shadow-sm hover:shadow-spre">
            <a href="<?= $spre_button_link1; ?>" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-brown_dark bg-spre-white hover:bg-spre-brown_light hover:shadow-spre">
            <?= $spre_button1; ?>
            </a>
          </div>
        </div>
        <div class="flex flex-col items-start pl-10">
          <div class="flex">
            <div class="mt-2 mr-8 text-2xl font-semibold text-spre-purple_light"><?= $spre_title2; ?></div>
            <div><img class="image" src="<?= $spre_icon_url2; ?>" /></div>
          </div>
          <div  class="w-3/4 mt-5 mb-4 text-lg leading-normal text-left font-regular text-spre-white"><?= $spre_content2; ?></div>
          <div class="inline-flex mt-5 rounded-md shadow-sm hover:shadow-spre">
            <a href="<?= $spre_button_link2; ?>" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-brown_dark bg-spre-white hover:bg-spre-brown_light hover:shadow-spre">
            <?= $spre_button2; ?>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="absolute bottom-0 z-0 overflow-hidden" style="left: -250px;">
      <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/circle_help.svg" />
    </div>
    <div class="absolute bottom-0 right-0 z-0 overflow-hidden">
      <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/points_help.svg" />
    </div>
  </div>
</div>