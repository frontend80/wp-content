<?php
  // $terms = get_terms('help_center_category');
  $args = array(
    'post_type' => 'help_center',
    'posts_per_page' => -1,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'suppress_filters' => 0 ,
  );
  $helps = get_posts( $args );
  $title = 'Également dans le centre d’aide';
  $chapeau = 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.';
  // var_dump($helps)
?>

<div class="relative z-0 -mb-14 bg-spre-brown_ultralight rounded-t-4xl">
  <div class="px-4 pt-20 mx-auto pb-36 max-w-1100">
    <div class="flex items-baseline mb-8">
      <div class="spre_section_title_red"><?= $title; ?></div>
    </div>
    <div class="mb-4 spre_chapeau"><?= $chapeau; ?></div>
    <div class="flex flex-wrap justify-start pt-12">
      <?php
        foreach ($helps as $help) {
          // $cats = get_the_category( $block->ID );
          // $date = get_the_time('d M Y', $block->ID);
      ?>
        <div class="flex w-full p-0 py-2 lg:px-2 lg:w-1/2">
          <a href="<?php echo site_url() . '/éléments/' . $help->post_name ?>" class="flex items-center justify-between w-full p-6 rounded-md bg-spre-brown_dark hover:bg-spre-red">
            <div class="font-semibold leading-snug text-white text-xxl"><?= $help->post_title ?></div>
            <div><img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/chevron_right.svg" /></div>
          </a>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
