<?php
    $position = isset($postition) ? $postition : 0;
    $image_version = (in_array($position, array(0, 1))) ? 'large' : 'thumbnail';
    $image_url = spre_get_featured_image_url($post->ID, $image_version);
    $link_url = get_permalink($post->ID);
    $date = get_the_time('d/m/Y', $post->ID);

    $categories = get_the_terms($post->ID, 'category');
    $category = $categories ? $categories[0]->name : '';
?>
  <div class="flex w-full p-2 lg:w-1/3">
    <a href="<?= $link_url; ?>" class="w-full p-6 rounded-md group hover:shadow-spre bg-spre-brown_ultralight">
      <?php if($post->post_type == 'post') { ?>
        <div class="mb-4 text-sm font-medium text-spre-red"><?= $category; ?> - <?= $date; ?></div>
      <?php } ?>
      <div class="h-12 mb-4 text-lg font-semibold leading-snug text-spre-purple group-hover:text-spre-red"><?= $post->post_title ?></div>
      <?php if($post->post_type == 'post') { ?>
        <div class="flex items-center"><?= spre_svg('clock'); ?> <span class="ml-3 text-base"><?= spre_get_time_reading($post->ID); ?> de lecture</span></div>
      <?php } ?>
      <div class="block overflow-hidden spre_paragraph h-28"><?= $post->post_excerpt ?></div>
    </a>
  </div>
<!-- <a href="<?= $link_url; ?>" class="newsItem position<?= $position; ?> <?= $image_url ? '' : 'noImage'; ?>">
  <div class="postContent">
    <div class="postMetas">
      <span class="category"><?= $category; ?></span>
      <span class="date"> - <?= $date; ?></span>
    </div>
    <h3><?= truncateHtml($post->post_title, 110); ?></h3>
    <div class="timeReading"><?= spre_svg('clock'); ?> <span><?= spre_get_time_reading($post->ID); ?> de lecture</span></div>
    <?php if ($position == 0){ ?><div class="date"><?= $date; ?></div><?php } ?>
  </div>
  <div class="featuredImg" <?php if ($image_url){ ?>style="background-image: url(<?= $image_url; ?>);"<?php } ?>></div>
  <div class="clear"></div>
</a> -->