<?php
  $terms = get_terms('spre_user_category');
?>

<div class="relative">
  <div class="relative z-10 pt-20 pb-40 mx-auto max-w-1100">
    <div class="relative flex flex-wrap justify-center">
      <?php 
        foreach ($terms as $term) {
          $args = array(
            'post_type' => 'spre_user',
            'posts_per_page' => -1,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'suppress_filters' => 0 ,
            'tax_query' => array(
              array(
                'taxonomy' => 'spre_user_category',
                'field' => 'term_id',
                'terms' => $term->term_id,
              )
            )
          );
          $term_posts = get_posts( $args );
          $src = wp_get_attachment_image_src( get_post_thumbnail_id($term_posts[0]->ID), 'thumbnail_size' );
          $url = $src[0];
      ?>
        <div class="relative flex w-full p-2 md:w-1/2 lg:w-1/4">
          <?php if(count($term_posts) <= 1) { ?>
          <a class="flex flex-col justify-between flex-1 h-auto p-6 transition duration-150 ease-in-out rounded-md hover:text-spre-red bg-spre-brown_ultralight hover:shadow-spre" href="./../utilisateurs/<?= $term_posts[0]->post_name; ?>">
            <div class="font-semibold leading-none text-xxl">
              <?= $term->name; ?>
            </div>
            <div class="flex items-center justify-center w-full pt-4">
              <img class="w-auto" src="<?= $url; ?>" />
            </div>
          </a>
          <?php } else { ?>
            <div class="flex flex-col justify-between flex-1 h-auto p-6 transition duration-150 ease-in-out rounded-md bg-spre-brown_ultralight hover:shadow-spre">
              <div class="font-semibold leading-none text-xxl">
                <?= $term->name; ?>
              </div>
              <?php if($term->name === 'Radios') { ?>
                <div class="relative flex items-center justify-center w-full pt-4">
                  <img class="absolute bottom-0 w-auto" style="right:-24px;" src="<?= $url; ?>" />
                </div>
              <?php } else { ?>
                <div class="flex items-center justify-center w-full pt-4">
                  <img class="w-auto" src="<?= $url; ?>" />
                </div>
              <?php } ?>
            </div>
            <div class="absolute inset-0 transition duration-150 ease-in-out opacity-0 hover:opacity-100">
              <div class="flex flex-col items-center justify-center w-full h-full p-6 bg-white bg-opacity-50">
                <?php foreach ($term_posts as $term_post) { ?>
                  <div class="inline-flex w-full my-2 rounded-md shadow-sm hover:shadow-md">
                    <a href="<?php echo site_url() . '/utilisateurs/' . $term_post->post_name ?>" class="inline-flex items-center justify-center w-full px-6 py-1 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md text-spre-brown_dark bg-spre-white hover:shadow-md">
                    <?= $term_post->post_title ?>
                    </a>
                  </div>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
  </div>
  <div class="absolute z-0 overflow-hidden" style="bottom: -600px; right: -270px;">
    <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/big_circles.svg" />
  </div>
</div>