<div class="relative z-0">
  <div class="box-content px-12 pt-16 pb-12 mx-auto -mt-20 bg-white rounded-md max-w-1100">
    <div class="flex items-baseline mb-4">
      <div class="spre_section_title_red"><?= $spre_title; ?></div>
      <img class="ml-8" src="<?= $spre_icon_url; ?>" />
    </div>
    <div class="mb-4 spre_chapeau"><?= $spre_content; ?></div>
    <a class="text-base font-semibold text-spre-purple" href="<?= $spre_button_link; ?>"><?= $spre_button; ?></a>
    <div class="pt-12 xl:grid xl:grid-cols-3 xl:gap-5">
      <div class="w-full p-6 rounded-md bg-spre-brown_ultralight">
        <div class="mb-4 text-sm font-medium text-spre-red">Thématique - XX  MOIS XXXX</div>
        <div class="mb-4 text-lg font-semibold leading-snug text-spre-purple">Excepteur sint occaecat cupidatat non proident, sunt in</div>
        <div class="spre_paragraph">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim. sunt in culpa qui officia deserunt mollit anim.</div>
      </div>
      <div class="w-full p-6 rounded-md bg-spre-brown_ultralight">
        <div class="mb-4 text-sm font-medium text-spre-red">Thématique - XX  MOIS XXXX</div>
        <div class="mb-4 text-lg font-semibold leading-snug text-spre-purple">Excepteur sint occaecat cupidatat non proident, sunt in</div>
        <div class="spre_paragraph">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim. sunt in culpa qui officia deserunt mollit anim.</div>
      </div>
      <div class="w-full p-6 rounded-md bg-spre-brown_ultralight">
        <div class="mb-4 text-sm font-medium text-spre-red">Thématique - XX  MOIS XXXX</div>
        <div class="mb-4 text-lg font-semibold leading-snug text-spre-purple">Excepteur sint occaecat cupidatat non proident, sunt in</div>
        <div class="spre_paragraph">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim. sunt in culpa qui officia deserunt mollit anim.</div>
      </div>
    </div>
  </div>
</div>
