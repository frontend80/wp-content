<?php
$background = 'bg-spre-red';
$padding = 'px-10 pb-6 pt-8';

if ($spre_button_style === 'purple') {
  $background = 'bg-spre-purple';
  $padding = 'px-12 pb-10 pt-14';
} 
?>

<div class="relative z-0">
  <div class="relative z-0 mx-auto rounded-md <?= $background ?> max-w-1100">
    <div class="relative z-10 <?= $padding ?>">
      <div class="text-3xl font-semibold text-spre-white"><?= $spre_title; ?></div>
      <div  class="mt-5 mb-4 text-lg leading-normal text-left font-regular text-spre-white"><?= $spre_content; ?></div>
      <div class="inline-flex mt-5 rounded-md shadow-sm">
        <a href="<?= $spre_button_link; ?>" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-brown_dark bg-spre-white">
        <?= $spre_button; ?>
        </a>
      </div>
    </div>
    <?php if ($spre_button_style === 'purple') { ?>
      <div class="absolute z-0 overflow-hidden" style="right: -3px; bottom: -3px;">
        <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/circle_alert.svg" />
      </div>
    <?php } else { ?>
      <div class="absolute z-0 overflow-hidden" style="right: 0px; top: 0px;">
        <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/feature_round.svg" />
      </div>
    <?php } ?>
  </div>
</div>