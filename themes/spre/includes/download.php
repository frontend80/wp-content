
<a href="<?= $download_url; ?>" class="downloadLink <?= $style; ?>">
    <img src="<?= $image_url; ?>" />
    <div class="actions">
        <?= spre_svg('download_blue'); ?>
        <span class="title">Télécharger</span>
    </div>
</a>