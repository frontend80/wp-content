<?php
   $doCat = isset($doCat) ? $doCat : false; 

   if ($post && $post->ID && !$doCat){
      $id = $post->ID;
      $title = $post->post_title;
      $url = get_permalink($post->ID);
      $url_short = wp_get_shortlink();
      $share_excerpt = $title . '%0D%0A%0D%0A' . wp_strip_all_tags(get_the_excerpt($post->ID)) . '%0D%0A%0D%0A' . $url;
   }
   else if (is_category()) {
      $id = the_category_ID($echo=false);
      $url = get_category_link($id);
      $url_short = $url;
      $title = get_cat_name($id);
   }
   else if ($term_id) {
      $id = $term_id;
      $term = get_term($id, 'category');
      $title = $term->name;
      $url = get_term_link($term);
      $url_short = $url;
   }else{
      $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
   }
   $email_object = __('visitez cette page sur le site spre France :', "spre") . $title;
   $twitter_share = urlencode( $title . ' ' . $url_short );
?>
<ul class="socials socialShare">
    <li class="mb-1"><a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=$url; ?>" title="<?php _e('Partager sur', 'spre'); ?> Facebook" target="_blank"><?= spre_svg('socials/facebook'); ?></a></li>
    <li class="mb-1"><a class="twitter" href="https://twitter.com/home?status=<?= $twitter_share; ?>" title="<?php _e('Partager sur', "spre"); ?> Twitter" target="_blank"><?= spre_svg('socials/twitter'); ?></a></li>
    <li class="mb-1"><a class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $url; ?>&title=<?= $title; ?>" title="<?php _e('Partager sur', "spre"); ?> LinkedIn" target="_blank"><?= spre_svg('socials/linkedin'); ?></a></li>
    <li class="mb-1"><a class="email" href="mailto:?subject=spre France : <?= $title; ?>&body=<?= $share_excerpt; ?>" title="<?php _e('Partager par mail', "spre"); ?>" target="_blank"><?= spre_svg('socials/mail'); ?></a></li>
</ul>