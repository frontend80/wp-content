<?php
  $terms = get_terms('help_center_category');
?>

<div class="relative">
  <div class="relative z-10 pt-0 pb-20 mx-auto max-w-1100">
    <div class="relative flex flex-wrap justify-center">
      <?php 
        foreach ($terms as $term) {
          $args = array(
            'post_type' => 'help_center',
            'posts_per_page' => -1,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'suppress_filters' => 0 ,
            'tax_query' => array(
              array(
                'taxonomy' => 'help_center_category',
                'field' => 'term_id',
                'terms' => $term->term_id,
              )
            )
          );
          $term_posts = get_posts( $args );
          $src = wp_get_attachment_image_src( get_post_thumbnail_id($term_posts[0]->ID), 'thumbnail_size' );
          $url = $src[0];

          $name = $term->name;
          if($term->name === 'Dématérialisation') {
            $name = '<span class="block lg:hidden">Dématérialisation</span><span class="hidden lg:block">Dématéria-<br />lisation</span>';
          }

      ?>
        <div class="flex w-full p-2 md:w-1/2 lg:w-1/5">
          <div class="relative flex flex-col justify-start flex-1 h-auto max-w-full px-4 pt-4 transition duration-150 ease-in-out rounded-md text-spre-gray_light pb-28 md:pb-20 bg-spre-purple hover:text-spre-purple_light hover:shadow-spre" href="./../utilisateurs/<?= $block->post_name; ?>">
            <div class="flex items-center justify-start w-full h-14">
              <img class="w-auto" src="<?= $url; ?>" />
            </div>
            <div class="mt-2 text-xl font-bold leading-tight break-words">
              <?= $name; ?>
            </div>
            <?php if(count($term_posts) <= 1) { ?>
              <div class="absolute inline-flex bottom-4 left-4 right-8">
                <a href="<?php echo site_url() . '/éléments/' . $term_posts[0]->post_name ?>" class="inline-flex items-center justify-center w-full py-1 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md text-spre-brown_dark bg-spre-white hover:bg-spre-brown_light hover:shadow-spre">
                En savoir +
                </a>
              </div>
            <?php } else { ?>
              <div class="absolute flex flex-col bottom-4 left-4 right-8">
                <?php foreach ($term_posts as $term_post) { ?>
                  <a href="<?php echo site_url() . '/éléments/' . $term_post->post_name ?>" class="inline-flex items-center justify-center w-full py-1 mt-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md text-spre-brown_dark bg-spre-white hover:bg-spre-brown_light hover:shadow-spre">
                    <?= $term_post->post_title ?>
                  </a>
                <?php } ?>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>