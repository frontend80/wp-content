<div class="relative">
  <div class="py-24 mx-auto bg-white max-w-1100">
    <div class="text-2xl font-bold text-spre-brown_dark"><?= $spre_title; ?></div>
    <div class="relative flex flex-wrap mt-6">
      <div class="flex items-center justify-center w-1/4 p-6">
        <a href="<?= $spre_logo_link1; ?>" target="_blank">
          <img class="image" src="<?= $spre_logo_url1; ?>" />
        </a>
      </div>
      <div class="flex items-center justify-center w-1/4 p-6">
        <a href="<?= $spre_logo_link2; ?>" target="_blank">
          <img class="image" src="<?= $spre_logo_url2; ?>" />
        </a>
      </div>
      <div class="flex items-center justify-center w-1/4 p-6">
        <a href="<?= $spre_logo_link3; ?>" target="_blank">
          <img class="image" src="<?= $spre_logo_url3; ?>" />
        </a>
      </div>
      <div class="flex items-center justify-center w-1/4 p-6">
        <a href="<?= $spre_logo_link4; ?>" target="_blank">
          <img class="image" src="<?= $spre_logo_url4; ?>" />
        </a>
      </div>
      <div class="flex items-center justify-center w-1/4 p-6">
        <a href="<?= $spre_logo_link5; ?>" target="_blank">
          <img class="image" src="<?= $spre_logo_url5; ?>" />
        </a>
      </div>
      <div class="flex items-center justify-center w-1/4 p-6">
        <a href="<?= $spre_logo_link6; ?>" target="_blank">
          <img class="image" src="<?= $spre_logo_url6; ?>" />
        </a>
      </div>
    </div>
  </div>
</div>