<div class="relative z-0">
  <div class="px-12 pt-16 pb-12 mx-auto -mt-20 bg-white rounded-md max-w-1100">
    <div class="pt-12 xl:grid xl:grid-cols-2 xl:gap-8">
      <div class="flex flex-col items-end w-full p-6">
        <div class="mb-13"><img class="image" src="<?= $spre_icon_url; ?>" /></div>
        <div class="w-3/4 text-xl font-light leading-normal text-right text-spre-red"><?= $spre_hat; ?></div>
      </div>
      <div class="w-full p-6">
        <div class="mb-12 spre_section_title_red"><?= $spre_title; ?></div>
        <div class="spre_paragraph"><?= $spre_content; ?></div>
      </div>
    </div>
  </div>
</div>
