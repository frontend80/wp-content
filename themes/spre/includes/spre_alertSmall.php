<?php
$background = 'bg-spre-red';
if ($spre_button_style === 'purple') {
  $background = 'bg-spre-purple';
} 
?>

<div class="relative z-0">
  <div class="relative z-0 py-5 mx-auto <?= $background; ?> rounded-md">
    <div class="relative z-10 px-6 pt-2 pb-4">
      <div class="pr-12 font-semibold text-xxl text-spre-white"><?= $spre_title; ?></div>
      <div  class="mt-5 mb-4 text-lg leading-normal text-left font-regular text-spre-white"><?= $spre_content; ?></div>
      <div class="inline-flex mt-5 rounded-md shadow-sm">
        <a href="<?= $spre_button_link; ?>" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-brown_dark bg-spre-white">
        <?= $spre_button; ?>
        </a>
      </div>
      <div class="absolute top-0 z-10 overflow-hidden right-6">
        <img class="w-auto h-auto max-w-full max-h-full" src="<?= $spre_icon_url; ?>" />
      </div>
    </div>
  </div>
</div>