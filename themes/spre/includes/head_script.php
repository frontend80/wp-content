<script type="text/javascript">
var spre_current_size = 'desktop',
    WindowW = 0,
    WindowH = 0;
(function($){
    function getAdminBarHeight(){
        if ( $('#wpadminbar').length ){
            return $('#wpadminbar').outerHeight();
        }else{
            return 0;
        }
    }
    function updateViewport(){
        // $("meta[name='viewport']").attr('content', 'width=device-width, initial-scale=1.0');
        $('html').css('overflow-y', 'hidden');
        WindowW = $(window).width();
        WindowH = $(window).height();
        $('html').css('overflow-y', 'visible');
        // Hmin = Math.max(H, W*1/2.3);
        Hmin = WindowH;
        
        if (WindowW < 768){ // Mobile
            spre_current_size = 'mobile';
            //$("meta[name='viewport']").attr('content', 'user-scalable=0, width=410');
        }
        else if (WindowW <= 991){ // Tablet
            spre_current_size = 'tablet';
            //$("meta[name='viewport']").attr('content', 'user-scalable=0, width=991');
        }
        else{
            spre_current_size = 'desktop';
            $("meta[name='viewport']").attr('content', 'width=device-width, initial-scale=1.0');
        }
        // $('body').css('min-height', Hmin-getAdminBarHeight());
    }
    $(window).resize(function() {
        updateViewport();
    });
    updateViewport();
        
    $(document).ready(function(){
    });
})(jQuery);
</script>