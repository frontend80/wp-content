<?php
  /**
   * Template Name: Page Contact
  **/
get_header();
while (have_posts()):

	the_post();

	// $header_url = spre_get_featured_image_url($post->ID, 'page_header');
	// $excerpt = $post->post_excerpt;
	// $h1_title = get_post_meta($post->ID, 'h1_title', true);
	// $h1_title = $h1_title ? $h1_title : $post->post_title;
  ?>
  <header class="relative z-0 pb-20 pageHeader">
    <div class="relative z-20 pageInner">
      <div class="relative flex justify-end w-full pt-10">
        <?php include locate_template('includes/social_share.php'); ?>
      </div>
      <div class="w-3/5 headerContent">
        <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($post->post_title) ?></h1>
        <?php if ($post->post_excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $post->post_excerpt
          ) ?></div>
        <?php } ?>
      </div>
      <?php if (get_the_post_thumbnail_url($post->ID)) { ?>
        <div class="absolute top-0 bottom-0 right-0 z-10 w-2/5 mt-20">
          <div class="absolute z-10 overflow-hidden" style="left: calc(50% - 70px); top: calc(50% - 47px);">
            <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_the_post_thumbnail_url($post->ID); ?>" />
          </div>
        </div>
        <div class="absolute top-0 bottom-0 right-0 z-0 w-2/5">
          <div class="absolute z-0" style='left: calc(50% - 282px); top: calc(50% - 242px); bottom: calc(50% - 322px); right: calc(50% - 282px); background-image: url("<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/cercles_contact.svg"); background-size: initial;'>
          </div>
        </div>
      <?php } ?>
    </div>
  </header>
  <div class="relative pageContent editableContent">
    <div class="pb-48 pageInner">
      <?php the_content(); ?>
      <div class="clear"></div>
    </div>
    <div class="absolute bottom-0 left-0 right-0 z-0 h-64 lg:right-1/3" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/vibes.svg)">
    </div>
    <div class="clear"></div>
  </div>
  <?php
endwhile;
?>
<?php get_footer(); ?>
