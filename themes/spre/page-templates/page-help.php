<?php
    /**
     * Template Name: Page Centre Aide
    **/
    get_header();
    while ( have_posts() ) : the_post();

    $header_url = spre_get_featured_image_url($post->ID, 'page_header');
    $excerpt = $post->post_excerpt;
    $title = $post->post_title;

?>
  <div class="relative">
    <div class="absolute inset-0 flex items-start justify-center">
    <img class="w-auto h-auto" src="<?= get_template_directory_uri() ?>/assets/images/svg/patterns/big_bg_circles.svg" />
    </div>
    <header class="relative z-0 pb-20 pageHeader">
      <div class="relative z-10 pageInner">
        <div class="relative flex justify-end w-full py-10">
          <?php include locate_template('includes/social_share.php'); ?>
        </div>
        <div class="flex flex-wrap">
          <div class="w-full md:w-1/2">
            <h1 class="mb-8 text-4xl spre_section_title_red"><?= nl2br($title) ?></h1>
            <?php if ($excerpt) { ?>
              <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
                'the_excerpt',
                $excerpt
              ) ?></div>
            <?php } ?>
          </div>
          <div class="flex items-center w-full mt-10 md:w-1/2 md:mt-0">
            <div x-data="{search:''}" class="flex w-full mt-1 rounded-md md:ml-8">
              <div class="relative flex items-stretch flex-grow focus-within:z-10">
                <div class="absolute inset-y-0 left-0 flex items-center pl-4 pointer-events-none">
                  <!-- Heroicon name: users -->
                  <img src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/search.svg" />
                </div>
                <input id="search" x-model="search" class="block w-full pl-12 transition duration-150 ease-in-out border-none rounded-none text-spre-brown_dark bg-spre-brown_ultralight form-input rounded-l-md sm:text-sm sm:leading-5" placeholder="Rechercher dans le centre d'aide">
              </div>
              <a x-bind:href="'../?s=' + search + '&type=help_center'" class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium leading-5 text-white transition duration-150 ease-in-out cursor-pointer rounded-r-md bg-spre-red hover:text-white hover:bg-spre-red_hover focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700">
                <!-- Heroicon name: sort-ascending -->
                <!-- <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h5a1 1 0 000-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM13 16a1 1 0 102 0v-5.586l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 101.414 1.414L13 10.414V16z" />
                </svg> -->
                <span class="">Rechercher</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- <?php if ($header_url) { ?>
        <div class="absolute top-0 bottom-0 right-0 z-0 w-1/2">
          <div class="absolute z-0 overflow-hidden" style="left: 20px; top: 0px;">
            <img class="w-auto h-auto max-w-full max-h-full" src="<?= $header_url ?>" />
          </div>
        </div>
      <?php } ?> -->
    </header>
    <div class="pageContent editableContent">
      <div class="pageInner">
        <?php the_content(); ?>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <?php endwhile; ?>
<?php get_footer(); ?>
