<?php
    /**
     * Template Name: Page Accueil
    **/
    get_header();
    while ( have_posts() ) : the_post();

    $header_url = spre_get_featured_image_url($post->ID, 'page_header');
    $excerpt = $post->post_excerpt;
    $title = $post->post_title;

?>
  <header class="relative z-0 pt-20 pb-36 pageHeader">
    <div class="relative z-10 pageInner ">
      <div class="w-2/3 headerContent">
        <h1 class="mb-8 text-5xl font-semibold leading-none text-spre-purple"><?= nl2br($title) ?></h1>
        <?php if ($excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $excerpt
          ) ?></div>
        <?php } ?>
      </div>
    </div>
    <?php if ($header_url) { ?>
      <div class="absolute top-0 bottom-0 right-0 z-0 w-1/2">
        <div class="absolute z-0 overflow-hidden" style="left: 20px; top: 0px;">
          <img class="w-auto h-auto max-w-full max-h-full" src="<?= $header_url ?>" />
        </div>
      </div>
    <?php } ?>
  </header>
  <div class="pageContent editableContent">
    <div class="pageInner">
      <?php the_content(); ?>
      <div class="clear"></div>
    </div>
  </div>
  <?php endwhile; ?>
<?php get_footer(); ?>
