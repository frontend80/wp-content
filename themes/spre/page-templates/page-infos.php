<?php
  /**
   * Template Name: Page Infos
  **/
  get_header();
  while (have_posts()):

  the_post();

  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  // var_dump($paged);
  $postsPerPage = 15;
  $postOffset = $paged * $postsPerPage;
  $args = array( 'posts_per_page' => $postsPerPage, 'paged' => $paged, 'post_type' => 'post' );
  $postslist = new WP_Query( $args );
  // $blocks = $postslist->posts;
  // var_dump($blocks);

  $current_tag = isset($_GET['tag']) ? $_GET['tag'] : '';
  $current_cat = isset($_GET['cat']) ? $_GET['cat'] : '';
  $current_use = isset($_GET['user']) ? $_GET['user'] : '';
  $current_year = isset($_GET['y']) ? $_GET['y'] : '';
  $count = isset($_GET['count']) ? $_GET['count'] : get_option('posts_per_page');

  $blocks = spre_get_posts($count, 0, $current_tag, $current_cat, $current_use, $current_year);

  $categories = get_terms(
    'category'
  );
  $users = get_terms(
    'spre_user_category'
  );
  // var_dump($current_use);

  $bg_full = '';
  $bg_container = 'bg-white rounded-md box-content';
  $bg_box = 'bg-spre-brown_ultralight';
?>
  <header class="relative z-0 pb-20 pageHeader">
    <div class="relative z-20 pageInner">
      <div class="relative flex justify-end w-full pt-10">
        <?php include locate_template('includes/social_share.php'); ?>
      </div>
      <div class="w-2/3 headerContent">
        <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($post->post_title) ?></h1>
        <?php if ($post->post_excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $post->post_excerpt
          ) ?></div>
        <?php } ?>
      </div>
    </div>
    <?php if (get_the_post_thumbnail_url($post->ID)) { ?>
      <div class="absolute top-0 bottom-0 right-0 z-10 w-1/3">
        <div class="absolute z-10 overflow-hidden" style="left: 0px; top: 20%;">
          <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_the_post_thumbnail_url($post->ID); ?>" />
        </div>
      </div>
    <?php } ?>
    <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
      <div class="absolute bottom-0 right-0 z-0 overflow-hidden" style='left: 20px; top: 0px; background-image: url("<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg"); background-size: cover;'>
        <!-- <img class="w-auto h-auto max-w-full max-h-full" src="<?= $header_url ?>" /> -->
      </div>
    </div>
  </header>
  <div class="pageContent editableContent">
      <div class="pageInner">
        <div class="flex w-1/2 mb-16 headerContent">
          <div class="flex w-1/2 mr-3 headerContent">
            <form class="w-full filtersForm" method="GET">
              <div class="block w-full border-b border-black">
                <select id="location" name="cat" class="block w-full py-2 pl-0 pr-10 mt-1 mb-0 text-base font-semibold leading-6 border-none select-chevron form-select focus:outline-none sm:text-sm sm:leading-5">
                  <option value="">Thème</option>
                  <?php foreach ($categories as $category){ ?><option value="<?= $category->slug; ?>" <?php if ($current_cat && $current_cat == $category->slug){ ?>selected="selected"<?php } ?>><?= $category->name; ?></option><?php } ?>
                </select>
              </div>
            </form>
            <script type="text/javascript">
            (function($){
                $(document).ready(function(){
                    $('.filtersForm select').change(function(){
                        $('.filtersForm').submit();
                    })
                    Custom.init();
                });
            })(jQuery);
            </script>
          </div>
          <div class="flex w-1/2 headerContent">
            <form class="w-full filtersForm2" method="GET">
              <div class="block w-full border-b border-black">
                <select id="location" name="user" class="block w-full py-2 pl-0 pr-10 mt-1 mb-0 text-base font-semibold leading-6 border-none select-chevron form-select focus:outline-none sm:text-sm sm:leading-5">
                  <option value="">Type</option>
                  <?php foreach ($users as $user){ ?><option value="<?= $user->slug; ?>" <?php if ($current_use && $current_use == $user->slug){ ?>selected="selected"<?php } ?>><?= $user->name; ?></option><?php } ?>
                </select>
              </div>
            </form>
            <script type="text/javascript">
            (function($){
                $(document).ready(function(){
                    $('.filtersForm2 select').change(function(){
                        $('.filtersForm2').submit();
                    })
                    Custom.init();
                });
            })(jQuery);
            </script>
          </div>
        </div>
        <div class="flex flex-wrap justify-center pt-12">
          <?php
            foreach ($blocks as $block) {
              $cats = get_the_category( $block->ID );
              $date = get_the_time('d M Y', $block->ID);
          ?>
            <div class="flex w-full p-2 lg:w-1/3">
              <a href="<?php echo site_url() . '/' . $block->post_name ?>" class="w-full p-6 group rounded-md hover:shadow-spre <?= $bg_box; ?>">
                <div class="mb-4 text-sm font-medium text-spre-red"><?= $cats[0]->name ?> - <?= $date ?></div>
                <div class="h-12 mb-4 text-lg font-semibold leading-snug text-spre-purple group-hover:text-spre-red"><?= $block->post_title ?></div>
                <div class="block overflow-hidden spre_paragraph h-28"><?= $block->post_excerpt ?></div>
              </a>
            </div>
          <?php } ?>
        </div>
        <div class="flex flex-wrap items-center justify-center pt-24 pb-48">
          <!-- <div class="inline-flex rounded-md shadow-sm">
            <div class="textCenter">
              <a href="javascript:;" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md morePosts moreNews px-9 text-spre-white bg-spre-brown_dark hover:bg-spre-brown">
                <?= __("Afficher + d'articles", "spre"); ?>
              </a>
            </div>
            <script type="text/javascript">
                <?php
                    $blocks = spre_get_posts(-1, 0, $current_tag, $current_cat, $current_use, $current_year);
                ?>
                var posts_count = <?= count( $all_posts ); ?>,
                    current_tag = '<?= $current_tag; ?>',
                    current_cat = '<?= $current_cat; ?>',
                    current_use = '<?= $current_use; ?>',
                    current_year = '<?= $current_year; ?>',
                    posts_per_page = '<?= $count; ?>';
            </script>
          </div> -->
        </div>
          <!-- <?php the_content(); ?> -->
        <div class="clear"></div>
      </div>
  </div>
  <?php
endwhile;
?>
<?php get_footer(); ?>
