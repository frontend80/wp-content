<?php
    /**
     * Template Name: Page Actualités
    **/
    get_header();
    while ( have_posts() ) : the_post();

    $header_url = spre_get_featured_image_url($post->ID, 'page_header');
    $excerpt = $post->post_excerpt;
?>
    <header class="pageHeader" <?php if ($header_url){ ?>style="background-image: url(<?= $header_url; ?>);"<?php } ?>>
        <div class="pageInner">
            <?php if (function_exists('spre_bread')) spre_bread(); ?>
            <div class="headerContent">
                <h1><?php the_title(); ?></h1>
                <?php if ($excerpt){ ?>
                    <div class="excerpt contentIntro"><?= apply_filters('the_excerpt', $excerpt); ?></div>
                <?php } ?>
            </div>
            <?php include(locate_template('includes/social_share.php')); ?>
        </div>
    </header>
    <div class="subHeader"></div>
    <div class="pageContent editableContent">
        <div class="pageInner">
            <div class="row">
                <div class="col-9 col-m-8 col-s-12">
                    <div class="clear espace50"></div>
                    <?php the_content(); ?>
                    <div class="clear espace60"></div>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
<?php get_footer(); ?>