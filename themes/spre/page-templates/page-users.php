<?php
    /**
     * Template Name: Page Utilisateurs
    **/
    get_header();
    while ( have_posts() ) : the_post();

    $header_url = spre_get_featured_image_url($post->ID, 'page_header');
    $excerpt = $post->post_excerpt;
    $title = $post->post_title;

?>
  <header class="relative z-0 pb-36 pageHeader">
    <div class="relative z-10 pageInner ">
      <div class="relative flex justify-end w-full py-10">
        <?php include locate_template('includes/social_share.php'); ?>
      </div>
      <div class="w-3/4 lg:w-1/2 headerContent">
        <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($title) ?></h1>
        <?php if ($excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $excerpt
          ) ?></div>
        <?php } ?>
      </div>
    </div>
    <?php if ($header_url) { ?>
      <div class="absolute top-0 bottom-0 right-0 z-0 w-1/2">
        <div class="absolute bottom-0 right-0 z-0 overflow-hidden" style='left: 20px; top: 0px; background-image: url("<?= $header_url ?>"); background-size: auto 90%; background-repeat: no-repeat; background-position: left top;'>
        </div>
      </div>
    <?php } ?>
  </header>
  <div class="pageContent editableContent">
    <div class="pageInner">
      <?php the_content(); ?>
      <div class="clear"></div>
    </div>
  </div>
  <?php endwhile; ?>
<?php get_footer(); ?>
