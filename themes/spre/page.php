<?php
get_header();
while (have_posts()):

	the_post();

	// $header_url = spre_get_featured_image_url($post->ID, 'page_header');
	// $excerpt = $post->post_excerpt;
	// $h1_title = get_post_meta($post->ID, 'h1_title', true);
	// $h1_title = $h1_title ? $h1_title : $post->post_title;
  ?>
  <header class="relative z-0 pb-20 pageHeader">
    <div class="relative z-20 pageInner">
      <div class="relative flex justify-end w-full pt-10">
        <?php include locate_template('includes/social_share.php'); ?>
      </div>
      <div class="w-2/3 headerContent">
        <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($post->post_title) ?></h1>
        <?php if ($post->post_excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $post->post_excerpt
          ) ?></div>
        <?php } ?>
      </div>
    </div>
    <?php if (get_the_post_thumbnail_url($post->ID)) { ?>
      <div class="absolute top-0 bottom-0 right-0 z-10 w-1/3">
        <div class="absolute z-10 overflow-hidden" style="left: 0px; top: 20%;">
          <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_the_post_thumbnail_url($post->ID); ?>" />
        </div>
      </div>
    <?php } ?>
    <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
      <div class="absolute bottom-0 right-0 z-0 overflow-hidden" style='left: 20px; top: 0px; background-image: url("<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg"); background-size: cover;'>
        <!-- <img class="w-auto h-auto max-w-full max-h-full" src="<?= $header_url ?>" /> -->
      </div>
    </div>
  </header>
  <div class="pageContent editableContent">
      <div class="pageInner">
          <?php the_content(); ?>
          <div class="clear"></div>
      </div>
  </div>
  <?php
endwhile;
?>
<?php get_footer(); ?>
