<?php if ( spre_is_private_area() ){ spre_check_user_login(); } ?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php wp_title( '|', true, 'right' ); ?></title>

  <link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri(); ?>/assets/images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri(); ?>/assets/images/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri(); ?>/assets/images/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?= get_template_directory_uri(); ?>/assets/images/favicons/site.webmanifest">
  <link rel="mask-icon" href="<?= get_template_directory_uri(); ?>/assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#ffc40d">
  <meta name="theme-color" content="#ffffff">

  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]> <script src="<?= get_template_directory_uri(); ?>/assets/js/jquery.backgroundSize.js"></script> <![endif]-->
  <?php wp_head(); ?>
  <?php // include_once(locate_template('includes/head_script.php')); ?>
  <!-- <?php include_once(locate_template('includes/manage_cookies_js.php')); ?> -->
  <script type="module" src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.2/dist/alpine.min.js"></script>
  <script nomodule src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.2/dist/alpine-ie11.min.js" defer></script>
  <?php if ( spre_get_option( 'custom_code_head' ) ) { echo spre_get_option( 'custom_code_head' ); } ?>
  <!-- <style>
    .pageInner {
      margin-left: auto;
      margin-right: auto;
      max-width: 1100px;
      padding-left: 1rem;
      padding-right: 1rem
    }

    @media (min-width: 640px) {
      .pageInner {
        padding-left: 1.5rem;
        padding-right: 1.5rem;
      }
    }

    .spre_paragraph {
      font-size: .938rem;
      font-weight: 300;
      line-height: 1.8;
      --text-opacity: 1;
      color: #4A4A49;
      color: rgba(74, 74, 73, var(--text-opacity))
    }

    .spre_section_title_red {
      font-size: 2.063rem;
      font-weight: 600;
      --text-opacity: 1;
      color: #CC1F30;
      color: rgba(204, 31, 48, var(--text-opacity));
      line-height: 1.5
    }

    .spre_chapeau {
      font-size: 1.125rem;
      line-height: 1.5;
      font-weight: 400;
      --text-opacity: 1;
      color: #4A4A49;
      color: rgba(74, 74, 73, var(--text-opacity))
    }

    .spre_chapeau_purple {
      font-size: 1.125rem;
      line-height: 1.5;
      font-weight: 400;
      --text-opacity: 1;
      color: #3C287E;
      color: rgba(60, 40, 126, var(--text-opacity))
    }

    .menu-menu-principal li.menu-item {
      display: inline-flex;
      align-items: center;
      border-bottom-width: 2px;
      border-color: transparent;
      font-size: .938rem;
      font-weight: 600;
      line-height: 1.5rem;
      --text-opacity: 1;
      color: #CC1F30;
      color: rgba(204, 31, 48, var(--text-opacity));
      transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
      transition-duration: 150ms;
      transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1)
    }

    .menu-menu-principal li.menu-item:hover {
      border-bottom-width: 2px;
      --border-opacity: 1;
      border-color: #CC1F30;
      border-color: rgba(204, 31, 48, var(--border-opacity))
    }

    .menu-menu-droite li.menu-item {
      display: inline-flex;
      align-items: center;
      font-size: .813rem;
      font-weight: 600;
      line-height: 1.5rem;
      --text-opacity: 1;
      color: #4A4A49;
      color: rgba(74, 74, 73, var(--text-opacity));
      transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
      transition-duration: 150ms;
      transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1)
    }

    .menu-menu-droite li.menu-item:hover {
      --text-opacity: 1;
      color: #232323;
      color: rgba(35, 35, 35, var(--text-opacity));
      text-decoration: underline
    }

    .menu-menu-principal-mobile li.menu-item, .menu-menu-droite-mobile li.menu-item {
      display: flex;
      align-items: center;
      padding: 0.75rem;
      margin: -0.75rem;
      transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
      transition-duration: 150ms;
      transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
      border-radius: 0.375rem
    }

    .menu-menu-principal-mobile li.menu-item:hover, .menu-menu-droite-mobile li.menu-item:hover {
      --bg-opacity: 1;
      background-color: #f9fafb;
      background-color: rgba(249, 250, 251, var(--bg-opacity))
    }

    .menu-menu-principal-mobile li.menu-item a {
      font-size: .938rem;
      font-weight: 600;
      line-height: 1.5rem;
      --text-opacity: 1;
      color: #CC1F30;
      color: rgba(204, 31, 48, var(--text-opacity));
      width: 100%
    }

    .menu-menu-droite-mobile li.menu-item a {
      font-size: .813rem;
      font-weight: 600;
      line-height: 1.5rem;
      --text-opacity: 1;
      color: #4A4A49;
      color: rgba(74, 74, 73, var(--text-opacity));
      width: 100%
    }

    footer .menu-footer li.menu-item {
      margin-bottom: 1rem;
      transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
      transition-duration: 150ms;
      transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1)
    }

    footer .menu-footer li.menu-item:hover,
    footer .menu-footer-bottom li.menu-item:hover,
    footer .menu-footer-right1 li.menu-item:hover,
    footer .menu-footer-right2 li.menu-item:hover {
      text-decoration: underline;
      --text-opacity: 1;
      color: #232323;
      color: rgba(35, 35, 35, var(--text-opacity))
    }
  </style> -->
</head>

<body <?php body_class(); ?>>
  <header id="header">
    <div class="relative z-20">
      <div
        x-data="{ isOpen: false }"
        @keydown.escape="isOpen = false"
        class="relative"
      >
        <div class="px-4 mx-auto max-w-8xl sm:px-6">
          <div class="flex items-center justify-between py-5 lg:justify-start lg:space-x-10">
            <div class="flex">
              <div class="flex items-center flex-shrink-0">
                <a href="<?= home_url('/'); ?>" class="inline-flex">
                  <?php if (spre_get_option('logo1x')){ ?>
                    <img class="w-auto h-14 lg:h-20" alt="La SPRE" src="<?= spre_get_option('logo2x'); ?>" />
                  <?php } ?>
                </a>
              </div>
              <nav class="hidden space-x-10 lg:ml-6 lg:flex lg:items-center">
                <?php wp_nav_menu( array( 'theme_location' => 'header-main', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_class' => 'menu-menu-principal flex space-x-3 xl:space-x-12 ml-0 xl:ml-8', 'container' => false ) ); ?>
              </nav>
            </div>
            <div class="-my-2 -mr-2 lg:hidden">
              <button 
                @click="isOpen = !isOpen"
                type="button" 
                class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500"
              >
                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </button>
            </div>
            <div class="items-center justify-end hidden space-x-2 xl:space-x-5 lg:flex lg:flex-1 lg:w-0">
              <div class="hidden lg:flex lg:items-center">
                <?php wp_nav_menu( array( 'theme_location' => 'header-right', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_class' => 'menu-menu-droite flex space-x-2 xl:space-x-6', 'container' => false ) ); ?>
              </div>
              <div class="hidden xl:flex">
                <a href="/?s=" class="flex">
                  <img src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/search.svg" />
                </a>
              </div>
              <span class="inline-flex rounded-md shadow-sm">
                <a href="https://portail.spre.fr/" target="_blank" class="inline-flex items-center justify-center px-6 py-1 text-base font-medium leading-6 text-white whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md bg-spre-red hover:shadow-spre">
                  Mon Espace
                </a>
              </span>
            </div>
          </div>
        </div>

        <div
            @click.away="isOpen = false"
            x-show="isOpen"
            x-transition:enter="duration-200 ease-out"
            x-transition:enter-start="opacity-0 scale-95"
            x-transition:enter-end="opacity-100 scale-100"
            x-transition:leave="duration-100 ease-in"
            x-transition:leave-start="opacity-100 scale-100"
            x-transition:leave-end="opacity-0 scale-95"
            class="absolute inset-x-0 top-0 z-10 block p-2 transition origin-top-right transform shadow-3xl lg:hidden"
        >
          <div class="rounded-lg shadow-lg">
            <div class="bg-white divide-y-2 rounded-lg shadow-xs divide-gray-50">
              <div class="px-5 pt-5 pb-6 space-y-6">
                <div class="flex items-center justify-between">
                  <div class="flex items-center flex-shrink-0">
                    <a href="<?= home_url('/'); ?>" class="inline-flex">
                      <?php if (spre_get_option('logo1x')){ ?>
                          <img class="w-auto h-14 lg:h-20" alt="La SPRE" src="<?= spre_get_option('logo2x'); ?>" />
                      <?php } ?>
                    </a>
                  </div>
                  <div class="-mr-2">
                    <button @click="isOpen = false" type="button" class="inline-flex items-center justify-center p-2 text-gray-400 transition duration-150 ease-in-out rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500">
                      <!-- Heroicon name: x -->
                      <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                      </svg>
                    </button>
                  </div>
                </div>
                <div>
                  <nav>
                    <?php wp_nav_menu( array( 'theme_location' => 'header-main', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_id' => 'menu-menu-principal-mobile', 'menu_class' => 'menu-menu-principal-mobile grid gap-y-8', 'container' => false ) ); ?>
                  </nav>
                </div>
              </div>
              <div class="px-5 py-6 space-y-6">
                <?php wp_nav_menu( array( 'theme_location' => 'header-right', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_id' => 'menu-menu-droite-mobile', 'menu_class' => 'menu-menu-droite-mobile grid gap-y-8', 'container' => false ) ); ?>
                <div class="flex items-center p-3 -m-3 transition duration-150 ease-in-out rounded-md hover:bg-gray-50">
                  <a href="/?s=" class="w-full text-sm font-semibold leading-6 text-spre-brown_dark">Recherche</a>
                </div>
                <div class="space-y-6">
                  <span class="flex w-full rounded-md shadow-sm">
                    <a href="https://portail.spre.fr/" target="_blank" class="flex items-center justify-center w-full px-6 py-1 text-base font-medium leading-6 text-white transition duration-150 ease-in-out border-transparent rounded-md bg-spre-red">
                      Mon Espace
                    </a>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div id="mainContent" role="main">