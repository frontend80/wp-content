<?php
get_header();
while (have_posts()):
	the_post(); ?>
    <div class="pageContent editableContent">
        <div class="pageInner">
            <?php the_content(); ?>
            <div class="clear"></div>
        </div>
    </div>
    <?php
endwhile;
?>
<?php get_footer(); ?>
