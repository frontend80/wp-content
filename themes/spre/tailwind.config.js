module.exports = {
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: true,
    content: ['./**/*.php']
  },
  // purge: {
  //   mode: 'all',
  //   content: [
  //     './*.php',
  //     './page-templates/*.php',
  //     './includes/*.php',
  //   ]
  // },
  theme: {
    fontFamily: {
      sans: ['Poppins', 'sans-serif'],
    },
    fontSize: {
      xxs: '.688rem',
      xs: '.75rem',
      sm: '.813rem',
      base: '.938rem',
      lg: '1.125rem',
      xl: '1.25rem',
      xxl: '1.438rem',
      '2xl': '1.625rem',
      '3xl': '2.063rem',
      '4xl': '2.625rem',
      '5xl': '3.125rem',
    },
    extend: {
      boxShadow: {
        'spre': '0 4px 10px 4px rgba(0, 0, 0, 0.1), 0 2px 4px 2px rgba(0, 0, 0, 0.05)',
      },
      inset: {
        '-191': '-191px',
        '-465': '-465px',
      },
      borderRadius: {
        '4xl': '4.375rem',
      },
      lineHeight: {
        '1_4': '1.4',
        '1_8': '1.8',
      },
      maxWidth: {
        '1100': '1100px',
        '8xl': '87rem',
      },
      spacing: {
        '1_5': '.35rem',
      },
      colors: {
        spre: {
          purple_light: "#BCC0FA",
          purple: "#3C287E",
          red: "#CC1F30",
          red_hover: "#a80c1c",
          brown_ultralight: "#EDE9E2",
          brown_ultralight_hover: "#ddd6ca",
          brown_light: "#DDD9CF",
          brown: "#B9B2A3",
          brown_dark: "#4A4A49",
          white: "#FFFFFF",
          gray_ultralight: "#EBEBEB",
          gray_light: "#C0C0C0",
          graybg: '#F5F5F5',
          gray: "#7A7A7A",
          gray_dark: "#232323",
          footer: "#F7F7F7",
        }
      },
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/ui'),
  ],
}
