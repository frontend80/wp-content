<?php
$queries = array();
parse_str($_SERVER['QUERY_STRING'], $queries);
$addType = array_key_exists('type', $queries) ? '&type=help_center' : '';
$placeholder = array_key_exists('type', $queries) ? "Rechercher dans le centre d'aide" : 'Rechercher';
get_header(); ?>
    
<div class="pageInner">
  <div class="row">
    <div class="col-8 col-l-8 col-m-12 col-s-12">
      <section class="pageContent editableContent">
        <header class="relative z-0 pb-20 pageHeader">
          <div class="relative z-20 pageInner">
            <!-- <div class="relative flex justify-end w-full pt-10">
              <?php include locate_template('includes/social_share.php'); ?>
            </div> -->
            <div class="w-2/3 mt-16 headerContent">
              <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= __('Résultats de recherche', 'spre') ?></h1>
              <div class="flex items-center w-full my-10 ">
                <div x-data="{search:''}" class="flex w-full mt-1 rounded-md">
                  <div class="relative flex items-stretch flex-grow focus-within:z-10">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-4 pointer-events-none">
                      <!-- Heroicon name: users -->
                      <img src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/search.svg" />
                    </div>
                    <input id="search" x-model="search" class="block w-full pl-12 transition duration-150 ease-in-out border-none rounded-none text-spre-brown_dark bg-spre-brown_ultralight form-input rounded-l-md sm:text-sm sm:leading-5" placeholder="<?= $placeholder ?>">
                  </div>
                  <a x-bind:href="`./?s=${search}<?= $addType ?>`" class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium leading-5 text-white transition duration-150 ease-in-out cursor-pointer rounded-r-md bg-spre-red hover:text-white hover:bg-spre-red_hover focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700">
                    <!-- Heroicon name: sort-ascending -->
                    <!-- <svg class="w-5 h-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                      <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h5a1 1 0 000-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM13 16a1 1 0 102 0v-5.586l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 101.414 1.414L13 10.414V16z" />
                    </svg> -->
                    <span class="">Rechercher</span>
                  </a>
                </div>
              </div>
              <?php if(get_search_query()) { ?>
                <div class="excerpt contentIntro spre_chapeau_purple">
                  <?= $wp_query->found_posts ?>
                  <?= __(
                  'résultats de recherche pour',
                  'spre'
                  ) ?> 
                  <em><?= get_search_query() ?></em>
                </div>
              <?php } ?>
            </div>
            <!-- <div class="relative flex justify-end w-full pt-10">
              <?php include locate_template('searchform.php'); ?>
            </div> -->
          </div>
        </header>
        <?php if($wp_query->found_posts > 0) { ?>
          <div class="flex flex-wrap justify-center mb-12">
            <?php while (have_posts()):
              the_post(); ?>
                <?php 
                  include locate_template('includes/news_item.php');
                ?>
            <?php
            endwhile; ?>
          </div>
        <?php } ?>
        <div class="flex flex-wrap items-center justify-center pt-24 pb-48">
          <!-- <div class="inline-flex rounded-md shadow-sm">
            <a href="/?paged=2" class="inline-flex items-center justify-center py-2 text-base font-medium leading-6 whitespace-no-wrap transition duration-150 ease-in-out border border-transparent rounded-md px-9 text-spre-white bg-spre-brown_dark">
              Afficher + d'articles
            </a>
          </div> -->
        </div>
      </section>
    </div>
    <div class="clear"></div>
  </div>
</div>
    
<?php get_footer(); ?>
