<?php
get_header();
while (have_posts()):

	the_post();

	$post_cats = get_the_terms($post->ID, 'category');
	$related_posts = [];
	if ($post_cats) {
		$post_cats_ids = [];
		foreach ($post_cats as $cat) {
			$post_cats_ids[] = $cat->term_id;
		}
		$tax_query = [
			[
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $post_cats_ids,
			],
		];
		$args = [
			// post_type
			'posts_per_page' => 3,
			'tax_query' => $tax_query,
			'exclude' => [$post->ID],
			'suppress_filters' => 0,
		];
		$related_posts = get_posts($args);
	}

	$date = get_the_time('d M Y', $post->ID);
	$all_link_url = spre_get_option('blog_page_id')
		? get_permalink(spre_get_option('blog_page_id'))
		: '';

	// $header_url = spre_get_featured_image_url($post->ID, 'page_header');
	$h1_title = $post->post_title;
	?>
    <header class="pageHeader singleHeader postHeader">
        <div class="pageInner">
            <?php include locate_template('includes/post_nav.php'); ?>
            <div class="postMetas">
                <?php if (
                	$post_cats &&
                	$post_cats[0]->term_id != 1
                ) { ?><span class="category"><?= $post_cats[0]->name ?></span> - <?php } ?>
                <span class="date"><?= $date ?></span>
            </div>
            <div class="clear"></div>
            <h1><?= nl2br($h1_title) ?></h1>
            <?php if ($post->post_excerpt) { ?>
            <div class="excerpt"><?= nl2br($post->post_excerpt) ?></div>
            <?php } ?>
            <div class="timeReading"><?= spre_svg('clock') ?> <span><?= spre_get_time_reading(
 	$post->ID
 ) ?> de lecture</span></div>
            <?php
	// include(locate_template('includes/social_share.php'));
	?>
        </div>
    </header>

    <div class="row singleContentWrapper postContentWrapper">
        <div class="pageInner">
            <div class="singleContent postContent editableContent">
                <?php the_content(); ?>
                <div class="espace20"></div>
                <?php include locate_template('includes/social_share_inner.php'); ?>
            </div>
        </div>
    </div>

    <?php if ($related_posts) { ?>
    <div class="pagePart relatedPosts relatedNews">
        <div class="pageInner">
            <div class="h2"><?= spre_get_option('related_posts_title') ?></div>
            <?php if (
            	spre_get_option('related_posts_intro')
            ) { ?><div class="subtitle"><?= spre_get_option(
	'related_posts_intro'
) ?></div><?php } ?>
            <?php
            $posts = $related_posts;
            $featured = true;
            include locate_template('includes/news.php');
            ?>
        </div>
    </div>
    <?php } ?>
    <?php
endwhile;
?>
<?php get_footer(); ?>
