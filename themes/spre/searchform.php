
<?php
$products_page_id = spre_get_option('products_page_id');
if ($products_page_id) { ?>
<form methid="GET" action="<?= get_permalink($products_page_id) ?>" id="searchForm">
    <div class="searchWrapper">
        <input type="search" placeholder="Rechercher un produit" name="recherche" value="<?= $current_search ?>" />
        <button type="submit"><?= spre_svg('search') ?></button>
    </div>
</form>
<?php }
 ?>
