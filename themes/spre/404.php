<?php
    get_header();
    if ( spre_get_option( 'page_404' ) ) {
        $block_id = spre_get_option( 'page_404' );
    }
?>
<div class="relative pageContent editableContent">
  <div class="pb-48 pageInner">
    <div class="flex flex-wrap px-4 sm:flex-no-wrap lg:px-0">
      <div class="flex items-center w-full sm:w-1/4">
        <img class="mt-12 mb-4 sm:mb-12 sm:mt-0" src="<?= get_template_directory_uri() ?>/assets/images/svg/icons/404.svg)" />
      </div>
      <div class="w-full sm:w-3/4 lg:w-1/2">
        <h1 class="mt-8 mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= __("404...", "spre"); ?></h1>
        <?= do_shortcode('[block id="' . $block_id . '"]') ?>
      </div>
    </div>
  </div>
  <div class="absolute left-0 right-0 z-0 h-52 lg:right-1/3" style="bottom:-40px; background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/vibes.svg)">
  </div>
  <div class="clear"></div>
</div>

<?php get_footer(); ?>