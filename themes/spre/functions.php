<?php
/**
 * cp functions and definitions
 */

if (!function_exists('spre_setup')):
	/**
	 * cp setup.
	 *
	 * Set up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support post thumbnails.
	 *
	 * @since cp 1.0
	 */
	function spre_setup()
	{
		// This theme styles the visual editor to resemble the theme style.
		add_editor_style(['assets/css/editor-style.css', spre_fonts_url()]);

		// Tailles d'images

		add_theme_support('post-thumbnails');

		remove_image_size('medium');
		remove_image_size('medium_large');
		remove_image_size('large');

		update_option('thumbnail_size_w', 380);
		update_option('thumbnail_size_h', 240);
		update_option('thumbnail_crop', '1');

		// This theme uses wp_nav_menu() in two locations.
		$nav_menus = [
			'header-main' => 'Menu Principal',
			'header-right' => 'Menu Droite',
			'footer-1' => 'Menu Pied de page 1',
			'footer-2' => 'Menu Pied de page 2',
			'footer-3' => 'Menu Pied de page 3',
			'footer-4' => 'Menu Pied de page 4',
			'footer-bottom' => 'Menu Bas du pied de page',
		];
		register_nav_menus($nav_menus);
	}
endif; // spre_setup
add_action('after_setup_theme', 'spre_setup');

add_filter('jpeg_quality', function () {
	return 80;
});

/* Webfonts */

function spre_fonts_url() {
	$font_url = add_query_arg( 'family',  'Poppins:200,300,400,500,600,700', "//fonts.googleapis.com/css" );
	$font_url = add_query_arg( 'display',  'swap', $font_url );
return $font_url;
}

/* Enqueue scripts and styles */

function enqueue_swiper(){
	if( !wp_script_is('swiper') ) { wp_enqueue_script( 'swiper',  get_template_directory_uri() . '/assets/js/swiper/swiper.min.js', array(), '1.0', true ); }
}

function enqueue_chosen(){
	if( !wp_script_is('chosen') ) { wp_enqueue_script( 'chosen', get_template_directory_uri() . '/assets/js/chosen/chosen.jquery.min.js' ); }
}

function enqueue_fancybox(){
	wp_enqueue_style( 'fancybox-style', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css', array(), '1.0', 'screen' );
	wp_enqueue_script( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', array(), '1.0', true );
}

function enqueue_datepicker(){
	wp_enqueue_style( 'air-datepicker', get_template_directory_uri() . '/assets/js/air-datepicker/css/datepicker.min.css', array(), '1.0', 'screen' );
	wp_enqueue_script( 'air-datepicker-fr', get_template_directory_uri() . '/assets/js/air-datepicker/js/datepicker.min.js', array(), '1.0', true );
	wp_enqueue_script( 'air-datepicker', get_template_directory_uri() . '/assets/js/air-datepicker/js/i18n/datepicker.fr.js', array(), '1.0', true );
}

if (function_exists('vc_remove_element')) {
	include_once 'extensions/visual_composer/visual_composer.php';
}
	
function spre_scripts() {

	wp_enqueue_style( 'spre-webfonts', spre_fonts_url(), array(), null );
	
	// wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery',  '//code.jquery.com/jquery-3.4.1.min.js', array(), '3.4.1', false );

	wp_enqueue_script( 'custom-form-elements-script',  get_template_directory_uri() . '/assets/js/custom-form-elements.min.js', array(), '1.100', false );

	// wp_enqueue_script( 'spre-scripts',  get_template_directory_uri() . '/assets/js/scripts.min.js', array(), '1.0.01', true );
	// wp_localize_script( 'spre-scripts', 'spre_script_objects', array(
	// 	'theme_url' => get_template_directory_uri(),
	// 			'ajax_url' => admin_url('admin-ajax.php'),
	// ));
	wp_enqueue_style( 'spre-style', get_template_directory_uri() . '/assets/css/style.css', array(), '1.0.05', 'screen' );
}
add_action( 'wp_enqueue_scripts', 'spre_scripts' );

function spre_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/assets/css/style-login.css', array(), '1.02', 'all' );
}
add_action( 'login_enqueue_scripts', 'spre_login_stylesheet' );

function spre_load_wp_admin_style() {
	wp_register_style( 'spre_chosen_wp_admin_css', get_template_directory_uri() . '/assets/js/chosen/chosen-original.css', false, '1.0.0' );
	wp_enqueue_style( 'spre_chosen_wp_admin_css' );
	wp_enqueue_script( 'spre_chosen_wp_admin_jquery', get_template_directory_uri() . '/assets/js/chosen/chosen.jquery.min.js' );
	wp_enqueue_script( 'spre_chosen_wp_admin_script', get_template_directory_uri() . '/assets/js/chosen/wp-chosen.js' );

	wp_register_style( 'spre_admin_css', get_template_directory_uri() . '/assets/css/admin.css', false, '1.0.02' );
	wp_enqueue_style( 'spre_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'spre_load_wp_admin_style' );

function spre_pre_get_posts( $query ) {
	if ( $query->is_main_query() && $query->is_search() && ! is_admin() ) {    
			if ( isset($_GET['type']) && $_GET['type'] ){
					$searchable_post__types = array( $_GET['type'], );   
					$query->set( 'post_type', $searchable_post__types );
			}
	}
	return $query;
}
add_filter( 'pre_get_posts' , 'spre_pre_get_posts' );

include_once 'extensions/utils.php';
include_once 'extensions/ajax.php';
include_once 'extensions/short_codes.php';
include_once 'extensions/tinymce.php';
include_once 'extensions/custom_taxonomy.php';
include_once 'extensions/custom_metabox.php';

include_once 'extensions/post_types/block.php';
include_once 'extensions/post_types/spre_user.php';
include_once 'extensions/post_types/help_center.php';
include_once 'extensions/post_types/download.php';
include_once 'extensions/post_types/scale.php';
// include_once('extensions/post_types/associate.php');
// include_once('extensions/post_types/partner.php');

add_filter('term_description', 'shortcode_unautop');
add_filter('term_description', 'do_shortcode');
add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');
add_filter('the_excerpt', 'shortcode_unautop');
add_filter('the_excerpt', 'do_shortcode');

remove_filter('pre_term_description', 'wp_filter_kses');
remove_filter('pre_link_description', 'wp_filter_kses');
remove_filter('pre_link_notes', 'wp_filter_kses');
remove_filter('term_description', 'wp_kses_data');
remove_filter('pre_term_description', 'wp_kses_data');

// Security
add_filter('login_errors', function ($a) {
	return null;
});
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');

add_filter('rest_authentication_errors', 'spre_secure_api');
function spre_secure_api($result)
{
	if (!empty($result)) {
		return $result;
	}
	if (!is_user_logged_in()) {
		return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', [
			'status' => 401,
		]);
	}
	return $result;
}

add_post_type_support('page', 'excerpt');

// disable Gutenberg for posts
add_filter('use_block_editor_for_post', '__return_false', 10);
// disable Gutenberg for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/**
 * Disable the emoji's
 */
function spre_disable_emojis()
{
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	add_filter('tiny_mce_plugins', 'spre_disable_emojis_tinymce');
	add_filter('wp_resource_hints', 'spre_disable_emojis_remove_dns_prefetch', 10, 2);
}
add_action('init', 'spre_disable_emojis');

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function spre_disable_emojis_tinymce($plugins)
{
	if (is_array($plugins)) {
		return array_diff($plugins, ['wpemoji']);
	} else {
		return [];
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function spre_disable_emojis_remove_dns_prefetch($urls, $relation_type)
{
	if ('dns-prefetch' == $relation_type) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

		$urls = array_diff($urls, [$emoji_svg_url]);
	}

	return $urls;
}

if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(
			array(
					// Replace [YOUR THEME TEXT DOMAIN] below with the text domain of your theme (found in the theme's `style.css`).
					'label' => __( 'Secondary Image', 'spre'),
					'id' => 'secondary-image',
					'post_type' => 'help_center'
			)
	);
}

if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(
			array(
					// Replace [YOUR THEME TEXT DOMAIN] below with the text domain of your theme (found in the theme's `style.css`).
					'label' => __( 'Secondary Image', 'spre'),
					'id' => 'secondary-image',
					'post_type' => 'spre_user'
			)
	);
}

if (class_exists('MultiPostThumbnails')) {
	new MultiPostThumbnails(
			array(
					// Replace [YOUR THEME TEXT DOMAIN] below with the text domain of your theme (found in the theme's `style.css`).
					'label' => __( 'Fichier', 'spre'),
					'id' => 'secondary-image',
					'post_type' => 'downloads'
			)
	);
}

add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
    return get_template_directory_uri() . "/assets/images/loading/loader.svg";
}

function wpm_login_style() { ?>
	<style type="text/css">
			#login h1 a, .login h1 a {
					background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_sre.png);
			}
	
	/*Vous pouvez ajouter d'autres styles CSS ici */
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'wpm_login_style' );