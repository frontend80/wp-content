(function($){

var scroll_top = 0,
    forms_initialized = false,
    current_state = "",
    hashtag = window.location.hash.replace('#', ''),
    current_offer = 0,
    anims = [],
    current_page_state = $('body').hasClass('parent') ? 'parent' : 'child';

$.fn.isOnScreen = function() {
    var $this_ = $(this);
    var $window = $(window);

    var docViewTop = scroll_top; //$window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $this_.offset().top;
    var elemBottom = elemTop + $this_.height();

    var offset = 150;
    if (elemTop == 0){ offset = 0; }

    return ((elemTop >= docViewTop + offset && elemTop <= docViewBottom - offset) || (elemBottom >= docViewTop + offset && elemBottom <= docViewBottom - offset));
};

$.fn.switchImage = function(to_state) {
    var this_ = $(this);
    this_.each(function() {
        var src = $(this).attr('data-src-'+to_state);
        $(this).attr('src', src);
    });
    return this_;
};
$.fn.switchImages = function(to_state) {
    var this_ = $(this);
    this_.each(function() {
        $(this).find('img').switchImage(to_state);
    });
    return this_;
};

function getScrollTop(){ //  Verifies the total sum in pixels of the whole page
    if(typeof pageYOffset!= 'undefined'){
        // Most browsers
        return pageYOffset;
    }
    else{
        var B= document.body; //IE 'quirks'
        var D= document.documentElement; //IE with doctype
        D= (D.clientHeight)? D: B;
        return D.scrollTop;
    }
}

function onScroll(){
    var new_scroll_top = getScrollTop();
    var scroll_mvt = new_scroll_top - scroll_top;
    scroll_top = new_scroll_top;

    if (scroll_top > 0){
        $('body').addClass('scrolled');
    }else{
        $('body').removeClass('scrolled');
    }

    if (scroll_mvt <= -5){
        $('body').removeClass('scrollDown');
    }else if (scroll_mvt > 5){
        $('body').addClass('scrollDown');
    }
    
    $('.opened').removeClass('opened');
    $('section, .wpb_row, .onShow').not('.loaded').each(function(){
        if ( $(this).isOnScreen() ){
            $(this).removeClass('unLoaded').addClass('loaded');
            $(this).find('.SVGAnim').each(function(){
                var anim_id = $(this).attr('id');
                anims[anim_id].stop();
                if ( !$(this).parents('.imageContent').hasClass('parentChild') || 
                      $(this).parents('.imageContent').hasClass(current_page_state+'State') ){
                    anims[anim_id].play();
                }
            });
        }
    });
}

function initSliders(){
    if (typeof Swiper != 'undefined'){
        // var homeSlider = new Swiper('.swiper-container.homeSlider', {
        //     speed: 500,
        //     spaceBetween: 0,
        //     // effect: 'fade',
        //     autoHeight: true,
        //     autoplay: {
        //         delay: 4000,
        //     },
        //     loop: true,
        //     navigation: {
        //         nextEl: '.swiper-button-next',
        //         prevEl: '.swiper-button-prev',
        //     },
        // });
        // homeSlider.on('slideChangeTransitionStart', function () {
        // });
    }
}

function setComHeight() {
    $('.comHeight').css('height', 'auto').removeClass('heightDone');
    if (current_state == 'mobile') {
        $('.comHeight').css('height', 'auto').removeClass('heightDone');
    } else {
        $('.comHeight').each(function() {
            if (!$(this).hasClass('heightDone')) {
                var group = $(this).attr('data-height-group');
                var max_height = 0;
                $('.comHeight[data-height-group=' + group + ']').each(function() {
                    max_height = Math.max(max_height, parseInt($(this).outerHeight()));
                });
                $('.comHeight[data-height-group=' + group + ']').each(function() {
                    var totalPadding = 0;
                    if ( $(this).css('box-sizing') != 'border-box' ){
                        totalPadding = parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));
                    }
                    $(this).css('height', (max_height - totalPadding + 1) + 'px').addClass('heightDone');
                })
            }
        });
    }
}

function onResize(){
    if (current_state != spre_current_size){
        $('.multiRes').not('.scrollLoad').switchImage(spre_current_size);
        $('body').removeClass(current_state).addClass(spre_current_size);
        
        $('.espace').each(function(){
            if ( this.hasAttribute('data-'+spre_current_size+'-height') && $(this).attr('data-'+spre_current_size+'-height')){
                var space_desktop_height = parseInt($(this).attr('data-desktop-height'));
                var space_height = parseInt($(this).attr('data-'+spre_current_size+'-height'));
                var property = 'padding-top';
                if (space_desktop_height < 0){ property = 'margin-top'; }
                $(this).css(property, $(this).attr('data-'+spre_current_size+'-height')+'px');
            }
        });
            
        $('.respBg').each(function(){
            $(this).css('background-image', 'url('+$(this).attr('data-'+spre_current_size+'-background')+')');
        });

        var mainNav = $('#mainMenu').detach(),
            topRightMenu = $('#topRightMenu').detach();

        if (spre_current_size == 'mobile' || spre_current_size == 'tablet') {
            $('#respMenuWrapper').append(mainNav);
            $('#respMenuWrapper').append(topRightMenu);

            $('#respMenuWrapper a').click(function(e){
                $('body').removeClass('menuOpened');
            });
        } else {
            $('#mainNavWrapper').append(mainNav);
            $('#headerTop .rightPart').append(topRightMenu);
        }
        current_state = spre_current_size;
    }
    setComHeight();
}

function initResp() {
    $('#respMenuButton').click(function(e) {
        $('body').toggleClass('menuOpened');
        e.preventDefault();
        e.stopPropagation();
        window.dispatchEvent(new Event('resize'));
    });
    $('#mainNav, #topRightLinks').click(function(e) {
        e.stopPropagation();
    });
}
function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}
function initKeyNumbers() {
    var duration = 2000,
        period = 100,
        steps_count = duration/period,
        iter = 0;
    var keys_interval = setInterval(function(){
        $('.keyNumber').not('.numberDone').each(function(){
            var number = parseInt($(this).attr('data-number'));
            var val = Math.min( parseInt( number / steps_count * (iter+1) ), number);
            $(this).find('.value').html(numberWithSpaces(val));
            if (val == number){
                $(this).addClass('numberDone');
            }
        });
        iter++;
    }, period);
    setTimeout(function(){
        clearInterval(keys_interval);
    }, duration + 1000);
}

$(window).scroll(function(){
    onScroll();
});
document.addEventListener("touchmove", onScroll, false);

$(window).resize(function() {
    onResize();
    onScroll();
});

function setEvents(){
    $('a').click(function(e){
        var hash_current = $(this).attr('href').replace(window.location.origin + window.location.pathname, '');
        if ( hash_current.startsWith('#')  ){
            $('html, body').animate({
                scrollTop: $(hash_current).offset().top - 67
            }, 100);
            e.preventDefault();
        }
    });
    $('*').not('.open').click(function() {
        $('.socialsShareWrapper').removeClass('opened');
    });
    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            $('.opened').removeClass('opened');
            closePopup();
        };
    });
    $('.socialsShareWrapper .open').click(function(e){
        $(this).parents('.socialsShareWrapper').toggleClass('opened');
        e.stopPropagation();
    });
}

function setEtc(){
    if ($(".chosen-select").length){
        $(".chosen-select").chosen({disable_search_threshold: 10});
    }
}

function setTabs(){
    function setCurrentTab(tabId){
        $('#'+tabId).parents('.tabsContent').find('.content').hide().removeClass('current');
        $('#'+tabId).parents('.tabsContent').find('.tabs a').removeClass('current');
        $('#'+tabId).addClass('current');
        $('#'+tabId).parents('.tabsContent').find('.content.'+tabId).fadeIn(300).addClass('current');
        
        var pointe = $('#'+tabId).parents('.tabsContent').find('.pointe')
        pointe.remove();
        $('#'+tabId).append(pointe);
    }
    $('.tabsContent .tabs').on("click", "a", function(e) {
        setCurrentTab($(this).attr('id'));
    });
    $('.tabLink').on("click", function(e) {
        setCurrentTab($(this).attr('alt'));
    });
    $('.tabsContent').each(function(){
        $(this).find('.tabs a:first').addClass('current');
        $(this).find('.content:first').addClass('current');
    });
}
    
function initCustomFileInput(){
    $('input[type=file]').each(function(){
        var fileInput = $(this).wrap('<div class="inputFile"></div>');
        var filename = $('<div class="file noSelected"><span class="label"></span></div>');
        fileInput.before(filename);
        filename.before($('<span class="link">'+spre_script_objects['upload_text']+'</span>'));
        var label = $(this).parents('.gfield').find('label').text();
        filename.find('span.label').text(label);
        $(this).parents('.gfield').addClass('gfield_file_wrapper');
    });
    $('.inputFile').wrap('<div class="inputFileWrapper"></div>');

    $('input[type=file]').change(function(){
        var val = $(this).val();
        var filename = val.split('fakepath\\')[1];
        $(this).parents('.inputFile').find('.file span.label').html(filename).removeClass('noSelected');
    });

    $('.inputFile .file, .inputFile .link').click(function(e){
        $(this).parents('.inputFile').find('input[type=file]').click();
        e.stopPropagation();
    }).show();
}

function initCustomElements(form_id){
    if ( typeof Custom !== "undefined" ){
        $('#gform_wrapper_'+form_id).find('.ginput_container_radio input[type=radio]').addClass('styled');
        $('#gform_wrapper_'+form_id).find('.ginput_container_checkbox input[type=checkbox]').addClass('styled');
        $('#gform_wrapper_'+form_id).find('.ginput_container_select select').addClass('styled').parents('li.gfield'); //.addClass('keepLabel');
        $('#gform_wrapper_'+form_id).find('input[type=file]').parents('li.gfield').addClass('keepLabel');
        // initCustomFileInput(form_id);
        Custom.init();
    }
}

function initForms(form_id){
    initCustomElements(form_id);
    $('#gform_wrapper_'+form_id).find('form button, form input[type=submit]').click(function(){
        var form = $(this).parents('form');
        $(this).addClass('loading');
    });
    $('.gform_wrapper form.twoCols').each(function(){
        var first_col_length = $(this).find('li.gfield').index($('.gsection.newCol').eq(0));        
        $(this).find('.gform_fields').wrapInner('<div class="row"></div>');
        $(this).find('.gsection.newCol').remove();
        $(this).find('li.gfield').slice(0,first_col_length).wrapAll('<div class="col-6 col-s-12"></div>');
        $(this).find('li.gfield').slice(first_col_length).wrapAll('<div class="col-6 col-s-12"></div>');
        $(this).addClass('loaded');
    });
}

$(document).bind('gform_post_render', function(event, form_id, current_page){
    console.log(form_id);
    initForms(form_id);
});
    
function updateMorePostsButtonVisibility(){
    if ($('.newsList').length > 0){
        if ($('.newsList .postItem').length >= posts_count){
          $('.morePosts.moreNews').hide();
        }else{
          $('.morePosts.moreNews').css('display', 'inline-block');
        }
    }
}

function initInfiniteScroll(){
    updateMorePostsButtonVisibility();
    if (typeof posts_count !== 'undefined') {
        $('.morePosts.moreNews').click(function(){
            var button = $(this);
            button.addClass('loading');
            var new_news = $('<div class="newNews"></div>');
            $('.newsList .postItem').removeClass('firstAdded');
            new_news.load(encodeURI(spre_script_objects['ajax_url'] + '?action=get-posts&cat=' + current_cat + '&offset=' + $('.newsList .postItem').length), function(){
                new_news.find('.postItem:first').addClass('firstAdded');
                $('.newsList').append( new_news.html() );
                new_news.remove();
                button.removeClass('loading');
                $('html, body').animate({
                    scrollTop: $('.newsList .postItem.firstAdded').offset().top - 180
                }, 'slow');
                setComHeight();
                onResize();
                updateMorePostsButtonVisibility();
                // setDownloadLinks();
            });
        });
    }
}

function setVideos(){
    $('.spreIframePlayerWrapper').on('click', '.cover', function(){
        var video_id = $(this).parents('.spreIframePlayerWrapper').attr('data-video-id');
        if (video_id){
            var video_provider = $(this).parents('.spreIframePlayerWrapper').attr('data-video-provider');
            if (video_provider && video_provider == 'vimeo'){
                var video_code = '<iframe id="player'+video_id+'" src="https://player.vimeo.com/video/'+video_id+'?autoplay=1&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>';
            }else{
                var video_code = '<iframe id="player'+video_id+'" frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" title="YouTube video player" src="https://www.youtube.com/embed/'+video_id+'?rel=0&amp;showinfo=0&amp;enablejsapi=1&amp;widgetid=1;autoplay=1" ></iframe>';
            }
            $(this).fadeOut(150).parents('.spreIframePlayerWrapper').append(video_code);
        }
    });
}

$(document).ready(function(){
    setEvents();
    // initSliders();
    initResp();
    setEtc();
    setTabs();
    setVideos();
    // initForms();
    initInfiniteScroll();
    window.setTimeout(function(){ 
        if ( hashtag && $('#'+hashtag).length ){
            $('html, body').animate({
                scrollTop: $('#'+hashtag).offset().top - $('#header').outerHeight()
            }, 100);
        }
    }, 200);
});

window.onload = function() {
    onResize(); onScroll();
    $('body').removeClass('loading');
    $('.homeHeader').addClass('loaded');
    window.setTimeout(function(){ 
        onScroll();
    }, 0);
}

})(jQuery);