(function() {
    // on cree un plugin pour ajouter des boutons
  
  
  /* fonction pour découper les chaines */
  function URLToArray(url) {
		var request = {};
		var pairs = url.substring(url.indexOf('?') + 1).split('&');
		for (var i = 0; i < pairs.length; i++) {
			var pair = pairs[i].split('=');
		var pasdehash = pair[1].split('#');
		request[decodeURIComponent(pair[0])] = decodeURIComponent(pasdehash[0]);
		}
		return request;
	}	
	
	tinymce.create('tinymce.plugins.tsc_buttons', {
		init : function(editor, url) {
			editor.addButton(
				'space', 
				{
					title : 'Espace',
					cmd : 'space',
					image : url + '/space.png'
				}
			);	
			editor.addCommand('space', function() {
				editor.insertContent( '[space height="20"]');
			});
			
			editor.addButton(
				'button', 
				{
					title : 'Button',
					cmd : 'button',
					image : url + '/button.png'
				}
			);	
			editor.addCommand('button', function() {
				editor.insertContent( '[button url="" target="_self"][/button]');
			});

			editor.addButton(
				'news', 
				{
					title : 'Actualités',
					cmd : 'news',
					image : url + '/news.png'
				}
			);	
			editor.addCommand('news', function() {
				editor.insertContent( "[news]");
			});

			editor.addButton(
				'featured_news', 
				{
					title : 'Actualités à la une',
					cmd : 'featured_news',
					image : url + '/news_featured.png'
				}
			);	
			editor.addCommand('featured_news', function() {
				editor.insertContent( '[featured_news title="Actualités" intro="Texte d\'introduction" count="4"]');
			});

			editor.addButton(
				'events', 
				{
					title : 'Evénements',
					cmd : 'events',
					image : url + '/events.png'
				}
			);	
			editor.addCommand('events', function() {
				editor.insertContent( "[events]");
			});
			editor.addButton(
				'featured_events', 
				{
					title : 'Evénements à la une',
					cmd : 'featured_events',
					image : url + '/events_featured.png'
				}
			);	
			editor.addCommand('featured_events', function() {
				editor.insertContent( "[featured_events]");
			});

			editor.addButton(
				'desktop_br', 
				{
					title : 'Saut de ligne desktop uniquement',
					cmd : 'desktop_br',
					image : url + '/desktop_br.png'
				}
			);	
			editor.addCommand('desktop_br', function() {
				editor.insertContent( '<br class="xlOnly" />');
			});
			
			editor.addButton(
				'accordion', 
				{
					title : 'Accordéon',
					cmd : 'accordion',
					image : url + '/accordion.png'
				}
			);	
			editor.addCommand('accordion', function() {
				editor.insertContent( '[accordion][content title=""][/content][content title=""][/content][/accordion]');
			});
			
			editor.addButton(
				'tabs', 
				{
					title : 'Onglets',
					cmd : 'tabs',
					image : url + '/tabs.png'
				}
			);	
			editor.addCommand('tabs', function() {
				editor.insertContent( '[tabs][tab title=""][/tab][tab title=""][/tab][/tabs]');
			});
			
			editor.addButton(
				'exergue', 
				{
					title : 'Exergue',
					cmd : 'exergue',
					image : url + '/exergue.png'
				}
			);	
			editor.addCommand('exergue', function() {
				editor.insertContent( '[exergue][/exergue]');
			});
			
			editor.addButton(
				'quote', 
				{
					title : 'Citation',
					cmd : 'quote',
					image : url + '/quote.png'
				}
			);	
			editor.addCommand('quote', function() {
				editor.insertContent( '[quote signature=""][/quote]');
			});
			
			editor.addButton(
				'team', 
				{
					title : 'Equipe',
					cmd : 'team',
					image : url + '/team.png'
				}
			);	
			editor.addCommand('team', function() {
				editor.insertContent( "[team]");
			});
		},
	});

	// on enregistre notre plugin
	tinymce.PluginManager.add( 'tsc_buttons', tinymce.plugins.tsc_buttons );

})();
