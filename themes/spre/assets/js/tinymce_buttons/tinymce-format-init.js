jQuery( document ).on( 'tinymce-editor-init', function( event, editor ) {
  // register the formats
  tinymce.activeEditor.formatter.register('spre_section_title_red', {
      block : 'h2',
      classes : 'spre_section_title_red',
  });
  tinymce.activeEditor.formatter.register('spre_h3_title_red', {
      block : 'div',
      classes : 'spre_h3_title_red',
  });
  tinymce.activeEditor.formatter.register('spre_chapeau', {
      block : 'div',
      classes : 'spre_chapeau',
  });
  tinymce.activeEditor.formatter.register('spre_paragraph', {
      block : 'p',
      classes : 'spre_paragraph',
  });
  tinymce.activeEditor.formatter.register('spre_chapeau_purple', {
    block : 'div',
    classes : 'spre_chapeau_purple',
  });
});
