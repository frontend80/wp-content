jQuery(document).ready(function($) {
    var config = {
      '.chosen-select' : {no_results_text: "<?php _e('Aucun résultat pour', 'pui'); ?>"},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
});