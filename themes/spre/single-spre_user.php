<?php
get_header();
while (have_posts()):

	the_post();
?>
  <header class="relative z-0 pb-20 pageHeader">
    <div class="relative z-20 pageInner">
      <div class="relative flex justify-between w-full py-10">
        <?php include locate_template('includes/user_nav.php'); ?>
        <?php include locate_template('includes/social_share.php'); ?>
      </div>
      <div class="w-2/3 headerContent">
        <h1 class="mb-8 text-3xl sm:text-4xl spre_section_title_red"><?= nl2br($post->post_title) ?></h1>
        <?php if ($post->post_excerpt) { ?>
          <div class="excerpt contentIntro spre_chapeau_purple"><?= apply_filters(
            'the_excerpt',
            $post->post_excerpt
          ) ?></div>
        <?php } ?>
      </div>
    </div>
    <?php if (get_the_post_thumbnail_url($post->ID)) { ?>
      <div class="absolute top-0 bottom-0 right-0 z-10 w-1/3">
        <div class="absolute z-10 overflow-hidden" style="left: 0px; top: 20%;">
          <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_the_post_thumbnail_url($post->ID); ?>" />
        </div>
      </div>
    <?php } ?>
    <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
      <div class="absolute bottom-0 right-0 z-0 overflow-hidden" style='left: 20px; top: 0px; background-image: url("<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg"); background-size: cover;'>
        <!-- <img class="w-auto h-auto max-w-full max-h-full" src="<?= $header_url ?>" /> -->
      </div>
    </div>
    <!-- <div class="absolute top-0 bottom-0 right-0 z-0 w-1/3">
      <div class="absolute z-0 overflow-hidden" style="left: 20px; top: 50px;">
        <img class="w-auto h-auto max-w-full max-h-full" src="<?= get_template_directory_uri(); ?>/assets/images/svg/patterns/vibes.svg" />
      </div>
    </div> -->
  </header>

  <div class="relative overflow-hidden row singleContentWrapper postContentWrapper">
    <div class="relative z-10 pageInner">
      <div class="singleContent postContent editableContent">
        <?php the_content(); ?>
        <div class="espace20"></div>
      </div>
    </div>
    <div class="absolute top-0 bottom-0 z-0 w-full" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/very_big_bg_circles.svg); background-position: top left; background-repeat: no-repeat; top:-330px; left: -1100px;">
    </div>
    <div class="absolute bottom-0 z-0 w-full top-72" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/big_circle_beige.svg); background-position: center right; background-repeat: no-repeat; right: -350px;">
    </div>
    <div class="absolute bottom-0 left-0 right-0 z-0 h-64 md:right-1/3" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/svg/patterns/vibes.svg)">
    </div>
  </div>

  <?php
endwhile;
?>
<?php include(locate_template('includes/spre_news_block.php')); ?>
<?php get_footer(); ?>
