<?php
$events_category_id = spre_get_option('events_category_id'); ?>
<aside class="col-3 col-m-4 col-s-12 sidebar news">
    <div class="clear espace50"></div>
    <?php if ($events_category_id) {

    	$tax_query = [];
    	$tax_query[] = [
    		'taxonomy' => 'category',
    		'field' => 'term_id',
    		'terms' => $events_category_id,
    	];
    	$args = [
    		'post_type' => 'post',
    		'posts_per_page' => -1,
    		'terms' => explode(',', $events_category_id),
    	];
    	$posts = get_posts($args);

    	$events = [];
    	foreach ($posts as $post) {
    		$date_begin = get_post_meta($post->ID, 'date_begin', true);
    		$date_description = get_post_meta($post->ID, 'date_description', true);
    		if ($date_begin) {
    			if (gettype($date_begin) == 'string') {
    				$date_begin = DateTime::createFromFormat('Y/m/j', $date_begin);
    			}
    			$date_begin_y = $date_begin->format('Y');
    			$date_begin_m = $date_begin->format('n');
    			$date_begin_d = $date_begin->format('j');
    		} else {
    			$date_begin_y = get_the_date('Y', $post->ID);
    			$date_begin_m = get_the_date('n', $post->ID);
    			$date_begin_d = get_the_date('j', $post->ID);
    		}
    		$events[] = [
    			'id' => $post->ID,
    			'date' => $date_begin_d . '/' . $date_begin_m . '/' . $date_begin_y,
    			'date_str' => $date_description
    				? $date_description
    				: date_i18n(
    					'j F Y',
    					strtotime($date_begin_y . '/' . $date_begin_m . '/' . $date_begin_d)
    				),
    			'title' => $post->post_title,
    			'url' => get_permalink($post->ID),
    		];
    	}
    	?>
    <h3 class="sideTitle">Agenda</h3>
    <div id="eventsDatepicker"></div>
    <div id="eventContent"></div>
    <script>
        var events = <?= json_encode($events) ?>;
        $(document).ready(function(){
            var eventDates = [1, 10, 12, 22],
            $picker = $('#eventsDatepicker'),
            $content = $('#eventContent');

            $picker.datepicker({
                language: 'fr',
                onRenderCell: function (date, cellType) {
                    var currentDate = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
                    if (cellType == 'day' && events.filter(e => e.date == currentDate).length) {
                        return {
                            html: date.getDate() + '<span class="dp-note"></span>'
                        }
                    }
                },
                onSelect: function onSelect(fd, date) {
                    $content.empty();
                    var currentDate = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
                    var date ='', title = '', content = '';
                    // If date with event is selected, show it                    
                    var dateEvents = events.filter(e => e.date == currentDate);
                    for (var i=0; i<dateEvents.length; i++){
                        var event = dateEvents[i], $eventItem = $('<a href="'+event.url+'" class="eventLink"></a>');
                        $eventItem.append('<div class="title">'+event.title+'</div>');
                        $eventItem.append('<div class="date">'+event.date_str+'</div>');
                        $eventItem.append('<div class="more">Voir</div>');
                        $content.append($eventItem);
                    }
                }
            })

            // Select initial date from `eventDates`
            var currentDate = currentDate = new Date();
            $picker.data('datepicker').selectDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), 10))
        });
    </script>
    <?php
    } ?>
    <?php
    $newsletter_page_id = spre_get_option('newsletter_page_id');
    if ($newsletter_page_id) { ?>
    <div class="clear espace50"></div>
    <div class="textRight"><a href="<?= get_permalink(
    	$newsletter_page_id
    ) ?>" class="link">Inscription Newsletter <?= spre_svg('arrow_right') ?></a></div>
    <?php }
    ?>
    <div class="clear espace60"></div>
</aside>